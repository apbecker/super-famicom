module SuperFamicom.Program

open SuperFamicom.Util

let runGame args =
  let driver = Driver.init args
  let audio = SDL2Audio.create ()
  let audioSink = SDL2Audio.sink audio
  let video = SDL2Video.create ()
  let videoSink = SDL2Video.sink video

  let rec loop status =
    match status with
    | VideoStatus.Exit ->
      ()

    | VideoStatus.More ->
      Driver.run driver audioSink videoSink
      SDL2Audio.render audio
      SDL2Video.render video |> loop

  loop VideoStatus.More


[<EntryPoint>]
let main args =
  runGame (Args.create args)
  0
