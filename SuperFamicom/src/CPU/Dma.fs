namespace SuperFamicom.CPU

type Dma =
  { channels: DmaChannel[]
    mutable mdmaEn: byte
    mutable hdmaEn: byte
    mutable mdmaCount: int }

module Dma =

  open SuperFamicom

  let init () =
    { channels = Array.init 8 (fun _ -> DmaChannel.init ())
      mdmaEn = 0uy
      hdmaEn = 0uy
      mdmaCount = 0 }


  let getAddressB (channel: DmaChannel) step =
    let step =
      match (int channel.control) &&& 7 with
      | 0 -> 0
      | 1 -> (step &&& 1)
      | 2 -> 0
      | 3 -> (step &&& 2) / 2
      | 4 -> (step &&& 3)
      | 5 -> (step &&& 1)
      | 6 -> 0
      | _ -> (step &&& 2) / 2

    uint16 (0x2100 ||| (((int channel.addressB) + step) &&& 0xff))


  let runChannel r w (channel: DmaChannel) =
    let mutable amount = 0<CpuCycle>
    let mutable step = 0
    let mutable loop = true

    while loop do
      amount <- amount + CpuCycle.Norm

      let bank = uint8 (channel.addressA >>> 16)
      let addr = uint16 (channel.addressA >>> 0)
      let dest = getAddressB channel step

      if (channel.control &&& 0x80uy) = 0uy then
        Writer.run w 00uy dest (Reader.run r bank addr 00uy)
      else
        Writer.run w bank addr (Reader.run r 00uy dest 00uy)

      channel.addressA <-
        match ((int channel.control) >>> 3) &&& 3 with
        | 0 -> channel.addressA + 1
        | 1 -> channel.addressA
        | 2 -> channel.addressA - 1
        | _ -> channel.addressA

      step <- step + 1

      channel.count <- channel.count - 1us

      if channel.count = 0us then
        loop <- false

    amount


  let run r w totalCycles (dma: Dma) =
    let mutable amount = 0<CpuCycle>
    let cycles = totalCycles % CpuCycle.Norm
    if cycles <> 0<CpuCycle> then
      // Align the clock divider
      amount <- amount + (CpuCycle.Norm - cycles)

    amount <- amount + CpuCycle.Norm // DMA initialization

    for i in 0 .. 7 do
      let enable = (dma.mdmaEn &&& (1uy <<< i)) <> 0uy
      if enable then
        amount <- amount + CpuCycle.Norm // DMA channel initialization
        amount <- amount + (runChannel r w dma.channels.[i])

    amount
