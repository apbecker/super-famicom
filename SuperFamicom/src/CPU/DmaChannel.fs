namespace SuperFamicom.CPU

type DmaChannel =
  { mutable count: uint16
    mutable addressA: int
    mutable addressB: byte
    mutable control: byte }

module DmaChannel =

  let init () =
    { count = 0us
      addressA = 0
      addressB = 0uy
      control = 0uy }
