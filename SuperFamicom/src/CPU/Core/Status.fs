namespace SuperFamicom.CPU.Core

type Status =
  { mutable e: bool
    mutable n: bool
    mutable v: bool
    mutable m: bool
    mutable x: bool
    mutable d: bool
    mutable i: bool
    mutable z: bool
    mutable c: bool }

module Status =

  let init () =
    { e = false
      n = false
      v = false
      m = false
      x = false
      d = false
      i = false
      z = false
      c = false }


  let pack status =
    byte (
      (if status.n then 0x80 else 0) |||
      (if status.v then 0x40 else 0) |||
      (if status.m then 0x20 else 0) |||
      (if status.x then 0x10 else 0) |||
      (if status.d then 0x08 else 0) |||
      (if status.i then 0x04 else 0) |||
      (if status.z then 0x02 else 0) |||
      (if status.c then 0x01 else 0) ||| (if status.e then 0x30 else 0)
    )


  let unpack (data: byte) status =
    let data = int data
    status.n <- (data &&& 0x80) <> 0
    status.v <- (data &&& 0x40) <> 0
    status.m <- (data &&& 0x20) <> 0 || status.e
    status.x <- (data &&& 0x10) <> 0 || status.e
    status.d <- (data &&& 0x08) <> 0
    status.i <- (data &&& 0x04) <> 0
    status.z <- (data &&& 0x02) <> 0
    status.c <- (data &&& 0x01) <> 0
