namespace SuperFamicom.CPU.Core

type Instruction =
  | Instruction of ((unit -> unit) -> SuperFamicom.Reader -> SuperFamicom.Writer -> Core -> unit)

module Codes =

  let opR flag codeB codeW = Instruction (fun io r w core ->
    if (flag core) || core.p.e then
      Core.lastCycle core
      let rdl = Core.read r core.regs.aab core.regs.aa
      codeB rdl core
    else
      let rdl = Core.read r core.regs.aab core.regs.aa
      core.regs.aa24 <- core.regs.aa24 + 1
      Core.lastCycle core
      let rdh = Core.read r core.regs.aab core.regs.aa
      codeW (Core.mk16 rdh rdl) core
  )


  let opM flag codeB codeW = Instruction (fun io r w core ->
    if (flag core) || core.p.e then
      let rdl = Core.read r core.regs.aab core.regs.aa

      io()
      let rdl = codeB rdl core

      Core.lastCycle core
      Core.write w core.regs.aab core.regs.aa rdl
    else
      let rdl = Core.read r core.regs.aab core.regs.aa
      core.regs.aa24 <- core.regs.aa24 + 1
      let rdh = Core.read r core.regs.aab core.regs.aa
      let rd  = Core.mk16 rdh rdl

      io()
      let rd = codeW rd core
      let rdl = uint8 (rd >>> 0)
      let rdh = uint8 (rd >>> 8)

      Core.write w core.regs.aab core.regs.aa rdh
      core.regs.aa24 <- core.regs.aa24 - 1
      Core.lastCycle core
      Core.write w core.regs.aab core.regs.aa rdl
  )


  let opN flag code8 code16 = Instruction (fun io r w core ->
    if (flag core) || core.p.e then
      Core.readImmediate8 code8 r core
    else
      Core.readImmediate16 code16 r core
  )


  let opW flag codeB codeW = Instruction (fun io r w core ->
    if (flag core) || core.p.e then
      let rdl = codeB core
      Core.lastCycle core
      Core.write w core.regs.aab core.regs.aa rdl
    else
      let rd  = codeW core
      let rdl = uint8 (rd >>> 0)
      let rdh = uint8 (rd >>> 8)

      Core.write w core.regs.aab core.regs.aa rdl; core.regs.aa24 <- core.regs.aa24 + 1
      Core.lastCycle core
      Core.write w core.regs.aab core.regs.aa rdh
  )


  let nz8 data core =
    core.p.n <- data > 0x7fuy
    core.p.z <- data = 0x00uy


  let nz16 data core =
    core.p.n <- data > 0x7fffus
    core.p.z <- data = 0x0000us


  let branch flag = Instruction (fun io r w core ->
    if not (flag core) then
      Core.lastCycle core
      ignore <| Core.fetch r core
    else
      let rdl = Core.fetch r core
      core.regs.aa <- core.regs.pc
      core.regs.pc <- core.regs.pc + (uint16 (int8 rdl))

      if core.p.e && (uint8 (core.regs.pc >>> 8)) <> core.regs.aah then
        io()

      Core.lastCycle core
      io()
  )


  let adc8 (data: byte) core =
    let mutable result = 0
    let a = int core.regs.al
    let data = int data

    if not core.p.d then
      result <- a + data + (if core.p.c then 1 else 0)
    else
      result <- (a &&& 0x0f) + (data &&& 0x0f) + (if core.p.c then 0x01 else 0);                     if (result > 0x09) then result <- result + 0x06; core.p.c <- result > 0x0f
      result <- (a &&& 0xf0) + (data &&& 0xf0) + (if core.p.c then 0x10 else 0) + (result &&& 0x0f)

    core.p.v <- (~~~(a ^^^ data) &&& (a ^^^ result) &&& 0x80) <> 0
    if (core.p.d && result > 0x9f) then result <- result + 0x60
    core.p.c <- result > 0xff
    core.p.n <- (byte)result >= 0x80uy
    core.p.z <- (byte)result = 0uy

    core.regs.al <- (byte)result


  let adc16 (data: uint16) core =
    let mutable result = 0
    let a = int core.regs.a
    let data = int data

    if not core.p.d then
      result <- a + data + (if core.p.c then 1 else 0)
    else
      result <- (a &&& 0x000f) + (data &&& 0x000f) + (if core.p.c then 0x0001 else 0);                       if (result > 0x0009) then result <- result + 0x0006; core.p.c <- result > 0x000f
      result <- (a &&& 0x00f0) + (data &&& 0x00f0) + (if core.p.c then 0x0010 else 0) + (result &&& 0x000f); if (result > 0x009f) then result <- result + 0x0060; core.p.c <- result > 0x00ff
      result <- (a &&& 0x0f00) + (data &&& 0x0f00) + (if core.p.c then 0x0100 else 0) + (result &&& 0x00ff); if (result > 0x09ff) then result <- result + 0x0600; core.p.c <- result > 0x0fff
      result <- (a &&& 0xf000) + (data &&& 0xf000) + (if core.p.c then 0x1000 else 0) + (result &&& 0x0fff)

    core.p.v <- (~~~(a ^^^ data) &&& (a ^^^ result) &&& 0x8000) <> 0
    if core.p.d && result > 0x9fff then result <- result + 0x6000
    core.p.c <- result > 0xffff
    core.p.n <- (uint16)result >= 0x8000us
    core.p.z <- (uint16)result = 0x0000us

    core.regs.a <- (uint16)result


  let and8 data core =
    core.regs.al <- core.regs.al &&& data
    nz8 core.regs.al core


  let and16 data core =
    core.regs.a <- core.regs.a &&& data
    nz16 core.regs.a core


  let asl8 (data: byte) core =
    core.p.c <- (data &&& 0x80uy) <> 0uy
    let data = data <<< 1
    nz8 data core
    data


  let asl16 (data: uint16) core =
    core.p.c <- (data &&& 0x8000us) <> 0us
    let data = data <<< 1
    nz16 data core
    data


  let opBraI = branch (fun _ -> true)
  let opBccI = branch (not << Core.flagC)
  let opBcsI = branch Core.flagC
  let opBplI = branch (not << Core.flagN)
  let opBmiI = branch Core.flagN
  let opBvcI = branch (not << Core.flagV)
  let opBvsI = branch Core.flagV
  let opBneI = branch (not << Core.flagZ)
  let opBeqI = branch Core.flagZ


  let bit8 data core =
    core.p.n <- (data &&& 0x80uy) <> 0uy
    core.p.v <- (data &&& 0x40uy) <> 0uy
    core.p.z <- (data &&& core.regs.al) = 0uy


  let bit16 data core =
    core.p.n <- (data &&& 0x8000us) <> 0us
    core.p.v <- (data &&& 0x4000us) <> 0us
    core.p.z <- (data &&& core.regs.a) = 0us


  let cmp8 (data: byte) core =
    nz8 (core.regs.al - data) core
    core.p.c <- core.regs.al >= data


  let cmp16 (data: uint16) core =
    nz16 (core.regs.a - data) core
    core.p.c <- core.regs.a >= data


  let cpx8 (data: byte) core =
    nz8 (core.regs.xl - data) core
    core.p.c <- core.regs.xl >= data


  let cpx16 (data: uint16) core =
    nz16 (core.regs.x - data) core
    core.p.c <- core.regs.x >= data


  let cpy8 (data: byte) core =
    nz8 (core.regs.yl - data) core
    core.p.c <- core.regs.yl >= data


  let cpy16 (data: uint16) core =
    nz16 (core.regs.y - data) core
    core.p.c <- core.regs.y >= data


  let dec8 (data: byte) core =
    let data = data - 1uy
    nz8 data core
    data


  let dec16 (data: uint16) core =
    let data = data - 1us
    nz16 data core
    data


  let eor8 data core =
    core.regs.al <- core.regs.al ^^^ data
    nz8 core.regs.al core


  let eor16 data core =
    core.regs.a <- core.regs.a ^^^ data
    nz16 core.regs.a core


  let inc8 data core =
    let data = data + 1uy
    nz8 data core
    data


  let inc16 data core =
    let data = data + 1us
    nz16 data core
    data


  let lda8 data core =
    core.regs.al <- data
    nz8 core.regs.al core


  let lda16 data core =
    core.regs.a <- data
    nz16 core.regs.a core


  let ldx8 data core =
    core.regs.xl <- data
    nz8 core.regs.xl core


  let ldx16 data core =
    core.regs.x <- data
    nz16 core.regs.x core


  let ldy8 data core =
    core.regs.yl <- data
    nz8 core.regs.yl core


  let ldy16 data core =
    core.regs.y <- data
    nz16 core.regs.y core


  let lsr8 (data: byte) core =
    core.p.c <- (data &&& 1uy) <> 0uy
    let data = data >>> 1
    nz8 data core
    data


  let lsr16 (data: uint16) core =
    core.p.c <- (data &&& 1us) <> 0us
    let data = data >>> 1
    nz16 data core
    data


  let ora8 data core =
    core.regs.al <- core.regs.al ||| data
    nz8 core.regs.al core


  let ora16 data core =
    core.regs.a <- core.regs.a ||| data
    nz16 core.regs.a core


  let rol8 (data: byte) core =
    let carry = if core.p.c then 1uy else 0uy
    core.p.c <- (data &&& 0x80uy) <> 0uy
    let data = (data <<< 1) ||| carry
    nz8 data core
    data


  let rol16 (data: uint16) core =
    let carry = if core.p.c then 1us else 0us
    core.p.c <- (data &&& 0x8000us) <> 0us
    let data = (data <<< 1) ||| carry
    nz16 data core
    data


  let ror8 (data: byte) core =
    let carry = if core.p.c then 0x80uy else 0uy
    core.p.c <- (data &&& 0x01uy) <> 0uy
    let data = (data >>> 1) ||| carry
    nz8 data core
    data


  let ror16 (data: uint16) core =
    let carry = (uint16)(if core.p.c then 0x8000 else 0)
    core.p.c <- (data &&& 0x0001us) <> 0us
    let data = (data >>> 1) ||| carry
    nz16 data core
    data


  let sbc8 (data: byte) core =
    let mutable result = 0
    let a = int core.regs.a
    let data = int (~~~data)

    if not core.p.d then
      result <- a + data + (if core.p.c then 1 else 0)
    else
      result <- (a &&& 0x0f) + (data &&& 0x0f) + (if core.p.c then 0x01 else 0)
      if (result <= 0x0f) then result <- result - 0x06
      core.p.c <- result > 0x0f
      result <- (a &&& 0xf0) + (data &&& 0xf0) + (if core.p.c then 0x10 else 0) + (result &&& 0x0f)

    core.p.v <- (~~~(a ^^^ data) &&& (a ^^^ result) &&& 0x80) <> 0
    if (core.p.d && result <= 0xff) then result <- result - 0x60
    core.p.c <- result > 0xff
    core.p.n <- (uint8 result) >= 0x80uy
    core.p.z <- (uint8 result) = 0uy

    core.regs.al <- uint8 result


  let sbc16 (data: uint16) core =
    let mutable result = 0
    let a = int core.regs.a
    let data = int (~~~data)

    if not core.p.d then
      result <- a + data + (if core.p.c then 1 else 0)
    else
      result <- (a &&& 0x000f) + (data &&& 0x000f) + (if core.p.c then 0x0001 else 0);                       if (result <= 0x000f) then result <- result - 0x0006; core.p.c <- result > 0x000f
      result <- (a &&& 0x00f0) + (data &&& 0x00f0) + (if core.p.c then 0x0010 else 0) + (result &&& 0x000f); if (result <= 0x00ff) then result <- result - 0x0060; core.p.c <- result > 0x00ff
      result <- (a &&& 0x0f00) + (data &&& 0x0f00) + (if core.p.c then 0x0100 else 0) + (result &&& 0x00ff); if (result <= 0x0fff) then result <- result - 0x0600; core.p.c <- result > 0x0fff
      result <- (a &&& 0xf000) + (data &&& 0xf000) + (if core.p.c then 0x1000 else 0) + (result &&& 0x0fff)

    core.p.v <- (~~~(a ^^^ data) &&& (a ^^^ result) &&& 0x8000) <> 0
    if core.p.d && result <= 0xffff then result <- result - 0x6000
    core.p.c <- result > 0xffff
    core.p.n <- (uint16 result) >= 0x8000us
    core.p.z <- (uint16 result) = 0us

    core.regs.a <- uint16 result


  let sta8 core = core.regs.al


  let sta16 core = core.regs.a


  let stx8 core = core.regs.xl


  let stx16 core = core.regs.x


  let sty8 core = core.regs.yl


  let sty16 core = core.regs.y


  let stz8 core = 0uy


  let stz16 core = 0us


  let trb8 (data: byte) core =
    core.p.z <- (data &&& core.regs.al) = 0uy
    data &&& (~~~core.regs.al)


  let trb16 (data: uint16) core =
    core.p.z <- (data &&& core.regs.a) = 0us
    data &&& (~~~core.regs.a)


  let tsb8 (data: byte) core =
    core.p.z <- (data &&& core.regs.al) = 0uy
    data ||| core.regs.al


  let tsb16 (data: uint16) core =
    core.p.z <- (data &&& core.regs.a) = 0us
    data ||| core.regs.a


  let tst8 data core =
    core.p.z <- (data &&& core.regs.al) = 0uy


  let tst16 data core =
    core.p.z <- (data &&& core.regs.a) = 0us


  let opAdcM = opR Core.flagM adc8 adc16
  let opAdcN = opN Core.flagM adc8 adc16
  let opAndM = opR Core.flagM and8 and16
  let opAndN = opN Core.flagM and8 and16
  let opAslM = opM Core.flagM asl8 asl16
  let opBitM = opR Core.flagM bit8 bit16
  let opCmpM = opR Core.flagM cmp8 cmp16
  let opCmpN = opN Core.flagM cmp8 cmp16
  let opCpxX = opR Core.flagX cpx8 cpx16
  let opCpxN = opN Core.flagX cpx8 cpx16
  let opCpyX = opR Core.flagX cpy8 cpy16
  let opCpyN = opN Core.flagX cpy8 cpy16
  let opDecM = opM Core.flagM dec8 dec16
  let opEorM = opR Core.flagM eor8 eor16
  let opEorN = opN Core.flagM eor8 eor16
  let opIncM = opM Core.flagM inc8 inc16
  let opLdaM = opR Core.flagM lda8 lda16
  let opLdaN = opN Core.flagM lda8 lda16
  let opLdxX = opR Core.flagX ldx8 ldx16
  let opLdxN = opN Core.flagX ldx8 ldx16
  let opLdyX = opR Core.flagX ldy8 ldy16
  let opLdyN = opN Core.flagX ldy8 ldy16
  let opLsrM = opM Core.flagM lsr8 lsr16
  let opOraM = opR Core.flagM ora8 ora16
  let opOraN = opN Core.flagM ora8 ora16
  let opRolM = opM Core.flagM rol8 rol16
  let opRorM = opM Core.flagM ror8 ror16
  let opSbcM = opR Core.flagM sbc8 sbc16
  let opSbcN = opN Core.flagM sbc8 sbc16
  let opStaM = opW Core.flagM sta8 sta16
  let opStxX = opW Core.flagX stx8 stx16
  let opStyX = opW Core.flagX sty8 sty16
  let opStzM = opW Core.flagM stz8 stz16
  let opTrbM = opM Core.flagM trb8 trb16
  let opTsbM = opM Core.flagM tsb8 tsb16
  let opTstN = opN Core.flagM tst8 tst16


  let opErrI = Instruction (fun _ _ _ core ->
    failwithf "Instruction \"%02x\" isn't implemented." core.code
  )


  let opMove (adjust: int) = Instruction (fun io r w core ->
    let dp = Core.fetch r core
    let sp = Core.fetch r core
    core.db <- dp
    let rdl = Core.read r sp core.regs.x
    Core.write w dp core.regs.y rdl
    io()

    if core.p.x || core.p.e then
      core.regs.xl <- core.regs.xl + ((byte)adjust)
      core.regs.yl <- core.regs.yl + ((byte)adjust)
    else
      core.regs.x <- core.regs.x + ((uint16)adjust)
      core.regs.y <- core.regs.y + ((uint16)adjust)

    Core.lastCycle core
    io()

    if core.regs.a <> 0us then
      core.regs.pc <- core.regs.pc - 3us

    core.regs.a <- core.regs.a - 1us
  )


  let opMvpI = opMove (-1)
  let opMvnI = opMove (+1)


  let opAslA = Instruction (fun io r w core ->
    if core.p.m || core.p.e then
      core.regs.al <- asl8 core.regs.al core
    else
      core.regs.a <- asl16 core.regs.a core
  )


  let opBrlI = Instruction (fun io r w core ->
    let rdl = Core.fetch r core
    let rdh = Core.fetch r core

    io()
    core.regs.pc <- core.regs.pc + (Core.mk16 rdh rdl)
  )


  let opClcI = Instruction (fun io r w core -> core.p.c <- false)
  let opCldI = Instruction (fun io r w core -> core.p.d <- false)
  let opCliI = Instruction (fun io r w core -> core.p.i <- false)
  let opClvI = Instruction (fun io r w core -> core.p.v <- false)


  let opDecA = Instruction (fun io r w core ->
    if core.p.m || core.p.e then
      core.regs.al <- core.regs.al - 1uy
      core.p.n <- core.regs.al >= 0x80uy
      core.p.z <- core.regs.al = 0x00uy
    else
      core.regs.a <- core.regs.a - 1us
      core.p.n <- core.regs.a >= 0x8000us
      core.p.z <- core.regs.a = 0x0000us
  )


  let opDexI = Instruction (fun io r w core ->
    if core.p.x || core.p.e then
      core.regs.xl <- core.regs.xl - 1uy
      core.p.n <- core.regs.xl >= 0x80uy
      core.p.z <- core.regs.xl = 0x00uy
    else
      core.regs.x <- core.regs.x - 1us
      core.p.n <- core.regs.x >= 0x8000us
      core.p.z <- core.regs.x = 0x0000us
  )


  let opDeyI = Instruction (fun io r w core ->
    if core.p.x || core.p.e then
      core.regs.yl <- core.regs.yl - 1uy
      core.p.n <- core.regs.yl >= 0x80uy
      core.p.z <- core.regs.yl = 0x00uy
    else
      core.regs.y <- core.regs.y - 1us
      core.p.n <- core.regs.y >= 0x8000us
      core.p.z <- core.regs.y = 0x0000us
  )


  let opIncA = Instruction (fun io r w core ->
    if core.p.m || core.p.e then
      core.regs.al <- core.regs.al + 1uy
      core.p.n <- core.regs.al >= 0x80uy
      core.p.z <- core.regs.al = 0x00uy
    else
      core.regs.a <- core.regs.a + 1us
      core.p.n <- core.regs.a >= 0x8000us
      core.p.z <- core.regs.a = 0x0000us
  )


  let opInxI = Instruction (fun io r w core ->
    if core.p.x || core.p.e then
      core.regs.xl <- core.regs.xl + 1uy
      core.p.n <- core.regs.xl >= 0x80uy
      core.p.z <- core.regs.xl = 0x00uy
    else
      core.regs.x <- core.regs.x + 1us
      core.p.n <- core.regs.x >= 0x8000us
      core.p.z <- core.regs.x = 0x0000us
  )


  let opInyI = Instruction (fun io r w core ->
    if core.p.x || core.p.e then
      core.regs.yl <- core.regs.yl + 1uy
      core.p.n <- core.regs.yl >= 0x80uy
      core.p.z <- core.regs.yl = 0x00uy
    else
      core.regs.y <- core.regs.y + 1us
      core.p.n <- core.regs.y >= 0x8000us
      core.p.z <- core.regs.y = 0x0000us
  )


  let opLsrA = Instruction (fun io r w core ->
    if core.p.m || core.p.e then
      core.regs.al <- lsr8 core.regs.al core
    else
      core.regs.a <- lsr16 core.regs.a core
  )


  let opNopI = Instruction (fun io r w core ->
    ()
  )


  let opPeaI = Instruction (fun io r w core ->
    Modes.amAbsW io r core

    Core.pushN w core.regs.aah core
    Core.lastCycle core
    Core.pushN w core.regs.aal core
  )


  let opPeiI = Instruction (fun io r w core ->
    Modes.amIndW io r core

    Core.pushN w core.regs.aah core
    Core.pushN w core.regs.aal core
  )


  let opPerI = Instruction (fun io r w core ->
    let rdl = Core.fetch r core
    let rdh = Core.fetch r core
    let rd  = Core.mk16 rdh rdl

    io()
    core.regs.aa <- core.regs.pc + rd
    Core.pushN w core.regs.aah core
    Core.pushN w core.regs.aal core
  )


  let opPhaI = Instruction (fun io r w core ->
    io()

    if core.p.m || core.p.e then
      Core.lastCycle core
      Core.pushN w core.regs.al core
    else
      Core.pushN w core.regs.ah core
      Core.lastCycle core
      Core.pushN w core.regs.al core
  )


  let opPhbI = Instruction (fun io r w core ->
    io()
    Core.lastCycle core
    Core.pushN w core.db core
  )


  let opPhdI = Instruction (fun io r w core ->
    io()
    Core.pushN w (uint8 (core.regs.dp >>> 8)) core
    Core.lastCycle core
    Core.pushN w (uint8 (core.regs.dp >>> 0)) core
  )


  let opPhkI = Instruction (fun io r w core ->
    io()
    Core.lastCycle core
    Core.pushN w core.regs.pb core
  )


  let opPhpI = Instruction (fun io r w core ->
    io()
    Core.lastCycle core
    Core.pushN w (Status.pack core.p) core
  )


  let opPhxI = Instruction (fun io r w core ->
    io()

    if core.p.x || core.p.e then
      Core.lastCycle core
      Core.pushN w core.regs.xl core
    else
      Core.pushN w core.regs.xh core
      Core.lastCycle core
      Core.pushN w core.regs.xl core
  )


  let opPhyI = Instruction (fun io r w core ->
    io()

    if core.p.x || core.p.e then
      Core.lastCycle core
      Core.pushN w core.regs.yl core
    else
      Core.pushN w core.regs.yh core
      Core.lastCycle core
      Core.pushN w core.regs.yl core
  )


  let opPlaI = Instruction (fun io r w core ->
    io()
    io()

    if core.p.m || core.p.e then
      Core.lastCycle core
      core.regs.al <- Core.pullN r core
      core.p.n <- core.regs.al >= 0x80uy
      core.p.z <- core.regs.al = 0x00uy
    else
      core.regs.al <- Core.pullN r core
      Core.lastCycle core
      core.regs.ah <- Core.pullN r core
      core.p.n <- core.regs.a >= 0x8000us
      core.p.z <- core.regs.a = 0x0000us
  )


  let opPlbI = Instruction (fun io r w core ->
    io()
    io()
    Core.lastCycle core

    core.db <- Core.pullN r core
    core.p.n <- core.db >= 0x80uy
    core.p.z <- core.db = 0x00uy
  )


  let opPldI = Instruction (fun io r w core ->
    io()
    io()
    let dpl = Core.pullN r core
    Core.lastCycle core
    let dph = Core.pullN r core

    core.regs.dp <- Core.mk16 dph dpl
    nz16 core.regs.dp core
  )


  let opPlpI = Instruction (fun io r w core ->
    io()
    io()
    Core.lastCycle core
    Status.unpack (Core.pullN r core) core.p

    if core.p.x || core.p.e then
      core.regs.xh <- 0uy
      core.regs.yh <- 0uy
  )


  let opPlxI = Instruction (fun io r w core ->
    io()
    io()

    if core.p.x || core.p.e then
      Core.lastCycle core
      core.regs.xl <- Core.pullN r core
      core.p.n <- core.regs.xl >= 0x80uy
      core.p.z <- core.regs.xl = 0x00uy
    else
      core.regs.xl <- Core.pullN r core
      Core.lastCycle core
      core.regs.xh <- Core.pullN r core
      core.p.n <- core.regs.x >= 0x8000us
      core.p.z <- core.regs.x = 0x0000us
  )


  let opPlyI = Instruction (fun io r w core ->
    io()
    io()

    if core.p.x || core.p.e then
      Core.lastCycle core
      core.regs.yl <- Core.pullN r core
      core.p.n <- core.regs.yl >= 0x80uy
      core.p.z <- core.regs.yl = 0x00uy
    else
      core.regs.yl <- Core.pullN r core
      Core.lastCycle core
      core.regs.yh <- Core.pullN r core
      core.p.n <- core.regs.y >= 0x8000us
      core.p.z <- core.regs.y = 0x0000us
  )


  let opRepI = Instruction (fun io r w core ->
    let data = int <| Core.fetch r core

    Core.lastCycle core
    io()

    if (data &&& 0x80) <> 0 then core.p.n <- false
    if (data &&& 0x40) <> 0 then core.p.v <- false
    if (data &&& 0x20) <> 0 then core.p.m <- false
    if (data &&& 0x10) <> 0 then core.p.x <- false
    if (data &&& 0x08) <> 0 then core.p.d <- false
    if (data &&& 0x04) <> 0 then core.p.i <- false
    if (data &&& 0x02) <> 0 then core.p.z <- false
    if (data &&& 0x01) <> 0 then core.p.c <- false
  )


  let opRolA = Instruction (fun io r w core ->
    if core.p.m || core.p.e then
      core.regs.al <- rol8 core.regs.al core
    else
      core.regs.a <- rol16 core.regs.a core
  )


  let opRorA = Instruction (fun io r w core ->
    if core.p.m || core.p.e then
      core.regs.al <- ror8 core.regs.al core
    else
      core.regs.a <- ror16 core.regs.a core
  )


  let opRtiI = Instruction (fun io r w core ->
    io()
    io()

    if core.p.e then
      Status.unpack (Core.pullN r core) core.p
      let pcl = Core.pullN r core
      Core.lastCycle core
      let pch = Core.pullN r core
      core.regs.pc <- Core.mk16 pch pcl
    else
      Status.unpack (Core.pullN r core) core.p
      let pcl = Core.pullN r core
      let pch = Core.pullN r core
      Core.lastCycle core
      core.regs.pb <- Core.pullN r core
      core.regs.pc <- Core.mk16 pch pcl
  )


  let opRtlI = Instruction (fun io r w core ->
    io()
    io()
    let pcl = Core.pullN r core
    let pch = Core.pullN r core
    Core.lastCycle core
    core.regs.pb <- Core.pullN r core
    core.regs.pc <- (Core.mk16 pch pcl) + 1us
  )


  let opRtsI = Instruction (fun io r w core ->
    io()
    io()
    let pcl = Core.pullN r core
    let pch = Core.pullN r core
    Core.lastCycle core
    io()
    core.regs.pc <- (Core.mk16 pch pcl) + 1us
  )


  let opSecI = Instruction (fun io r w core -> core.p.c <- true)
  let opSedI = Instruction (fun io r w core -> core.p.d <- true)
  let opSeiI = Instruction (fun io r w core -> core.p.i <- true)


  let opSepI = Instruction (fun io r w core ->
    let data = int <| Core.fetch r core

    Core.lastCycle core
    io()

    if (data &&& 0x80) <> 0 then core.p.n <- true
    if (data &&& 0x40) <> 0 then core.p.v <- true
    if (data &&& 0x20) <> 0 then core.p.m <- true
    if (data &&& 0x10) <> 0 then core.p.x <- true; core.regs.xh <- 0uy; core.regs.yh <- 0uy
    if (data &&& 0x08) <> 0 then core.p.d <- true
    if (data &&& 0x04) <> 0 then core.p.i <- true
    if (data &&& 0x02) <> 0 then core.p.z <- true
    if (data &&& 0x01) <> 0 then core.p.c <- true
  )


  let opTaxI = Instruction (fun io r w core ->
    if core.p.x || core.p.e then
      core.regs.xl <- core.regs.al
      core.p.n <- core.regs.xl >= 0x80uy
      core.p.z <- core.regs.xl = 0x00uy
    else
      core.regs.x <- core.regs.a
      core.p.n <- core.regs.x >= 0x8000us
      core.p.z <- core.regs.x = 0x0000us
  )


  let opTayI = Instruction (fun io r w core ->
    if core.p.x || core.p.e then
      core.regs.yl <- core.regs.al
      core.p.n <- core.regs.yl >= 0x80uy
      core.p.z <- core.regs.yl = 0x00uy
    else
      core.regs.y <- core.regs.a
      core.p.n <- core.regs.y >= 0x8000us
      core.p.z <- core.regs.y = 0x0000us
  )


  let opTcdI = Instruction (fun io r w core ->
    core.regs.dp <- core.regs.a
    core.p.n <- core.regs.dp >= 0x8000us
    core.p.z <- core.regs.dp = 0x0000us
  )


  let opTcsI = Instruction (fun io r w core ->
    core.regs.sp <- core.regs.a

    if core.p.e then
      core.regs.sph <- 1uy
  )


  let opTdcI = Instruction (fun io r w core ->
    core.regs.a <- core.regs.dp
    core.p.n <- core.regs.a >= 0x8000us
    core.p.z <- core.regs.a = 0x0000us
  )


  let opTscI = Instruction (fun io r w core ->
    core.regs.a <- core.regs.sp
    core.p.n <- core.regs.a >= 0x8000us
    core.p.z <- core.regs.a = 0x0000us
  )


  let opTsxI = Instruction (fun io r w core ->
    if core.p.x || core.p.e then
      core.regs.xl <- core.regs.spl
      core.p.n <- core.regs.xl >= 0x80uy
      core.p.z <- core.regs.xl = 0x00uy
    else
      core.regs.x <- core.regs.sp
      core.p.n <- core.regs.x >= 0x8000us
      core.p.z <- core.regs.x = 0x0000us
  )


  let opTxaI = Instruction (fun io r w core ->
    if core.p.m || core.p.e then
      core.regs.al <- core.regs.xl
      core.p.n <- core.regs.al >= 0x80uy
      core.p.z <- core.regs.al = 0x00uy
    else
      core.regs.a <- core.regs.x
      core.p.n <- core.regs.a >= 0x8000us
      core.p.z <- core.regs.a = 0x0000us
  )


  let opTxsI = Instruction (fun io r w core ->
    if core.p.e then
      core.regs.spl <- core.regs.xl
    else
      core.regs.sp <- core.regs.x
  )


  let opTxyI = Instruction (fun io r w core ->
    if core.p.x || core.p.e then
      core.regs.yl <- core.regs.xl
      core.p.n <- core.regs.yl >= 0x80uy
      core.p.z <- core.regs.yl = 0x00uy
    else
      core.regs.y <- core.regs.x
      core.p.n <- core.regs.y >= 0x8000us
      core.p.z <- core.regs.y = 0x0000us
  )


  let opTyaI = Instruction (fun io r w core ->
    if core.p.m || core.p.e then
      core.regs.al <- core.regs.yl
      core.p.n <- core.regs.al >= 0x80uy
      core.p.z <- core.regs.al = 0x00uy
    else
      core.regs.a <- core.regs.y
      core.p.n <- core.regs.a >= 0x8000us
      core.p.z <- core.regs.a = 0x0000us
  )


  let opTyxI = Instruction (fun io r w core ->
    if core.p.x || core.p.e then
      core.regs.xl <- core.regs.yl
      core.p.n <- core.regs.xl >= 0x80uy
      core.p.z <- core.regs.xl = 0x00uy
    else
      core.regs.x <- core.regs.y
      core.p.n <- core.regs.x >= 0x8000us
      core.p.z <- core.regs.x = 0x0000us
  )


  let opXbaI = Instruction (fun io r w core ->
    io()
    Core.lastCycle core
    io()

    core.regs.al <- core.regs.al ^^^ core.regs.ah
    core.regs.ah <- core.regs.ah ^^^ core.regs.al
    core.regs.al <- core.regs.al ^^^ core.regs.ah
    core.p.n <- core.regs.al >= 0x80uy
    core.p.z <- core.regs.al = 0x00uy
  )


  let opXceI = Instruction (fun io r w core ->
    let e = core.p.e
    core.p.e <- core.p.c
    core.p.c <- e
  )


  // Special purpose instructions

  let opJmpAbs = Instruction (fun io r w core ->
    let pcl = Core.fetch r core
    Core.lastCycle core
    let pch = Core.fetch r core
    core.regs.pc <- Core.mk16 pch pcl
  )


  let opJmpAbsLong = Instruction (fun io r w core ->
    let pcl = Core.fetch r core
    let pch = Core.fetch r core
    Core.lastCycle core
    core.regs.pb <- Core.fetch r core
    core.regs.pc <- Core.mk16 pch pcl
  )


  let opJmpAbsIndirect = Instruction (fun io r w core ->
    core.regs.aal <- Core.fetch r core
    core.regs.aah <- Core.fetch r core
    let pcl = Core.read r 0uy core.regs.aa
    core.regs.aa <- core.regs.aa + 1us
    let pch = Core.read r 0uy core.regs.aa
    core.regs.pc <- Core.mk16 pch pcl
  )


  let opJmpAbsIndirectLong = Instruction (fun io r w core ->
    core.regs.aal <- Core.fetch r core
    core.regs.aah <- Core.fetch r core
    let pcl = Core.read r 0uy (core.regs.aa + 0us)
    let pch = Core.read r 0uy (core.regs.aa + 1us)
    core.regs.pb <- Core.read r 0uy (core.regs.aa + 2us)
    core.regs.pc <- Core.mk16 pch pcl
  )


  let opJmpAbsXIndirect = Instruction (fun io r w core ->
    core.regs.aal <- Core.fetch r core
    core.regs.aah <- Core.fetch r core
    core.regs.aab <- core.regs.pb

    io()
    core.regs.aa <- core.regs.aa + core.regs.x

    let pcl = Core.read r core.regs.aab (core.regs.aa + 0us)
    Core.lastCycle core
    let pch = Core.read r core.regs.aab (core.regs.aa + 1us)

    core.regs.pc <- Core.mk16 pch pcl
  )


  let opJsrAbs = Instruction (fun io r w core ->
    core.regs.aal <- Core.read r core.regs.pb core.regs.pc; core.regs.pc <- core.regs.pc + 1us
    core.regs.aah <- Core.read r core.regs.pb core.regs.pc
    io()
    Core.pushN w (uint8 (core.regs.pc >>> 8)) core
    Core.lastCycle core
    Core.pushN w (uint8 (core.regs.pc >>> 0)) core
    core.regs.pc <- core.regs.aa
  )


  let opJsrLong = Instruction (fun io r w core ->
    core.regs.aal <- Core.fetch r core
    core.regs.aah <- Core.fetch r core
    Core.pushN w core.regs.pb core
    io()
    core.regs.pb <- Core.read r core.regs.pb core.regs.pc
    Core.pushN w (uint8 (core.regs.pc >>> 8)) core
    Core.lastCycle core
    Core.pushN w (uint8 (core.regs.pc >>> 0)) core
    core.regs.pc <- core.regs.aa
  )


  let opJsrAbsX = Instruction (fun io r w core ->
    core.regs.aal <- Core.fetch r core
    Core.push w (uint8 (core.regs.pc >>> 8)) core
    Core.push w (uint8 (core.regs.pc >>> 0)) core
    core.regs.aah <- Core.fetch r core

    io()
    core.regs.aa <- core.regs.aa + core.regs.x

    let pcl = Core.read r core.regs.pb (core.regs.aa + 0us)
    Core.lastCycle core
    let pch = Core.read r core.regs.pb (core.regs.aa + 1us)

    core.regs.pc <- Core.mk16 pch pcl
  )
