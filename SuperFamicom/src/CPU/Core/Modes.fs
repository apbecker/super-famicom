module SuperFamicom.CPU.Core.Modes

let mkAddress b h l =
  ((int b) <<< 16) |||
  ((int h) <<<  8) |||
  ((int l) <<<  0)


let amAbsW io r core =
  core.regs.aal <- Core.fetch r core
  core.regs.aah <- Core.fetch r core
  core.regs.aab <- core.db


let amAbxW io r core =
  let ial = Core.fetch r core
  let iah = Core.fetch r core
  let iab = core.db
  core.regs.aa24 <- (mkAddress iab iah ial) + (int core.regs.x)

  if not core.p.x || (core.regs.aah <> iah) || (core.code = 0x9duy || core.code = 0x9euy) then
    io()


let amAbxL io r core =
  core.regs.aal <- Core.fetch r core
  core.regs.aah <- Core.fetch r core
  core.regs.aab <- Core.fetch r core

  core.regs.aa24 <- core.regs.aa24 + (int core.regs.x)


let amAbyW io r core =
  let ial = Core.fetch r core
  let iah = Core.fetch r core
  let iab = core.db
  core.regs.aa24 <- (mkAddress iab iah ial) + (int core.regs.y)

  if not core.p.x || (core.regs.aah <> iah) || (core.code = 0x99uy) then
    io()


let amAbsL io r core =
  core.regs.aal <- Core.fetch r core
  core.regs.aah <- Core.fetch r core
  core.regs.aab <- Core.fetch r core


let amDpgW io r core =
  core.regs.aal <- Core.fetch r core
  core.regs.aah <- uint8 (core.regs.dp >>> 8)
  core.regs.aab <- 0x00uy

  let dpl = core.regs.dp &&& 0xffus
  if dpl <> 0us then
    io()
    core.regs.aa <- core.regs.aa + dpl


let amDpxW io r core =
  core |> amDpgW io r

  io()
  core.regs.aa <- core.regs.aa + core.regs.x


let amDpyW io r core =
  core |> amDpgW io r

  io()
  core.regs.aa <- core.regs.aa + core.regs.y


let amImpW io r core =
  core |> Core.lastCycle
  io()


let amIndW io r core =
  core |> amDpgW io r

  let ial = Core.read r core.regs.aab (core.regs.aa + 0us)
  let iah = Core.read r core.regs.aab (core.regs.aa + 1us)
  let iab = core.db
  core.regs.aa24 <- mkAddress iab iah ial


let amIndL io r core =
  core |> amDpgW io r

  let ial = Core.read r core.regs.aab (core.regs.aa + 0us)
  let iah = Core.read r core.regs.aab (core.regs.aa + 1us)
  let iab = Core.read r core.regs.aab (core.regs.aa + 2us)
  core.regs.aa24 <- mkAddress iab iah ial


let amInxW io r core =
  core |> amDpxW io r

  let ial = Core.read r core.regs.aab (core.regs.aa + 0us)
  let iah = Core.read r core.regs.aab (core.regs.aa + 1us)
  let iab = core.db
  core.regs.aa24 <- mkAddress iab iah ial


let amInyW io r core =
  core |> amDpgW io r

  let ial = Core.read r core.regs.aab (core.regs.aa + 0us)
  let iah = Core.read r core.regs.aab (core.regs.aa + 1us)
  let iab = core.db
  core.regs.aa24 <- (mkAddress iab iah ial) + (int core.regs.y)

  if not core.p.x || (core.regs.aah <> iah) || (core.code = 0x91uy) then
    io()


let amInyL io r core =
  core |> amDpgW io r

  let ial = Core.read r core.regs.aab (core.regs.aa + 0us)
  let iah = Core.read r core.regs.aab (core.regs.aa + 1us)
  let iab = Core.read r core.regs.aab (core.regs.aa + 2us)
  core.regs.aa24 <- (mkAddress iab iah ial) + (int core.regs.y)


let amSprW io r core =
  core.regs.aal <- Core.fetch r core
  core.regs.aah <- 0x00uy
  core.regs.aab <- 0x00uy

  io()
  core.regs.aa <- core.regs.aa + core.regs.sp


let amSpyW io r core =
  core |> amSprW io r

  let ial = Core.read r core.regs.aab (core.regs.aa + 0us)
  let iah = Core.read r core.regs.aab (core.regs.aa + 1us)
  let iab = core.db

  io()
  core.regs.aa24 <- (mkAddress iab iah ial) + (int core.regs.y)
