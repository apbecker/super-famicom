namespace SuperFamicom.CPU.Core

type Core =
  { regs: Registers
    p: Status
    mutable code: byte
    mutable db: byte
    mutable interrupt: bool
    mutable irq: bool
    mutable nmi: bool }

module Core =

  open SuperFamicom

  let init () =
    { regs = Registers.init ()
      p = Status.init ()
      code = 0uy
      db = 0uy
      interrupt = false
      irq = false
      nmi = false }


  let read r bank address =
    Reader.run r bank address 0uy


  let write w bank address data =
    Writer.run w bank address data


  let flagE core = core.p.e
  let flagN core = core.p.n
  let flagV core = core.p.v
  let flagM core = core.p.m
  let flagX core = core.p.x
  let flagD core = core.p.d
  let flagI core = core.p.i
  let flagZ core = core.p.z
  let flagC core = core.p.c


  let lastCycle core =
    core.interrupt <- core.nmi || (core.irq && not core.p.i)


  let fetch r core : uint8 =
    let data = read r core.regs.pb core.regs.pc
    core.regs.pc <- core.regs.pc + 1us
    data


  let readImmediate8 code r core : unit =
    lastCycle core
    let data = fetch r core
    code data core


  let mk16 h l =
    ((uint16 h) <<< 8) |||
    ((uint16 l) <<< 0)


  let readImmediate16 code r core : unit =
    let lower = fetch r core
    lastCycle core
    let upper = fetch r core

    code (mk16 upper lower) core


  let reset r core =
    let pcl = read r 0uy 0xfffcus
    let pch = read r 0uy 0xfffdus
    core.regs.pc <- mk16 pch pcl
    core.regs.pb <- 0uy
    core.regs.sp <- 0x1ffus

    printfn "Reset vector: %02x:%04x"
      <| core.regs.pb
      <| core.regs.pc

    core.p.e <- true
    core.p.m <- true
    core.p.x <- true
    core.p.d <- false
    core.p.i <- true


  let pull r core =
    core.regs.sp <- core.regs.sp + 1us
    read r 0uy core.regs.sp


  let pullN r core =
    core.regs.sp <- core.regs.sp + 1us
    let data = read r 0uy core.regs.sp

    if core.p.e then
      core.regs.sph <- 1uy

    data


  let push w data core =
    write w 0uy core.regs.sp data
    core.regs.sp <- core.regs.sp - 1us


  let pushN w data core =
    write w 0uy core.regs.sp data
    core.regs.sp <- core.regs.sp - 1us

    if core.p.e then
      core.regs.sph <- 1uy


  let isr r w vector core =
    read r core.regs.pb core.regs.pc |> ignore
    read r core.regs.pb core.regs.pc |> ignore

    if not core.p.e then
      core |> push w core.regs.pb

    let flag = Status.pack core.p
    let flag = byte (if core.p.e then flag &&& (~~~0x10uy) else flag)
    core.p.d <- false
    core.p.i <- true

    core |> pushN w (uint8 (core.regs.pc >>> 8))
    core |> pushN w (uint8 (core.regs.pc >>> 0))
    core |> pushN w flag

    let pcl = read r 0uy (vector + 0us)
    let pch = read r 0uy (vector + 1us)

    core.regs.pb <- 0uy
    core.regs.pc <- mk16 pch pcl


  let irq core =
    core.irq <- true


  let nmi core =
    core.nmi <- true
