module SuperFamicom.CPU.Core.Core2

let [<Literal>] NmiE = 0xfffaus
let [<Literal>] NmiN = 0xffeaus
let [<Literal>] IrqE = 0xfffeus
let [<Literal>] IrqN = 0xffeeus


let getCode io r w core =
  match core.code with
  | 0x00uy (* BRK #$nn       *) -> (*                   *) Codes.opErrI
  | 0x01uy (* ORA ($nn,x)    *) -> Modes.amInxW io r core; Codes.opOraM
  | 0x02uy (* COP #$nn       *) -> (*                   *) Codes.opErrI
  | 0x03uy (* ORA $nn,s      *) -> Modes.amSprW io r core; Codes.opOraM
  | 0x04uy (* TSB $nn        *) -> Modes.amDpgW io r core; Codes.opTsbM
  | 0x05uy (* ORA $nn        *) -> Modes.amDpgW io r core; Codes.opOraM
  | 0x06uy (* ASL $nn        *) -> Modes.amDpgW io r core; Codes.opAslM
  | 0x07uy (* ORA [$nn]      *) -> Modes.amIndL io r core; Codes.opOraM
  | 0x08uy (* PHP            *) -> (*                   *) Codes.opPhpI
  | 0x09uy (* ORA #$nnnn     *) -> (*                   *) Codes.opOraN
  | 0x0auy (* ASL A          *) -> Modes.amImpW io r core; Codes.opAslA
  | 0x0buy (* PHD            *) -> (*                   *) Codes.opPhdI
  | 0x0cuy (* TSB $nnnn      *) -> Modes.amAbsW io r core; Codes.opTsbM
  | 0x0duy (* ORA $nnnn      *) -> Modes.amAbsW io r core; Codes.opOraM
  | 0x0euy (* ASL $nnnn      *) -> Modes.amAbsW io r core; Codes.opAslM
  | 0x0fuy (* ORA $nn:nnnn   *) -> Modes.amAbsL io r core; Codes.opOraM
  | 0x10uy (* BPL #$nn       *) -> (*                   *) Codes.opBplI
  | 0x11uy (* ORA ($nn),y    *) -> Modes.amInyW io r core; Codes.opOraM
  | 0x12uy (* ORA ($nn)      *) -> Modes.amIndW io r core; Codes.opOraM
  | 0x13uy (* ORA ($nn,s),y  *) -> Modes.amSpyW io r core; Codes.opOraM
  | 0x14uy (* TRB $nn        *) -> Modes.amDpgW io r core; Codes.opTrbM
  | 0x15uy (* ORA $nn,x      *) -> Modes.amDpxW io r core; Codes.opOraM
  | 0x16uy (* ASL $nn,x      *) -> Modes.amDpxW io r core; Codes.opAslM
  | 0x17uy (* ORA [$nn],y    *) -> Modes.amInyL io r core; Codes.opOraM
  | 0x18uy (* CLC            *) -> Modes.amImpW io r core; Codes.opClcI
  | 0x19uy (* ORA $nnnn,y    *) -> Modes.amAbyW io r core; Codes.opOraM
  | 0x1auy (* INC A          *) -> Modes.amImpW io r core; Codes.opIncA
  | 0x1buy (* TCS            *) -> Modes.amImpW io r core; Codes.opTcsI
  | 0x1cuy (* TRB $nnnn      *) -> Modes.amAbsW io r core; Codes.opTrbM
  | 0x1duy (* ORA $nnnn,x    *) -> Modes.amAbxW io r core; Codes.opOraM
  | 0x1euy (* ASL $nnnn,x    *) -> Modes.amAbxW io r core; Codes.opAslM
  | 0x1fuy (* ORA $nn:nnnn,x *) -> Modes.amAbxL io r core; Codes.opOraM
  | 0x20uy (* JSR $nnnn      *) -> (*                   *) Codes.opJsrAbs
  | 0x21uy (* AND ($nn,x)    *) -> Modes.amInxW io r core; Codes.opAndM
  | 0x22uy (* JSR $nn:nnnn   *) -> (*                   *) Codes.opJsrLong
  | 0x23uy (* AND $nn,s      *) -> Modes.amSprW io r core; Codes.opAndM
  | 0x24uy (* BIT $nn        *) -> Modes.amDpgW io r core; Codes.opBitM
  | 0x25uy (* AND $nn        *) -> Modes.amDpgW io r core; Codes.opAndM
  | 0x26uy (* ROL $nn        *) -> Modes.amDpgW io r core; Codes.opRolM
  | 0x27uy (* AND [$nn]      *) -> Modes.amIndL io r core; Codes.opAndM
  | 0x28uy (* PLP            *) -> (*                   *) Codes.opPlpI
  | 0x29uy (* AND #$nnnn     *) -> (*                   *) Codes.opAndN
  | 0x2auy (* ROL A          *) -> Modes.amImpW io r core; Codes.opRolA
  | 0x2buy (* PLD            *) -> (*                   *) Codes.opPldI
  | 0x2cuy (* BIT $nnnn      *) -> Modes.amAbsW io r core; Codes.opBitM
  | 0x2duy (* AND $nnnn      *) -> Modes.amAbsW io r core; Codes.opAndM
  | 0x2euy (* ROL $nnnn      *) -> Modes.amAbsW io r core; Codes.opRolM
  | 0x2fuy (* AND $nn:nnnn   *) -> Modes.amAbsL io r core; Codes.opAndM
  | 0x30uy (* BMI #$nn       *) -> (*                   *) Codes.opBmiI
  | 0x31uy (* AND ($nn),y    *) -> Modes.amInyW io r core; Codes.opAndM
  | 0x32uy (* AND ($nn)      *) -> Modes.amIndW io r core; Codes.opAndM
  | 0x33uy (* AND ($nn,s),y  *) -> Modes.amSpyW io r core; Codes.opAndM
  | 0x34uy (* BIT $nn,x      *) -> Modes.amDpxW io r core; Codes.opBitM
  | 0x35uy (* AND $nn,x      *) -> Modes.amDpxW io r core; Codes.opAndM
  | 0x36uy (* ROL $nn,x      *) -> Modes.amDpxW io r core; Codes.opRolM
  | 0x37uy (* AND [$nn],y    *) -> Modes.amInyL io r core; Codes.opAndM
  | 0x38uy (* SEC            *) -> Modes.amImpW io r core; Codes.opSecI
  | 0x39uy (* AND $nnnn,y    *) -> Modes.amAbyW io r core; Codes.opAndM
  | 0x3auy (* DEC A          *) -> Modes.amImpW io r core; Codes.opDecA
  | 0x3buy (* TSC            *) -> Modes.amImpW io r core; Codes.opTscI
  | 0x3cuy (* BIT $nnnn,x    *) -> Modes.amAbxW io r core; Codes.opBitM
  | 0x3duy (* AND $nnnn,x    *) -> Modes.amAbxW io r core; Codes.opAndM
  | 0x3euy (* ROL $nnnn,x    *) -> Modes.amAbxW io r core; Codes.opRolM
  | 0x3fuy (* AND $nn:nnnn,x *) -> Modes.amAbxL io r core; Codes.opAndM
  | 0x40uy (* RTI            *) -> (*                   *) Codes.opRtiI
  | 0x41uy (* EOR ($nn,x)    *) -> Modes.amInxW io r core; Codes.opEorM
  | 0x42uy (* WDM            *) -> (*                   *) Codes.opErrI
  | 0x43uy (* EOR $nn,s      *) -> Modes.amSprW io r core; Codes.opEorM
  | 0x44uy (* MVP #$nn, #$nn *) -> (*                   *) Codes.opMvpI
  | 0x45uy (* EOR $nn        *) -> Modes.amDpgW io r core; Codes.opEorM
  | 0x46uy (* LSR $nn        *) -> Modes.amDpgW io r core; Codes.opLsrM
  | 0x47uy (* EOR [$nn]      *) -> Modes.amIndL io r core; Codes.opEorM
  | 0x48uy (* PHA            *) -> (*                   *) Codes.opPhaI
  | 0x49uy (* EOR #$nnnn     *) -> (*                   *) Codes.opEorN
  | 0x4auy (* LSR A          *) -> Modes.amImpW io r core; Codes.opLsrA
  | 0x4buy (* PHK            *) -> (*                   *) Codes.opPhkI
  | 0x4cuy (* JMP $nnnn      *) -> (*                   *) Codes.opJmpAbs
  | 0x4duy (* EOR $nnnn      *) -> Modes.amAbsW io r core; Codes.opEorM
  | 0x4euy (* LSR $nnnn      *) -> Modes.amAbsW io r core; Codes.opLsrM
  | 0x4fuy (* EOR $nn:nnnn   *) -> Modes.amAbsL io r core; Codes.opEorM
  | 0x50uy (* BVC #$nn       *) -> (*                   *) Codes.opBvcI
  | 0x51uy (* EOR ($nn),y    *) -> Modes.amInyW io r core; Codes.opEorM
  | 0x52uy (* EOR ($nn)      *) -> Modes.amIndW io r core; Codes.opEorM
  | 0x53uy (* EOR ($nn,s),y  *) -> Modes.amSpyW io r core; Codes.opEorM
  | 0x54uy (* MVN #$nn, #$nn *) -> (*                   *) Codes.opMvnI
  | 0x55uy (* EOR $nn,x      *) -> Modes.amDpxW io r core; Codes.opEorM
  | 0x56uy (* LSR $nn,x      *) -> Modes.amDpxW io r core; Codes.opLsrM
  | 0x57uy (* EOR [$nn],y    *) -> Modes.amInyL io r core; Codes.opEorM
  | 0x58uy (* CLI            *) -> Modes.amImpW io r core; Codes.opCliI
  | 0x59uy (* EOR $nnnn,y    *) -> Modes.amAbyW io r core; Codes.opEorM
  | 0x5auy (* PHY            *) -> (*                   *) Codes.opPhyI
  | 0x5buy (* TCD            *) -> Modes.amImpW io r core; Codes.opTcdI
  | 0x5cuy (* JMP $nn:nnnn   *) -> (*                   *) Codes.opJmpAbsLong
  | 0x5duy (* EOR $nnnn,x    *) -> Modes.amAbxW io r core; Codes.opEorM
  | 0x5euy (* LSR $nnnn,x    *) -> Modes.amAbxW io r core; Codes.opLsrM
  | 0x5fuy (* EOR $nn:nnnn,x *) -> Modes.amAbxL io r core; Codes.opEorM
  | 0x60uy (* RTS            *) -> (*                   *) Codes.opRtsI
  | 0x61uy (* ADC ($nn,x)    *) -> Modes.amInxW io r core; Codes.opAdcM
  | 0x62uy (* PER #$nnnn     *) -> (*                   *) Codes.opPerI
  | 0x63uy (* ADC $nn,s      *) -> Modes.amSprW io r core; Codes.opAdcM
  | 0x64uy (* STZ $nn        *) -> Modes.amDpgW io r core; Codes.opStzM
  | 0x65uy (* ADC $nn        *) -> Modes.amDpgW io r core; Codes.opAdcM
  | 0x66uy (* ROR $nn        *) -> Modes.amDpgW io r core; Codes.opRorM
  | 0x67uy (* ADC [$nn]      *) -> Modes.amIndL io r core; Codes.opAdcM
  | 0x68uy (* PLA            *) -> (*                   *) Codes.opPlaI
  | 0x69uy (* ADC #$nnnn     *) -> (*                   *) Codes.opAdcN
  | 0x6auy (* ROR A          *) -> Modes.amImpW io r core; Codes.opRorA
  | 0x6buy (* RTL            *) -> (*                   *) Codes.opRtlI
  | 0x6cuy (* JMP ($nnnn)    *) -> (*                   *) Codes.opJmpAbsIndirect
  | 0x6duy (* ADC $nnnn      *) -> Modes.amAbsW io r core; Codes.opAdcM
  | 0x6euy (* ROR $nnnn      *) -> Modes.amAbsW io r core; Codes.opRorM
  | 0x6fuy (* ADC $nn:nnnn   *) -> Modes.amAbsL io r core; Codes.opAdcM
  | 0x70uy (* BVS #$nn       *) -> (*                   *) Codes.opBvsI
  | 0x71uy (* ADC ($nn),y    *) -> Modes.amInyW io r core; Codes.opAdcM
  | 0x72uy (* ADC ($nn)      *) -> Modes.amIndW io r core; Codes.opAdcM
  | 0x73uy (* ADC ($nn,s),y  *) -> Modes.amSpyW io r core; Codes.opAdcM
  | 0x74uy (* STZ $nn,x      *) -> Modes.amDpxW io r core; Codes.opStzM
  | 0x75uy (* ADC $nn,x      *) -> Modes.amDpxW io r core; Codes.opAdcM
  | 0x76uy (* ROR $nn,x      *) -> Modes.amDpxW io r core; Codes.opRorM
  | 0x77uy (* ADC [$nn],y    *) -> Modes.amInyL io r core; Codes.opAdcM
  | 0x78uy (* SEI            *) -> Modes.amImpW io r core; Codes.opSeiI
  | 0x79uy (* ADC $nnnn,y    *) -> Modes.amAbyW io r core; Codes.opAdcM
  | 0x7auy (* PLY            *) -> (*                   *) Codes.opPlyI
  | 0x7buy (* TDC            *) -> Modes.amImpW io r core; Codes.opTdcI
  | 0x7cuy (* JMP ($nnnn,x)  *) -> (*                   *) Codes.opJmpAbsXIndirect
  | 0x7duy (* ADC $nnnn,x    *) -> Modes.amAbxW io r core; Codes.opAdcM
  | 0x7euy (* ROR $nnnn,x    *) -> Modes.amAbxW io r core; Codes.opRorM
  | 0x7fuy (* ADC $nn:nnnn,x *) -> Modes.amAbxL io r core; Codes.opAdcM
  | 0x80uy (* BRA #$nn       *) -> (*                   *) Codes.opBraI
  | 0x81uy (* STA ($nn,x)    *) -> Modes.amInxW io r core; Codes.opStaM
  | 0x82uy (* BRL #$nnnn     *) -> (*                   *) Codes.opBrlI
  | 0x83uy (* STA $nn,s      *) -> Modes.amSprW io r core; Codes.opStaM
  | 0x84uy (* STY $nn        *) -> Modes.amDpgW io r core; Codes.opStyX
  | 0x85uy (* STA $nn        *) -> Modes.amDpgW io r core; Codes.opStaM
  | 0x86uy (* STX $nn        *) -> Modes.amDpgW io r core; Codes.opStxX
  | 0x87uy (* STA [$nn]      *) -> Modes.amIndL io r core; Codes.opStaM
  | 0x88uy (* DEY            *) -> Modes.amImpW io r core; Codes.opDeyI
  | 0x89uy (* BIT #$nnnn     *) -> (*                   *) Codes.opTstN
  | 0x8auy (* TXA            *) -> Modes.amImpW io r core; Codes.opTxaI
  | 0x8buy (* PHB            *) -> (*                   *) Codes.opPhbI
  | 0x8cuy (* STY $nnnn      *) -> Modes.amAbsW io r core; Codes.opStyX
  | 0x8duy (* STA $nnnn      *) -> Modes.amAbsW io r core; Codes.opStaM
  | 0x8euy (* STX $nnnn      *) -> Modes.amAbsW io r core; Codes.opStxX
  | 0x8fuy (* STA $nn:nnnn   *) -> Modes.amAbsL io r core; Codes.opStaM
  | 0x90uy (* BCC            *) -> (*                   *) Codes.opBccI
  | 0x91uy (* STA ($nn),y    *) -> Modes.amInyW io r core; Codes.opStaM
  | 0x92uy (* STA ($nn)      *) -> Modes.amIndW io r core; Codes.opStaM
  | 0x93uy (* STA ($nn,s),y  *) -> Modes.amSpyW io r core; Codes.opStaM
  | 0x94uy (* STY $nn,x      *) -> Modes.amDpxW io r core; Codes.opStyX
  | 0x95uy (* STA $nn,x      *) -> Modes.amDpxW io r core; Codes.opStaM
  | 0x96uy (* STX $nn,y      *) -> Modes.amDpyW io r core; Codes.opStxX
  | 0x97uy (* STA [$nn],y    *) -> Modes.amInyL io r core; Codes.opStaM
  | 0x98uy (* TYA            *) -> Modes.amImpW io r core; Codes.opTyaI
  | 0x99uy (* STA $nnnn,y    *) -> Modes.amAbyW io r core; Codes.opStaM
  | 0x9auy (* TXS            *) -> Modes.amImpW io r core; Codes.opTxsI
  | 0x9buy (* TXY            *) -> Modes.amImpW io r core; Codes.opTxyI
  | 0x9cuy (* STZ $nnnn      *) -> Modes.amAbsW io r core; Codes.opStzM
  | 0x9duy (* STA $nnnn,x    *) -> Modes.amAbxW io r core; Codes.opStaM
  | 0x9euy (* STZ $nnnn,x    *) -> Modes.amAbxW io r core; Codes.opStzM
  | 0x9fuy (* STA $nn:nnnn,x *) -> Modes.amAbxL io r core; Codes.opStaM
  | 0xa0uy (* LDY #$nnnn     *) -> (*                   *) Codes.opLdyN
  | 0xa1uy (* LDA ($nn,x)    *) -> Modes.amInxW io r core; Codes.opLdaM
  | 0xa2uy (* LDX #$nnnn     *) -> (*                   *) Codes.opLdxN
  | 0xa3uy (* LDA $nn,s      *) -> Modes.amSprW io r core; Codes.opLdaM
  | 0xa4uy (* LDY $nn        *) -> Modes.amDpgW io r core; Codes.opLdyX
  | 0xa5uy (* LDA $nn        *) -> Modes.amDpgW io r core; Codes.opLdaM
  | 0xa6uy (* LDX $nn        *) -> Modes.amDpgW io r core; Codes.opLdxX
  | 0xa7uy (* LDA [$nn]      *) -> Modes.amIndL io r core; Codes.opLdaM
  | 0xa8uy (* TAY            *) -> Modes.amImpW io r core; Codes.opTayI
  | 0xa9uy (* LDA #$nnnn     *) -> (*                   *) Codes.opLdaN
  | 0xaauy (* TAX            *) -> Modes.amImpW io r core; Codes.opTaxI
  | 0xabuy (* PLB            *) -> (*                   *) Codes.opPlbI
  | 0xacuy (* LDA $nnnn      *) -> Modes.amAbsW io r core; Codes.opLdyX
  | 0xaduy (* LDA $nnnn      *) -> Modes.amAbsW io r core; Codes.opLdaM
  | 0xaeuy (* LDX $nnnn      *) -> Modes.amAbsW io r core; Codes.opLdxX
  | 0xafuy (* LDA $nn:nnnn   *) -> Modes.amAbsL io r core; Codes.opLdaM
  | 0xb0uy (* BCS #$nn       *) -> (*                   *) Codes.opBcsI
  | 0xb1uy (* LDA ($nn),y    *) -> Modes.amInyW io r core; Codes.opLdaM
  | 0xb2uy (* LDA ($nn)      *) -> Modes.amIndW io r core; Codes.opLdaM
  | 0xb3uy (* LDA ($nn,s),y  *) -> Modes.amSpyW io r core; Codes.opLdaM
  | 0xb4uy (* LDY $nn,x      *) -> Modes.amDpxW io r core; Codes.opLdyX
  | 0xb5uy (* LDA $nn,x      *) -> Modes.amDpxW io r core; Codes.opLdaM
  | 0xb6uy (* LDX $nn,y      *) -> Modes.amDpyW io r core; Codes.opLdxX
  | 0xb7uy (* LDA [$nn],y    *) -> Modes.amInyL io r core; Codes.opLdaM
  | 0xb8uy (* CLV            *) -> Modes.amImpW io r core; Codes.opClvI
  | 0xb9uy (* LDA $nnnn,y    *) -> Modes.amAbyW io r core; Codes.opLdaM
  | 0xbauy (* TSX            *) -> Modes.amImpW io r core; Codes.opTsxI
  | 0xbbuy (* TYX            *) -> Modes.amImpW io r core; Codes.opTyxI
  | 0xbcuy (* LDY $nnnn,x    *) -> Modes.amAbxW io r core; Codes.opLdyX
  | 0xbduy (* LDA $nnnn,x    *) -> Modes.amAbxW io r core; Codes.opLdaM
  | 0xbeuy (* LDX $nnnn,y    *) -> Modes.amAbyW io r core; Codes.opLdxX
  | 0xbfuy (* LDA $nn:nnnn,x *) -> Modes.amAbxL io r core; Codes.opLdaM
  | 0xc0uy (* CPY #$nnnn     *) -> (*                   *) Codes.opCpyN
  | 0xc1uy (* CMP ($nn,x)    *) -> Modes.amInxW io r core; Codes.opCmpM
  | 0xc2uy (* REP #$nn       *) -> (*                   *) Codes.opRepI
  | 0xc3uy (* CMP $nn,s      *) -> Modes.amSprW io r core; Codes.opCmpM
  | 0xc4uy (* CPY $nn        *) -> Modes.amDpgW io r core; Codes.opCpyX
  | 0xc5uy (* CMP $nn        *) -> Modes.amDpgW io r core; Codes.opCmpM
  | 0xc6uy (* DEC $nn        *) -> Modes.amDpgW io r core; Codes.opDecM
  | 0xc7uy (* CMP [$nn]      *) -> Modes.amIndL io r core; Codes.opCmpM
  | 0xc8uy (* INY            *) -> Modes.amImpW io r core; Codes.opInyI
  | 0xc9uy (* CMP #$nnnn     *) -> (*                   *) Codes.opCmpN
  | 0xcauy (* DEX            *) -> Modes.amImpW io r core; Codes.opDexI
  | 0xcbuy (* WAI            *) -> (*                   *) Codes.opErrI
  | 0xccuy (* CPY $nnnn      *) -> Modes.amAbsW io r core; Codes.opCpyX
  | 0xcduy (* CMP $nnnn      *) -> Modes.amAbsW io r core; Codes.opCmpM
  | 0xceuy (* DEC $nnnn      *) -> Modes.amAbsW io r core; Codes.opDecM
  | 0xcfuy (* CMP $nn:nnnn   *) -> Modes.amAbsL io r core; Codes.opCmpM
  | 0xd0uy (* BNE #$nn       *) -> (*                   *) Codes.opBneI
  | 0xd1uy (* CMP ($nn),y    *) -> Modes.amInyW io r core; Codes.opCmpM
  | 0xd2uy (* CMP ($nn)      *) -> Modes.amIndW io r core; Codes.opCmpM
  | 0xd3uy (* CMP ($nn,s),y  *) -> Modes.amSpyW io r core; Codes.opCmpM
  | 0xd4uy (* PEI            *) -> (*                   *) Codes.opPeiI
  | 0xd5uy (* CMP $nn,x      *) -> Modes.amDpxW io r core; Codes.opCmpM
  | 0xd6uy (* DEC $nn,x      *) -> Modes.amDpxW io r core; Codes.opDecM
  | 0xd7uy (* CMP [$nn],y    *) -> Modes.amInyL io r core; Codes.opCmpM
  | 0xd8uy (* CLD            *) -> Modes.amImpW io r core; Codes.opCldI
  | 0xd9uy (* CMP $nnnn,y    *) -> Modes.amAbyW io r core; Codes.opCmpM
  | 0xdauy (* PHX            *) -> (*                   *) Codes.opPhxI
  | 0xdbuy (* STP            *) -> (*                   *) Codes.opErrI
  | 0xdcuy (* JMP [$nnnn]    *) -> (*                   *) Codes.opJmpAbsIndirectLong
  | 0xdduy (* CMP $nnnn,x    *) -> Modes.amAbxW io r core; Codes.opCmpM
  | 0xdeuy (* DEC $nnnn,x    *) -> Modes.amAbxW io r core; Codes.opDecM
  | 0xdfuy (* CMP $nn:nnnn,x *) -> Modes.amAbxL io r core; Codes.opCmpM
  | 0xe0uy (* CPX #$nnnn     *) -> (*                   *) Codes.opCpxN
  | 0xe1uy (* SBC ($nn,x)    *) -> Modes.amInxW io r core; Codes.opSbcM
  | 0xe2uy (* SEP #$nn       *) -> (*                   *) Codes.opSepI
  | 0xe3uy (* SBC $nn,s      *) -> Modes.amSprW io r core; Codes.opSbcM
  | 0xe4uy (* CPX $nn        *) -> Modes.amDpgW io r core; Codes.opCpxX
  | 0xe5uy (* SBC $nn        *) -> Modes.amDpgW io r core; Codes.opSbcM
  | 0xe6uy (* INC $nn        *) -> Modes.amDpgW io r core; Codes.opIncM
  | 0xe7uy (* SBC [$nn]      *) -> Modes.amIndL io r core; Codes.opSbcM
  | 0xe8uy (* INX            *) -> Modes.amImpW io r core; Codes.opInxI
  | 0xe9uy (* SBC #$nnnn     *) -> (*                   *) Codes.opSbcN
  | 0xeauy (* NOP            *) -> Modes.amImpW io r core; Codes.opNopI
  | 0xebuy (* XBA            *) -> (*                   *) Codes.opXbaI
  | 0xecuy (* CPX $nnnn      *) -> Modes.amAbsW io r core; Codes.opCpxX
  | 0xeduy (* SBC $nnnn      *) -> Modes.amAbsW io r core; Codes.opSbcM
  | 0xeeuy (* INC $nnnn      *) -> Modes.amAbsW io r core; Codes.opIncM
  | 0xefuy (* SBC $nn:nnnn   *) -> Modes.amAbsL io r core; Codes.opSbcM
  | 0xf0uy (* BEQ #$nn       *) -> (*                   *) Codes.opBeqI
  | 0xf1uy (* SBC ($nn),y    *) -> Modes.amInyW io r core; Codes.opSbcM
  | 0xf2uy (* SBC ($nn)      *) -> Modes.amIndW io r core; Codes.opSbcM
  | 0xf3uy (* SBC ($nn,s),y  *) -> Modes.amSpyW io r core; Codes.opSbcM
  | 0xf4uy (* PEA $nnnn      *) -> (*                   *) Codes.opPeaI
  | 0xf5uy (* SBC $nn,x      *) -> Modes.amDpxW io r core; Codes.opSbcM
  | 0xf6uy (* INC $nn,x      *) -> Modes.amDpxW io r core; Codes.opIncM
  | 0xf7uy (* SBC [$nn],y    *) -> Modes.amInyL io r core; Codes.opSbcM
  | 0xf8uy (* SED            *) -> Modes.amImpW io r core; Codes.opSedI
  | 0xf9uy (* SBC $nnnn,y    *) -> Modes.amAbyW io r core; Codes.opSbcM
  | 0xfauy (* PLX            *) -> (*                   *) Codes.opPlxI
  | 0xfbuy (* XCE            *) -> Modes.amImpW io r core; Codes.opXceI
  | 0xfcuy (* JSR ($nnnn,x)  *) -> (*                   *) Codes.opJsrAbsX
  | 0xfduy (* SBC $nnnn,x    *) -> Modes.amAbxW io r core; Codes.opSbcM
  | 0xfeuy (* INC $nnnn,x    *) -> Modes.amAbxW io r core; Codes.opIncM
  | 0xffuy (* SBC $nn:nnnn,x *) -> Modes.amAbxL io r core; Codes.opSbcM


let step io r w core =
  core.code <- Core.fetch r core

  let (Instruction(code)) = getCode io r w core
  code io r w core

  if core.interrupt then
    if core.nmi then
      core.nmi <- false
      Core.isr r w (if core.p.e then NmiE else NmiN) core
    else
      if core.irq && not core.p.i then
        core.irq <- false
        Core.isr r w (if core.p.e then IrqE else IrqN) core
