namespace SuperFamicom.CPU.Core

open System.Runtime.InteropServices

#nowarn "9"

[<StructLayout(LayoutKind.Explicit)>]
type Registers =
  {
    [<FieldOffset(0x00)>] mutable al: uint8
    [<FieldOffset(0x01)>] mutable ah: uint8

    [<FieldOffset(0x02)>] mutable xl: uint8
    [<FieldOffset(0x03)>] mutable xh: uint8

    [<FieldOffset(0x04)>] mutable yl: uint8
    [<FieldOffset(0x05)>] mutable yh: uint8

    [<FieldOffset(0x06)>] mutable dpl: uint8
    [<FieldOffset(0x07)>] mutable dph: uint8

    [<FieldOffset(0x08)>] mutable spl: uint8
    [<FieldOffset(0x09)>] mutable sph: uint8

    [<FieldOffset(0x0e)>] mutable pb: uint8

    [<FieldOffset(0x10)>] mutable aal: uint8
    [<FieldOffset(0x11)>] mutable aah: uint8
    [<FieldOffset(0x12)>] mutable aab: uint8
    [<FieldOffset(0x13)>] mutable aa_: uint8

    [<FieldOffset(0x00)>] mutable a: uint16
    [<FieldOffset(0x02)>] mutable x: uint16
    [<FieldOffset(0x04)>] mutable y: uint16
    [<FieldOffset(0x06)>] mutable dp: uint16
    [<FieldOffset(0x08)>] mutable sp: uint16
    [<FieldOffset(0x0c)>] mutable pc: uint16
    [<FieldOffset(0x10)>] mutable aa: uint16

    [<FieldOffset(0x10)>] mutable aa24: int32 }

module Registers =

  let init () =
    { al = 0uy
      ah = 0uy

      xl = 0uy
      xh = 0uy

      yl = 0uy
      yh = 0uy

      dpl = 0uy
      dph = 0uy

      spl = 0uy
      sph = 0uy

      pb = 0uy

      aal = 0uy
      aah = 0uy
      aab = 0uy
      aa_ = 0uy

      a = 0us
      x = 0us
      y = 0us
      dp = 0us
      sp = 0us
      pc = 0us
      aa = 0us

      aa24 = 0 }
