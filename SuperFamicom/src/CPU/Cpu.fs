namespace SuperFamicom.CPU

open SuperFamicom
open SuperFamicom.CPU.Core

type Cpu =
  { core: Core
    dma: Dma
    
    mutable inHBlank: bool
    mutable inVBlank: bool
    mutable timerCoincidence: bool

    mutable dramPrescaler: int
    mutable dramTimer: int
    mutable timePrescaler: int
    mutable fastCart: bool

    mutable h: int
    mutable hTarget: int
    mutable v: int
    mutable vTarget: int
    mutable reg4200: int

    // multiply regs
    mutable wrmpya: byte
    mutable wrmpyb: byte
    mutable rdmpy: uint16

    // divide regs
    mutable wrdiv: uint16
    mutable wrdivb: byte
    mutable rddiv: uint16

    mutable oldNmi: bool
    mutable open': uint8
    mutable totalCycles: int<CpuCycle> }

module Cpu =

  let mutable cycles = 0<CpuCycle>


  let init () =
    { core = Core.init ()

      inHBlank = false
      inVBlank = false
      timerCoincidence = false

      dramPrescaler = 8
      dramTimer = 538
      timePrescaler = 4
      fastCart = false

      h = 0
      hTarget = 0
      v = 0
      vTarget = 0
      reg4200 = 0

      // multiply regs
      wrmpya = 0uy
      wrmpyb = 0uy
      rdmpy = 0us

      // divide regs
      wrdiv = 0us
      wrdivb = 0uy
      rddiv = 0us

      oldNmi = false
      open' = 0uy
      totalCycles = 0<CpuCycle>

      dma = Dma.init () }


  let getSpeed (bank: byte) (address: uint16) scpu =
    let addr = ((int bank) <<< 16) ||| (int address)

    if ((addr &&& 0x408000) <> 0) then
      if ((addr &&& 0x800000) <> 0 && scpu.fastCart) then
        CpuCycle.Fast
      else
        CpuCycle.Norm
    else
      match addr with
      | addr when ((addr + 0x6000) &&& 0x4000) <> 0 -> CpuCycle.Norm
      | addr when ((addr - 0x4000) &&& 0x7e00) <> 0 -> CpuCycle.Fast
      | _ -> CpuCycle.Slow


  let read reader bank address scpu =
    cycles <- cycles + (scpu |> getSpeed bank address)

    reader bank address (ref scpu.open')

    scpu.open'


  let write writer bank address data scpu =
    cycles <- cycles + (scpu |> getSpeed bank address)

    scpu.open' <- data

    writer bank address scpu.open'


  let tick scpu =
    scpu.dramPrescaler <- scpu.dramPrescaler - 1

    if scpu.dramPrescaler = 0 then
      scpu.dramPrescaler <- 8
      scpu.dramTimer <- scpu.dramTimer - 8

      if (scpu.dramTimer &&& (~~~7)) = 0 then
        scpu.dramTimer <- scpu.dramTimer + 1364
        cycles <- cycles + 40<CpuCycle>

    scpu.timePrescaler <- scpu.timePrescaler - 1

    if scpu.timePrescaler = 0 then
      scpu.timePrescaler <- 4
      scpu.h <- scpu.h + 1

      if scpu.h = 341 then
        scpu.h <- 0
        scpu.v <- scpu.v + 1

        if scpu.v = 262 then
          scpu.v <- 0

      let hCoincidence = scpu.h = scpu.hTarget
      let vCoincidence = scpu.v = scpu.vTarget
      let type' = (scpu.reg4200 >>> 4) &&& 3

      match (type', hCoincidence, vCoincidence) with
      | (1, true,    _)
      | (2,    _, true)
      | (3, true, true) ->
        scpu.timerCoincidence <- true
        scpu.core |> Core.irq

      | _ ->
        ()


  let clock reader writer scpu =
    if scpu.dma.mdmaCount <> 0 then
      scpu.dma.mdmaCount <- scpu.dma.mdmaCount - 1
      if scpu.dma.mdmaCount = 0 && scpu.dma.mdmaEn <> 0uy then
        let time = scpu.dma |> Dma.run reader writer scpu.totalCycles

        // TODO: I don't think this does whatever it was intended
        //       to do..

        // cycles +=
        //     cycles - (time % cycles)

        cycles <- cycles + time

        scpu.dma.mdmaEn <- 0uy

    for _ in 1 .. (cycles / 1<_>) do
      tick scpu

    scpu.totalCycles <- scpu.totalCycles + cycles


  let update reader writer scpu =
    cycles <- 0<CpuCycle>

    scpu.core |> Core2.step (fun () -> cycles <- cycles + CpuCycle.Fast) reader writer
    scpu |> clock reader writer

    cycles


  let nmiWrapper enable scpu =
    let newNmi = scpu.inVBlank && enable
    if newNmi && (not scpu.oldNmi) then
      scpu.core |> Core.nmi

    scpu.oldNmi <- newNmi


  let hblank inside scpu =
    scpu.inHBlank <- inside


  let vblank inside scpu =
    scpu.inVBlank <- inside
    scpu |> nmiWrapper ((scpu.reg4200 &&& 0x80) <> 0)


  let reset r scpu =
    Core.reset r scpu.core
