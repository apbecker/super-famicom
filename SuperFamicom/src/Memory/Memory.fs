namespace SuperFamicom.Memory

[<Struct>]
type Memory =
  | Ram of Ram:(byte array * int)
  | Rom of Rom:(byte array * int)

module Memory =

  let private nextPowerOfTwo number =
    let copyBits x n =
      n ||| (n >>> x)

    let addOne n = n + 1
    let subOne n = n - 1

    number
      |> subOne
      |> copyBits 1
      |> copyBits 2
      |> copyBits 4
      |> copyBits 8
      |> copyBits 16
      |> addOne


  let rom buffer =
    Rom (buffer, buffer.Length - 1)


  let ram capacity =
    let buffer =
      capacity
        |> nextPowerOfTwo
        |> Array.zeroCreate

    Ram (buffer, buffer.Length - 1)


  let get8 memory address =
    match memory with
    | Ram (buffer, mask) -> buffer.[address &&& mask]
    | Rom (buffer, mask) -> buffer.[address &&& mask]


  let put8 memory address data =
    match memory with
    | Ram (buffer, mask) -> buffer.[address &&& mask] <- data
    | Rom _ ->
      ()


  let get16 memory address =
    let address = address &&& (~~~1)
    let a = uint16 <| get8 memory (address + 0)
    let b = uint16 <| get8 memory (address + 1)

    (b <<< 8) ||| a


  let put16 memory address (data: uint16) =
    let address = address &&& (~~~1)
    let a = uint8 (data >>> 0)
    let b = uint8 (data >>> 8)

    put8 memory (address + 0) a
    put8 memory (address + 1) b
