namespace SuperFamicom.Memory

type WRAM =
  { array: byte[]
    mutable index: int }

module WRAM =

  open SuperFamicom


  let [<Literal>] WramSize = 0x20000
  let [<Literal>] WramGarbage = 0x55uy


  let init () =
    { array = Array.init WramSize (fun _ -> WramGarbage)
      index = 0 }


  let private makeAddress bank addr =
    let bank = int bank
    let addr = int addr
    ((bank <<< 16) &&& 0x10000) ||| addr


  let read wram = Reader (fun bank address _ ->
    wram.array.[makeAddress bank address]
  )


  let write wram = Writer (fun bank address data ->
    wram.array.[makeAddress bank address] <- data
  )


  let autoRead wram =
    let data = wram.array.[wram.index]
    wram.index <- (wram.index + 1) &&& 0x1ffff

    data


  let autoWrite data wram =
    wram.array.[wram.index] <- data
    wram.index <- (wram.index + 1) &&& 0x1ffff
