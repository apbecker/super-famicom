namespace SuperFamicom.PPU

open SuperFamicom
open SuperFamicom.Memory
open SuperFamicom.Util

type Ppu =
  { m7: Mode7
    bg0: Background
    bg1: Background
    bg2: Background
    bg3: Background
    clr: Layer
    spr: Sprite
    window1: Window
    window2: Window

    mutable cgramData: byte // latch for cgram
    mutable cgramAddress: int
    mutable oamData: byte // latch for oam
    mutable oamAddress: int
    mutable vramData: byte // latch for vram
    mutable vramAddress: int

    cram: Memory
    oam: Memory
    vram0: byte[]
    vram1: byte[]

    mutable vramControl: byte
    mutable vramStep: byte

    mutable forceBlank: bool
    mutable interlace: bool
    mutable overscan: bool
    mutable pseudoHi: bool
    mutable mathEnable: bool[]
    mutable ppu1Open: byte
    mutable ppu2Open: byte
    mutable ppu1Stat: byte
    mutable ppu2Stat: byte
    mutable forceMainToBlack: int
    mutable fixedColor: int
    mutable brightness: int
    mutable hLatchToggle: bool
    mutable vLatchToggle: bool
    mutable hlatch: int
    mutable vlatch: int
    mutable hclock: int
    mutable vclock: int
    mutable colors: int[]
    mutable colorMathEnabled: int
    mutable mathType: int
    mutable product: int
    mutable cycles: int<CpuCycle * PpuCycle>
    mutable bgPriority: bool
    mutable bgMode: int
    mutable bgMosaicSize: int
    mutable bgOffsetLatch: int }

module Ppu =

  let colorLookup = [|
    for brightness in 0 .. 15 do
      yield [|
        for colour in 0 .. 32767 do
          let mutable r = (colour <<< 3) &&& 0xf8
          let mutable g = (colour >>> 2) &&& 0xf8
          let mutable b = (colour >>> 7) &&& 0xf8

          // apply gradient to lower bits (this will make black=$000000 and white=$ffffff)
          r <- r ||| (r >>> 5)
          g <- g ||| (g >>> 5)
          b <- b ||| (b >>> 5)

          r <- (r * brightness) / 15
          g <- (g * brightness) / 15
          b <- (b * brightness) / 15

          yield ((r <<< 16) ||| (g <<< 8) ||| b)
      |]
  |]


  let init () =
    { m7 = Mode7.init ()
      bg0 = Background.init 0
      bg1 = Background.init 1
      bg2 = Background.init 2
      bg3 = Background.init 3
      window1 = Window.init ()
      window2 = Window.init ()
      bgPriority = false
      bgMode = 0
      bgMosaicSize = 0
      bgOffsetLatch = 0
      forceBlank = false
      interlace = false
      overscan = false
      pseudoHi = false
      mathEnable = Array.zeroCreate 6
      ppu1Open = 0uy
      ppu2Open = 0uy
      ppu1Stat = 1uy
      ppu2Stat = 2uy
      forceMainToBlack = 0
      fixedColor = 0
      brightness = 0
      hLatchToggle = false
      vLatchToggle = false
      hlatch = 0
      vlatch = 0
      hclock = 0
      vclock = 0
      colors = colorLookup.[0]
      colorMathEnabled = 0
      mathType = 0
      product = 0
      cycles = 0<_>
      clr = Layer.init 0
      spr = Sprite.init ()

      cgramData = 0uy // latch for cgram
      cgramAddress = 0
      oamData = 0uy // latch for oam
      oamAddress = 0
      vramData = 0uy // latch for vram
      vramAddress = 0

      cram = Memory.ram 0x0200 // 256x15-bit
      oam = Memory.ram 0x0220 // 256x16-bit + 16x16-bit
      vram0 = Array.zeroCreate 0x8000
      vram1 = Array.zeroCreate 0x8000

      vramControl = 0uy
      vramStep = 1uy
    }


  let priorityLut = [|
    [| [| 8; 11 |]; [| 7; 10 |]; [| 2;  5 |]; [| 1; 4 |]; [| 3; 6; 9; 12 |] |] // mode 0
    [| [| 6;  9 |]; [| 5;  8 |]; [| 1;  3 |]; [| 0; 0 |]; [| 2; 4; 7; 10 |] |] // mode 1
    [| [| 0;  0 |]; [| 0;  0 |]; [| 0;  0 |]; [| 0; 0 |]; [| 0; 0; 0;  0 |] |]
    [| [| 0;  0 |]; [| 0;  0 |]; [| 0;  0 |]; [| 0; 0 |]; [| 0; 0; 0;  0 |] |]
    [| [| 0;  0 |]; [| 0;  0 |]; [| 0;  0 |]; [| 0; 0 |]; [| 0; 0; 0;  0 |] |]
    [| [| 0;  0 |]; [| 0;  0 |]; [| 0;  0 |]; [| 0; 0 |]; [| 0; 0; 0;  0 |] |]
    [| [| 0;  0 |]; [| 0;  0 |]; [| 0;  0 |]; [| 0; 0 |]; [| 0; 0; 0;  0 |] |]
    [| [| 2;  3 |]; [| 0;  0 |]; [| 0;  0 |]; [| 0; 0 |]; [| 1; 4; 5;  6 |] |]
    [| [| 5;  8 |]; [| 4;  7 |]; [| 1; 10 |]; [| 0; 0 |]; [| 2; 3; 6;  9 |] |] // mode 1 priority
  |]


  let writeHOffset (bg: Background) (data: byte) ppu =
    let data = int data

    bg.hOffset <- (data <<< 8) ||| (ppu.bgOffsetLatch &&& (~~~7)) ||| ((bg.hOffset >>> 8) &&& 7)
    ppu.bgOffsetLatch <- data


  let writeVOffset (bg: Background) (data: byte) ppu =
    let data = int data

    bg.vOffset <- (data <<< 8) ||| ppu.bgOffsetLatch
    ppu.bgOffsetLatch <- data


  let m7a ppu =
    int32 (int16 (ppu.m7.a >>> 8))


  let m7b ppu =
    int32 (int8 (ppu.m7.b >>> 8))


  let read2134 ppu =
    ppu.product <- (m7a ppu) * (m7b ppu)
    ppu.ppu1Open <- ((byte)(ppu.product >>> 0))
    ppu.ppu1Open


  let read2135 ppu =
    ppu.product <- (m7a ppu) * (m7b ppu)
    ppu.ppu1Open <- ((byte)(ppu.product >>> 8))
    ppu.ppu1Open


  let read2136 ppu =
    ppu.product <- (m7a ppu) * (m7b ppu)
    ppu.ppu1Open <- ((byte)(ppu.product >>> 16))
    ppu.ppu1Open


  let read2137 ppu =
    ppu.hlatch <- ppu.hclock
    ppu.vlatch <- ppu.vclock
    0uy


  let read213C ppu =
    ppu.hLatchToggle <- not ppu.hLatchToggle

    ppu.ppu2Open <-
      if ppu.hLatchToggle then
        (ppu.ppu2Open &&& 0x00uy) ||| (uint8 (ppu.hlatch >>> 0))
      else
        (ppu.ppu2Open &&& 0xfeuy) ||| (uint8 (ppu.hlatch >>> 8))

    ppu.ppu2Open


  let read213D ppu =
    ppu.vLatchToggle <- not ppu.vLatchToggle

    ppu.ppu2Open <-
      if ppu.vLatchToggle then
        (ppu.ppu2Open &&& 0x00uy) ||| (uint8 (ppu.vlatch >>> 0))
      else
        (ppu.ppu2Open &&& 0xfeuy) ||| (uint8 (ppu.vlatch >>> 8))

    ppu.ppu2Open


  let read213E ppu =
    ppu.ppu1Stat


  let read213F ppu =
    let data = ppu.ppu2Stat

    ppu.hLatchToggle <- false
    ppu.vLatchToggle <- false

    data


  let write2100 (data: byte) ppu =
    let data = int data
    ppu.forceBlank <- (data &&& 0x80) <> 0
    ppu.brightness <- (data &&& 0x0f)

    ppu.colors <- colorLookup.[ppu.brightness]


  let write2101 (data: byte) ppu =
    let data = int data
    ppu.spr.addr <- (data &&& 0x07) <<< 13
    ppu.spr.name <- (data &&& 0x18) <<< 9
    ppu.spr.name <- ppu.spr.name + 0x1000
    ppu.spr.shape <- SpriteShape.ofPrimitive (data >>> 5)


  let write2105 (data: byte) ppu =
    let data = int data
    ppu.bgMode <- (data &&& 0x07)
    ppu.bgPriority <- (data &&& 0x08) <> 0
    ppu.bg0.charSize <- if (data &&& 0x10) <> 0 then 16 else 8
    ppu.bg1.charSize <- if (data &&& 0x20) <> 0 then 16 else 8
    ppu.bg2.charSize <- if (data &&& 0x40) <> 0 then 16 else 8
    ppu.bg3.charSize <- if (data &&& 0x80) <> 0 then 16 else 8

    let mutable table = priorityLut.[ppu.bgMode]

    if ppu.bgMode = 1 && ppu.bgPriority then
      table <- priorityLut.[8]

    ppu.bg0.layer.priorities <- table.[0]
    ppu.bg1.layer.priorities <- table.[1]
    ppu.bg2.layer.priorities <- table.[2]
    ppu.bg3.layer.priorities <- table.[3]
    ppu.spr.layer.priorities <- table.[4]


  let write2106 (data: byte) ppu =
    let data = int data
    ppu.bg0.mosaic <- (data &&& 0x01) <> 0
    ppu.bg1.mosaic <- (data &&& 0x02) <> 0
    ppu.bg2.mosaic <- (data &&& 0x04) <> 0
    ppu.bg3.mosaic <- (data &&& 0x08) <> 0

    ppu.bgMosaicSize <- ((data &&& 0xf0) >>> 4)


  let write2107 (data: byte) ppu =
    let data = int data
    ppu.bg0.nameSize <- (data &&& 0x03)
    ppu.bg0.nameBase <- (data &&& 0x7c) <<< 8


  let write2108 (data: byte) ppu =
    let data = int data
    ppu.bg1.nameSize <- (data &&& 0x03)
    ppu.bg1.nameBase <- (data &&& 0x7c) <<< 8


  let write2109 (data: byte) ppu =
    let data = int data
    ppu.bg2.nameSize <- (data &&& 0x03)
    ppu.bg2.nameBase <- (data &&& 0x7c) <<< 8


  let write210A (data: byte) ppu =
    let data = int data
    ppu.bg3.nameSize <- (data &&& 0x03)
    ppu.bg3.nameBase <- (data &&& 0x7c) <<< 8


  let write210B (data: byte) ppu =
    let data = int data
    ppu.bg0.charBase <- (data &&& 0x07) <<< 12
    ppu.bg1.charBase <- (data &&& 0x70) <<< 8


  let write210C (data: byte) ppu =
    let data = int data
    ppu.bg2.charBase <- (data &&& 0x07) <<< 12
    ppu.bg3.charBase <- (data &&& 0x70) <<< 8


  let write210D (data: byte) ppu =
    writeHOffset ppu.bg0 data ppu

    ppu.m7.hOffset <- uint16 ((data <<< 8) ||| ppu.m7.latch)
    ppu.m7.latch <- data


  let write210E (data: byte) ppu =
    writeVOffset ppu.bg0 data ppu

    ppu.m7.vOffset <- uint16 ((data <<< 8) ||| ppu.m7.latch)
    ppu.m7.latch <- data


  let write210F (data: byte) ppu =
    writeHOffset ppu.bg1 data ppu


  let write2110 (data: byte) ppu =
    writeVOffset ppu.bg1 data ppu


  let write2111 (data: byte) ppu =
    writeHOffset ppu.bg2 data ppu


  let write2112 (data: byte) ppu =
    writeVOffset ppu.bg2 data ppu


  let write2113 (data: byte) ppu =
    writeHOffset ppu.bg3 data ppu


  let write2114 (data: byte) ppu =
    writeVOffset ppu.bg3 data ppu


  let write2115 (data: byte) ppu =
    ppu.vramControl <- data

    match (int ppu.vramControl) &&& 3 with
    | 0 -> ppu.vramStep <- 0x01uy
    | 1 -> ppu.vramStep <- 0x20uy
    | 2 -> ppu.vramStep <- 0x80uy
    | _ -> ppu.vramStep <- 0x80uy


  let write211A (data: byte) ppu =
    ppu.m7.control <- data


  let write211B (data: byte) ppu =
    ppu.m7.a <- uint16 ((data <<< 8) ||| ppu.m7.latch)
    ppu.m7.latch <- data


  let write211C (data: byte) ppu =
    ppu.m7.b <- uint16 ((data <<< 8) ||| ppu.m7.latch)
    ppu.m7.latch <- data


  let write211D (data: byte) ppu =
    ppu.m7.c <- uint16 ((data <<< 8) ||| ppu.m7.latch)
    ppu.m7.latch <- data


  let write211E (data: byte) ppu =
    ppu.m7.d <- uint16 ((data <<< 8) ||| ppu.m7.latch)
    ppu.m7.latch <- data


  let write211F (data: byte) ppu =
    ppu.m7.x <- uint16 ((data <<< 8) ||| ppu.m7.latch)
    ppu.m7.latch <- data


  let write2120 (data: byte) ppu =
    ppu.m7.y <- uint16 ((data <<< 8) ||| ppu.m7.latch)
    ppu.m7.latch <- data


  let write2123 (data: byte) ppu =
    let data = int data

    ppu.bg0.layer.window1Inverted <- (data &&& 0x01) <> 0
    ppu.bg0.layer.window1Enable   <- (data &&& 0x02) <> 0
    ppu.bg0.layer.window2Inverted <- (data &&& 0x04) <> 0
    ppu.bg0.layer.window2Enable   <- (data &&& 0x08) <> 0

    ppu.bg1.layer.window1Inverted <- (data &&& 0x10) <> 0
    ppu.bg1.layer.window1Enable   <- (data &&& 0x20) <> 0
    ppu.bg1.layer.window2Inverted <- (data &&& 0x40) <> 0
    ppu.bg1.layer.window2Enable   <- (data &&& 0x80) <> 0


  let write2124 (data: byte) ppu =
    let data = int data

    ppu.bg2.layer.window1Inverted <- (data &&& 0x01) <> 0
    ppu.bg2.layer.window1Enable   <- (data &&& 0x02) <> 0
    ppu.bg2.layer.window2Inverted <- (data &&& 0x04) <> 0
    ppu.bg2.layer.window2Enable   <- (data &&& 0x08) <> 0

    ppu.bg3.layer.window1Inverted <- (data &&& 0x10) <> 0
    ppu.bg3.layer.window1Enable   <- (data &&& 0x20) <> 0
    ppu.bg3.layer.window2Inverted <- (data &&& 0x40) <> 0
    ppu.bg3.layer.window2Enable   <- (data &&& 0x80) <> 0


  let write2125 (data: byte) ppu =
    let data = int data

    ppu.spr.layer.window1Inverted <- (data &&& 0x01) <> 0
    ppu.spr.layer.window1Enable   <- (data &&& 0x02) <> 0
    ppu.spr.layer.window2Inverted <- (data &&& 0x04) <> 0
    ppu.spr.layer.window2Enable   <- (data &&& 0x08) <> 0

    ppu.clr.window1Inverted <- (data &&& 0x10) <> 0
    ppu.clr.window1Enable   <- (data &&& 0x20) <> 0
    ppu.clr.window2Inverted <- (data &&& 0x40) <> 0
    ppu.clr.window2Enable   <- (data &&& 0x80) <> 0


  let write2126 (data: byte) ppu =
    ppu.window1.x1 <- data


  let write2127 (data: byte) ppu =
    ppu.window1.x2 <- data


  let write2128 (data: byte) ppu =
    ppu.window2.x1 <- data


  let write2129 (data: byte) ppu =
    ppu.window2.x2 <- data


  let write212A (data: byte) ppu =
    let data = int data

    ppu.bg0.layer.windowLogic <- (data >>> 0) &&& 3
    ppu.bg1.layer.windowLogic <- (data >>> 2) &&& 3
    ppu.bg2.layer.windowLogic <- (data >>> 4) &&& 3
    ppu.bg3.layer.windowLogic <- (data >>> 6) &&& 3


  let write212B (data: byte) ppu =
    let data = int data

    ppu.spr.layer.windowLogic <- (data >>> 0) &&& 3
    ppu.clr.windowLogic <- (data >>> 2) &&& 3


  let write212C (data: byte) ppu =
    let data = int data

    ppu.bg0.layer.screenMain <- if (data &&& 0x01) <> 0 then (~~~0) else 0
    ppu.bg1.layer.screenMain <- if (data &&& 0x02) <> 0 then (~~~0) else 0
    ppu.bg2.layer.screenMain <- if (data &&& 0x04) <> 0 then (~~~0) else 0
    ppu.bg3.layer.screenMain <- if (data &&& 0x08) <> 0 then (~~~0) else 0
    ppu.spr.layer.screenMain <- if (data &&& 0x10) <> 0 then (~~~0) else 0


  let write212D (data: byte) ppu =
    let data = int data

    ppu.bg0.layer.screenSub <- if (data &&& 0x01) <> 0 then (~~~0) else 0
    ppu.bg1.layer.screenSub <- if (data &&& 0x02) <> 0 then (~~~0) else 0
    ppu.bg2.layer.screenSub <- if (data &&& 0x04) <> 0 then (~~~0) else 0
    ppu.bg3.layer.screenSub <- if (data &&& 0x08) <> 0 then (~~~0) else 0
    ppu.spr.layer.screenSub <- if (data &&& 0x10) <> 0 then (~~~0) else 0


  let write212E (data: byte) ppu =
    let data = int data

    ppu.bg0.layer.windowMain <- (data &&& 0x01) <> 0
    ppu.bg1.layer.windowMain <- (data &&& 0x02) <> 0
    ppu.bg2.layer.windowMain <- (data &&& 0x04) <> 0
    ppu.bg3.layer.windowMain <- (data &&& 0x08) <> 0
    ppu.spr.layer.windowMain <- (data &&& 0x10) <> 0


  let write212F (data: byte) ppu =
    let data = int data

    ppu.bg0.layer.windowSub <- (data &&& 0x01) <> 0
    ppu.bg1.layer.windowSub <- (data &&& 0x02) <> 0
    ppu.bg2.layer.windowSub <- (data &&& 0x04) <> 0
    ppu.bg3.layer.windowSub <- (data &&& 0x08) <> 0
    ppu.spr.layer.windowSub <- (data &&& 0x10) <> 0


  let write2130 (data: byte) ppu =
    let data = int data

    ppu.forceMainToBlack <- (data &&& 0xc0) >>> 6
    ppu.colorMathEnabled <- (data &&& 0x30) >>> 4


  let write2131 (data: byte) ppu =
    let data = int data

    ppu.mathEnable.[0] <- (data &&& 0x01) <> 0
    ppu.mathEnable.[1] <- (data &&& 0x02) <> 0
    ppu.mathEnable.[2] <- (data &&& 0x04) <> 0
    ppu.mathEnable.[3] <- (data &&& 0x08) <> 0
    ppu.mathEnable.[4] <- (data &&& 0x10) <> 0
    ppu.mathEnable.[5] <- (data &&& 0x20) <> 0
    ppu.mathType <- (data >>> 6) &&& 3


  let write2132 (data: byte) ppu =
    let data = int data

    if (data &&& 0x80) <> 0 then ppu.fixedColor <- (ppu.fixedColor &&& (~~~0x7c00)) ||| ((data &&& 0x1f) <<< 10)
    if (data &&& 0x40) <> 0 then ppu.fixedColor <- (ppu.fixedColor &&& (~~~0x03e0)) ||| ((data &&& 0x1f) <<< 5)
    if (data &&& 0x20) <> 0 then ppu.fixedColor <- (ppu.fixedColor &&& (~~~0x001f)) ||| ((data &&& 0x1f) <<< 0)


  let write2133 (data: byte) ppu =
    let data = int data
    // todo: mode7 extbg bit
    ppu.pseudoHi <- (data &&& 0x08) <> 0
    ppu.overscan <- (data &&& 0x04) <> 0
    ppu.spr.interlace <- (data &&& 0x02) <> 0
    ppu.interlace <- (data &&& 0x01) <> 0


  let mapVRamAddress (address: int) ppu =
    match ((int ppu.vramControl) >>> 2) &&& 3 with
    | 0 -> (address &&& 0x7fff)
    | 1 -> (address &&& 0x7f00) ||| ((address &&& 0x00e0) >>> 5) ||| ((address &&& 0x001f) <<< 3)
    | 2 -> (address &&& 0x7e00) ||| ((address &&& 0x01c0) >>> 6) ||| ((address &&& 0x003f) <<< 3)
    | _ -> (address &&& 0x7c00) ||| ((address &&& 0x0380) >>> 7) ||| ((address &&& 0x007f) <<< 3)


  let read213B ppu =
    let data = Memory.get8 ppu.cram ppu.cgramAddress

    ppu.cgramAddress <- (ppu.cgramAddress + 1) &&& 0x1ff

    uint8 data


  let read2138 ppu =
    let data =
      if ppu.oamAddress > 0x1ff then
        Memory.get8 ppu.oam (ppu.oamAddress &&& 0x21f)
      else
        Memory.get8 ppu.oam (ppu.oamAddress &&& 0x1ff)

    ppu.oamAddress <- (ppu.oamAddress + 1) &&& 0x3ff

    uint8 data


  let read2139 ppu =
    let data = ppu.vramData
    ppu.vramData <- ppu.vram0.[mapVRamAddress ppu.vramAddress ppu]

    if (ppu.vramControl &&& 0x80uy) = 0uy then
      ppu.vramAddress <- ppu.vramAddress + (int ppu.vramStep)

    ppu.ppu1Open <- data
    ppu.ppu1Open


  let read213A ppu =
    let data = ppu.vramData
    ppu.vramData <- ppu.vram1.[mapVRamAddress ppu.vramAddress ppu]

    if (ppu.vramControl &&& 0x80uy) <> 0uy then
      ppu.vramAddress <- ppu.vramAddress + (int ppu.vramStep)

    ppu.ppu1Open <- data
    ppu.ppu1Open


  let write2121 (data: byte) ppu =
    ppu.cgramAddress <- (int data) <<< 1


  let write2122 (data: byte) ppu =
    if (ppu.cgramAddress &&& 1) = 0 then
      ppu.cgramData <- data
    else
      Memory.put8 ppu.cram (ppu.cgramAddress &&& 0x1fe) ppu.cgramData
      Memory.put8 ppu.cram (ppu.cgramAddress &&& 0x1ff) (data &&& 0x7fuy)

    ppu.cgramAddress <- (ppu.cgramAddress + 1) &&& 0x1ff


  let write2102 (data: byte) ppu =
    ppu.oamAddress <- (ppu.oamAddress &&& 0x200) ||| (((int data) <<< 1) &&& 0x1fe)


  let write2103 (data: byte) ppu =
    ppu.oamAddress <- (ppu.oamAddress &&& 0x1fe) ||| (((int data) <<< 9) &&& 0x200)


  let write2104 (data: byte) ppu =
    if (ppu.oamAddress &&& 1) = 0 then
      ppu.oamData <- data

    if ppu.oamAddress > 0x1ff then
      Memory.put8 ppu.oam (ppu.oamAddress &&& 0x21f) data
    else
      if (ppu.oamAddress &&& 1) <> 0 then
        Memory.put8 ppu.oam (ppu.oamAddress &&& 0x1fe) ppu.oamData
        Memory.put8 ppu.oam (ppu.oamAddress &&& 0x1ff) data

    ppu.oamAddress <- (ppu.oamAddress + 1) &&& 0x3ff


  let write2116 (data: byte) ppu =
    ppu.vramAddress <- (ppu.vramAddress &&& 0xff00) ||| ((int data) <<< 0)


  let write2117 (data: byte) ppu =
    ppu.vramAddress <- (ppu.vramAddress &&& 0x00ff) ||| ((int data) <<< 8)


  let write2118 (data: byte) ppu =
    let address = mapVRamAddress ppu.vramAddress ppu

    ppu.vram0.[address] <- data

    if (ppu.vramControl &&& 0x80uy) = 0uy then
      ppu.vramAddress <- ppu.vramAddress + (int ppu.vramStep)


  let write2119 (data: byte) ppu =
    let address = mapVRamAddress ppu.vramAddress ppu

    ppu.vram1.[address] <- data

    if (ppu.vramControl &&& 0x80uy) <> 0uy then
      ppu.vramAddress <- ppu.vramAddress + (int ppu.vramStep)


  let readCram16 (address: int) ppu =
    Memory.get16 ppu.cram (address * 2)


  let blend video ppu =
    let layers = [|
      ppu.bg0.layer
      ppu.bg1.layer
      ppu.bg2.layer
      ppu.bg3.layer
      ppu.spr.layer
    |]

    for x in 0 .. 255 do
      let mutable color1 = int <| readCram16 0 ppu
      let mutable color2 = int <| ppu.fixedColor
      let mutable priority1 = 0
      let mutable priority2 = 0
      let mutable source1 = 5
      let mutable source2 = 5

      for i in 0 .. 4 do
        let layer = layers.[i]

        if layer.enable.[x] then
          if layer.screenMain <> 0 && priority1 < layer.priority.[x] then
            priority1 <- layer.priority.[x]
            source1 <- i

          if layer.screenSub <> 0 && priority2 < layer.priority.[x] then
            priority2 <- layer.priority.[x]
            source2 <- i

      let colorexempt = (source1 = 4 && (color1 < 0xc0))

      if source1 <> 5 then color1 <- int <| readCram16 (layers.[source1].raster.[x]) ppu
      if source2 <> 5 then color2 <- int <| readCram16 (layers.[source2].raster.[x]) ppu

      if not colorexempt && ppu.mathEnable.[source1] then
        let mutable r1 = (color1 >>>  0) &&& 31
        let mutable g1 = (color1 >>>  5) &&& 31
        let mutable b1 = (color1 >>> 10) &&& 31
        let mutable r2 = (color2 >>>  0) &&& 31
        let mutable g2 = (color2 >>>  5) &&& 31
        let mutable b2 = (color2 >>> 10) &&& 31

        match ppu.mathType with
        | 0 ->
          r1 <- r1 + r2
          g1 <- g1 + g2
          b1 <- b1 + b2

        | 1 ->
          r1 <- (r1 + r2) / 2
          g1 <- (g1 + g2) / 2
          b1 <- (b1 + b2) / 2

        | 2 ->
          r1 <- r1 - r2
          g1 <- g1 - g2
          b1 <- b1 - b2

        | _ ->
          r1 <- (r1 - r2) / 2
          g1 <- (g1 - g2) / 2
          b1 <- (b1 - b2) / 2

        if r1 > 31 then r1 <- 31
        if g1 > 31 then g1 <- 31
        if b1 > 31 then b1 <- 31
        if r1 < 0 then r1 <- 0
        if g1 < 0 then g1 <- 0
        if b1 < 0 then b1 <- 0

        color1 <- (r1 <<< 0) ||| (g1 <<< 5) ||| (b1 <<< 10)

      VideoSink.run video x ppu.vclock ppu.colors.[color1]


  let renderBkg depth bg ppu =
    bg |> Background.render ppu.bgMode ppu.vclock ppu.vram0 ppu.vram1 depth


  let renderSpr ppu =
    let (t, r) =
      ppu.spr |> Sprite.render ppu.vclock ppu.oam ppu.vram0 ppu.vram1

    if t then ppu.ppu1Stat <- ppu.ppu1Stat ||| 0x80uy
    if r then ppu.ppu1Stat <- ppu.ppu1Stat ||| 0x40uy


  let renderMode0 video ppu =
    ppu |> renderBkg Background.BPP2 ppu.bg0
    ppu |> renderBkg Background.BPP2 ppu.bg1
    ppu |> renderBkg Background.BPP2 ppu.bg2
    ppu |> renderBkg Background.BPP2 ppu.bg3
    ppu |> renderSpr

    blend video ppu


  let renderMode1 video ppu =
    ppu |> renderBkg Background.BPP4 ppu.bg0
    ppu |> renderBkg Background.BPP4 ppu.bg1
    ppu |> renderBkg Background.BPP2 ppu.bg2
    ppu |> renderSpr

    blend video ppu


  let renderMode2 video = // Offset-per-tile
    ()


  let getColor (i: int) ppu =
    let layers =
      [ ppu.spr.layer   // Sprites with priority 3
        ppu.bg0.layer   // BG1 tiles with priority 1
        ppu.spr.layer   // Sprites with priority 2
        ppu.bg1.layer   // BG2 tiles with priority 1
        ppu.spr.layer   // Sprites with priority 1
        ppu.bg0.layer   // BG1 tiles with priority 0
        ppu.spr.layer   // Sprites with priority 0
        ppu.bg1.layer ] // BG2 tiles with priority 0

    layers
      |> Seq.map (Layer.getMainColor ppu.window1 ppu.window2 i)
      |> Seq.tryFind (fun color -> color > 0)
      |> Option.defaultValue 0


  let renderMode3 video ppu =
    ppu |> renderBkg Background.BPP8 ppu.bg0
    ppu |> renderBkg Background.BPP4 ppu.bg1
    ppu |> renderSpr

    for i in 0 .. 255 do
      let color = getColor i ppu

      VideoSink.run video i ppu.vclock ppu.colors.[int <| readCram16 color ppu]


  let renderMode4 video = // Offset-per-tile
    ()


  let renderMode5 video = // Hi-res
    ()


  let renderMode6 video = // Hi-res
    ()


  let renderMode7 video ppu =
    ppu.bg0 |> Background.renderAffine ppu.m7 ppu.vclock ppu.vram0 ppu.vram1
    ppu |> renderSpr

    blend video ppu


  let renderScanline video ppu =
    if ppu.forceBlank || ppu.vclock > (if ppu.overscan then 239 else 223) then
      ()
    else
      for i in 0 .. 255 do
        ppu.bg0.layer.enable.[i] <- false
        ppu.bg1.layer.enable.[i] <- false
        ppu.bg2.layer.enable.[i] <- false
        ppu.bg3.layer.enable.[i] <- false
        ppu.spr.layer.enable.[i] <- false
        VideoSink.run video i ppu.vclock 0

      match ppu.bgMode with
      | 0 -> renderMode0 video ppu
      | 1 -> renderMode1 video ppu
      | 2 -> renderMode2 video
      | 3 -> renderMode3 video ppu
      | 4 -> renderMode4 video
      | 5 -> renderMode5 video
      | 6 -> renderMode6 video
      | _ -> renderMode7 video ppu // affine render


  let update hblank vblank video ppu =
    ppu.hclock <- ppu.hclock + 1

    if ppu.hclock = 274 then renderScanline video ppu
    if ppu.hclock = 341 then
      ppu.hclock <- 0
      ppu.vclock <- ppu.vclock + 1

      if ppu.vclock = (if ppu.overscan then 241 else 225) then
        ppu.ppu2Stat <- ppu.ppu2Stat ^^^ 0x80uy // toggle field flag every vblank
        vblank(true)

      if ppu.vclock = 262 then
        ppu.vclock <- 0
        vblank(false)

        ppu.ppu1Stat <- ppu.ppu1Stat &&& 0x3fuy // reset time and range flags

    if ppu.vclock < (if ppu.overscan then 241 else 225) then
      if ppu.hclock <= 18 then
        hblank(true)

      if ppu.hclock >= 289 then
        hblank(false)


  let clock hblank vblank video (cycles: int<PpuCycle>) ppu =
    for _ in 1 .. (cycles / 1<_>) do
      update hblank vblank video ppu
