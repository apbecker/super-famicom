namespace SuperFamicom.PPU

type Background =
  { layer: Layer
    mutable index: int
    mutable hOffset: int
    mutable vOffset: int
    mutable mosaic: bool
    mutable nameBase: int
    mutable nameSize: int
    mutable charBase: int
    mutable charSize: int }

module Background =

  open SuperFamicom.Util


  let [<Literal>] BPP2 = 2
  let [<Literal>] BPP4 = 4
  let [<Literal>] BPP8 = 8


  let init index =
    { layer = Layer.init 2
      index = index
      hOffset = 0
      vOffset = 0
      mosaic = false
      nameBase = 0
      nameSize = 0
      charBase = 0
      charSize = 0 }


  let renderSmall mode vclock (vram0: byte[]) (vram1: byte[]) depth (bg: Background) =
    let xLine = (bg.hOffset) &&& 7
    let xTile = (bg.hOffset) >>> 3
    let yLine = (bg.vOffset + vclock) &&& 7
    let yTile = (bg.vOffset + vclock) >>> 3

    let mutable fine = bg.hOffset &&& 7
    let mutable offset = 0

    let (yMove, xMove) =
      match bg.nameSize with
      | 0 -> (31, 31)
      | 1 -> (31,  5)
      | 2 -> ( 5, 31)
      | _ -> ( 6,  5)

    let bits = Array.zeroCreate depth

    for i in 0 .. 32 do
      let nameAddress =
        bg.nameBase +
          ((yTile &&& 31) <<< 5) +
          ((xTile &&& 31) <<< 0) +
          ((yTile &&& 32) <<< yMove) +
          ((xTile &&& 32) <<< xMove)

      let name =
        ((int vram0.[nameAddress &&& 0x7fff]) <<< 0) |||
        ((int vram1.[nameAddress &&& 0x7fff]) <<< 8)

      let priority = bg.layer.priorities.[(name >>> 13) &&& 1]

      let mutable charAddress = bg.charBase + ((name &&& 0x3ff) * depth * 4) + yLine

      if (name &&& 0x8000) <> 0 then
        charAddress <- charAddress ^^^ 0x0007 // vflip

      if (name &&& 0x4000) = 0 then // hflip
        for j in 0 .. 2 .. depth - 1 do
          let addr = (charAddress &&& 0x7fff) ||| (j <<< 2)
          bits.[j ||| 0] <- Byte.reverse(int vram0.[addr])
          bits.[j ||| 1] <- Byte.reverse(int vram1.[addr])
      else
        for j in 0 .. 2 .. depth - 1 do
          let addr = (charAddress &&& 0x7fff) ||| (j <<< 2)
          bits.[j ||| 0] <- vram0.[addr]
          bits.[j ||| 1] <- vram1.[addr]

      let mutable palette = ((name &&& 0x1c00) >>> 10) <<< depth

      match mode with
      | 0 -> palette <- palette + (bg.index <<< 5)
      | 3
      | 4 -> palette <- palette * bg.index
      | _ -> ()

      for k in 0 .. depth - 1 do
        bits.[k] <- bits.[k] >>> fine

      for j in fine .. 7 do
        let mutable colour = 0

        for k in 0 .. depth - 1 do
          colour <- colour ||| (((int bits.[k]) &&& 1) <<< k)
          bits.[k] <- bits.[k] >>> 1

        if offset < 256 then
          if colour <> 0 then
            bg.layer.enable.[offset] <- true
            bg.layer.raster.[offset] <- palette + colour
            bg.layer.priority.[offset] <- priority
          else
            bg.layer.enable.[offset] <- false

        offset <- offset + 1

      fine <- 0


  let render mode vclock vram0 vram1 depth bg =
    if bg.charSize = 16 then
      ()
    else
      renderSmall mode vclock vram0 vram1 depth bg


  let renderAffine (m7: Mode7) vclock (vram0: byte[]) (vram1: byte[]) (bg: Background) =
    let a = int32 (int16 m7.a)
    let b = int32 (int16 m7.b)
    let c = int32 (int16 m7.c)
    let d = int32 (int16 m7.d)

    let cx = (((int m7.x) &&& 0x1fff) ^^^ 0x1000) - 0x1000
    let cy = (((int m7.y) &&& 0x1fff) ^^^ 0x1000) - 0x1000

    let hoffs = (((int m7.hOffset) &&& 0x1fff) ^^^ 0x1000) - 0x1000
    let voffs = (((int m7.vOffset) &&& 0x1fff) ^^^ 0x1000) - 0x1000

    let mutable h = hoffs - cx
    let mutable v = voffs - cy
    let mutable y = vclock

    h <- if (h &&& 0x2000) <> 0 then (h ||| (~~~0x3ff)) else (h &&& 0x3ff)
    v <- if (v &&& 0x2000) <> 0 then (v ||| (~~~0x3ff)) else (v &&& 0x3ff)

    if ((int m7.control) &&& 0x02) <> 0 then
      y <- y ^^^ 255

    let mutable tx = ((a * h) &&& (~~~63)) + ((b * v) &&& (~~~63)) + ((b * y) &&& (~~~63)) + (cx <<< 8)
    let mutable ty = ((c * h) &&& (~~~63)) + ((d * v) &&& (~~~63)) + ((d * y) &&& (~~~63)) + (cy <<< 8)
    let mutable dx = a
    let mutable dy = c

    if ((int m7.control) &&& 0x01) <> 0 then
      tx <- tx + (dx * 255)
      ty <- ty + (dy * 255)
      dx <- (-dx)
      dy <- (-dy)

    for x in 0 .. 255 do
      let rx = (tx >>> 8) &&& 0x3ff
      let ry = (ty >>> 8) &&& 0x3ff

      let tile = int <| vram0.[((ry &&& (~~~7)) <<< 4) + (rx >>> 3)] // ..yy yyyy yxxx xxxx
      let data = int <| vram1.[(tile <<< 6) + ((ry &&& 7) <<< 3) + (rx &&& 7)] // ..dd dddd ddyy yxxx

      bg.layer.enable.[x] <- true
      bg.layer.raster.[x] <- data
      bg.layer.priority.[x] <- bg.layer.priorities.[0]

      tx <- tx + dx
      ty <- ty + dy
