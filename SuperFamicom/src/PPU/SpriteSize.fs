namespace SuperFamicom.PPU

type SpriteSize =
  | Small
  | Large

module SpriteSize =

  let ofPrimitive value =
    match value &&& 1 with
    | 0 -> Small
    | _ -> Large
