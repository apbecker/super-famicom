namespace SuperFamicom.PPU

open SuperFamicom
open SuperFamicom.Memory
open SuperFamicom.Util

type Sprite =
  { layer: Layer
    mutable interlace: bool
    mutable addr: int
    mutable name: int
    mutable shape: SpriteShape }

module Sprite =

  let dimensions shape size =
    match (shape, size) with
    | (Shape0, Small) -> ( 8,  8) // 000 =  8x8  and 16x16 sprites
    | (Shape0, Large) -> (16, 16) // 000 =  8x8  and 16x16 sprites
    | (Shape1, Small) -> ( 8,  8) // 001 =  8x8  and 32x32 sprites
    | (Shape1, Large) -> (32, 32) // 001 =  8x8  and 32x32 sprites
    | (Shape2, Small) -> ( 8,  8) // 010 =  8x8  and 64x64 sprites
    | (Shape2, Large) -> (64, 64) // 010 =  8x8  and 64x64 sprites
    | (Shape3, Small) -> (16, 16) // 011 = 16x16 and 32x32 sprites
    | (Shape3, Large) -> (32, 32) // 011 = 16x16 and 32x32 sprites
    | (Shape4, Small) -> (16, 16) // 100 = 16x16 and 64x64 sprites
    | (Shape4, Large) -> (64, 64) // 100 = 16x16 and 64x64 sprites
    | (Shape5, Small) -> (32, 32) // 101 = 32x32 and 64x64 sprites
    | (Shape5, Large) -> (64, 64) // 101 = 32x32 and 64x64 sprites
    | (Shape6, Small) -> (16, 32) // 110 = 16x32 and 32x64 sprites
    | (Shape6, Large) -> (32, 64) // 110 = 16x32 and 32x64 sprites
    | (Shape7, Small) -> (16, 32) // 111 = 16x32 and 32x32 sprites
    | (Shape7, Large) -> (32, 32) // 111 = 16x32 and 32x32 sprites


  let init () =
    { layer = Layer.init 4
      interlace = false
      addr = 0
      name = 0
      shape = Shape0 }


  let readOam8 oam address =
    Memory.get8 oam address


  let readOam16 oam address =
    Memory.get8 oam (address * 2)


  let render vclock oam (vram0: byte[]) (vram1: byte[]) sp =
    let mutable t = 0
    let mutable r = 0

    for i in 0 .. 255 do
      sp.layer.raster.[i] <- 0
      sp.layer.enable.[i] <- false

    for i in 127 .. -1 .. 0 do
      let exta = readOam16 oam ((i >>> 3) ||| 0x100) >>> ((i &&& 7) <<< 1) |> int
      let mutable xpos = readOam8 oam ((i <<< 2) ||| 0x000) |> int
      let mutable ypos = readOam8 oam ((i <<< 2) ||| 0x001) |> int
      let tile = readOam8 oam ((i <<< 2) ||| 0x002) |> int
      let attr = readOam8 oam ((i <<< 2) ||| 0x003) |> int

      if ypos >= 240 then
        ypos <- ypos - 256

      let mutable line = (vclock - ypos - 1) &&& System.Int32.MaxValue

      xpos <- xpos ||| ((exta &&& 1) <<< 8)

      let (xSize, ySize) = dimensions sp.shape (SpriteSize.ofPrimitive (exta >>> 1))

      if sp.interlace then line <- line <<< 1

      if line < ySize then
        r <- r + 1

        let palette = 0x80 + ((attr &&& 0x0e) <<< 3)

        if (attr &&& 0x80) <> 0 then
          line <- line ^^^ (ySize - 1)

        let mutable charAddress = sp.addr + (tile <<< 4) + ((line &&& 0x38) <<< 5) + (line &&& 7)
        let mutable charStep = 16

        if (attr &&& 0x40) <> 0 then
          charAddress <- charAddress + ((xSize - 8) <<< 1)
          charStep <- (-16)

        if (attr &&& 0x01) <> 0 then
          charAddress <- (charAddress + sp.name) &&& 0x7fff

        let priority = sp.layer.priorities.[(attr >>> 4) &&& 3]

        for i in 0 .. 8 .. xSize - 1 do
          t <- t + 1

          let mutable bit0 = int <| vram0.[charAddress + 0]
          let mutable bit1 = int <| vram1.[charAddress + 0]
          let mutable bit2 = int <| vram0.[charAddress + 8]
          let mutable bit3 = int <| vram1.[charAddress + 8]

          if (attr &&& 0x40) = 0 then
            bit0 <- int <| Byte.reverse(bit0)
            bit1 <- int <| Byte.reverse(bit1)
            bit2 <- int <| Byte.reverse(bit2)
            bit3 <- int <| Byte.reverse(bit3)

          for x in 0 .. 7 do
            let colour =
              ((bit0 &&& 1) <<< 0) |||
              ((bit1 &&& 1) <<< 1) |||
              ((bit2 &&& 1) <<< 2) |||
              ((bit3 &&& 1) <<< 3)

            bit0 <- bit0 >>> 1
            bit1 <- bit1 >>> 1
            bit2 <- bit2 >>> 1
            bit3 <- bit3 >>> 1

            if (colour <> 0 && xpos < 256) then
              sp.layer.enable.[xpos] <- true
              sp.layer.raster.[xpos] <- palette + colour
              sp.layer.priority.[xpos] <- priority
              xpos <- (xpos + 1) &&& 0x1ff

          charAddress <- (charAddress + charStep) &&& 0x7fff

    ( t > 34
    , r > 32 )
