namespace SuperFamicom.PPU

type Mode7 =
  { mutable control: byte
    mutable latch: byte
    mutable a: uint16
    mutable b: uint16
    mutable c: uint16
    mutable d: uint16
    mutable x: uint16
    mutable y: uint16
    mutable hOffset: uint16
    mutable vOffset: uint16 }

module Mode7 =

  let init () =
    { control = 0uy
      latch = 0uy
      a = 0us
      b = 0us
      c = 0us
      d = 0us
      x = 0us
      y = 0us
      hOffset = 0us
      vOffset = 0us }
