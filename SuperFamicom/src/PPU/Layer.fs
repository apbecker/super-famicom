namespace SuperFamicom.PPU

type Layer =
  { enable: bool[]
    raster: int[]
    priority: int[]
    mutable windowMain: bool
    mutable windowSub: bool
    mutable screenMain: int
    mutable screenSub: int
    mutable windowLogic: int
    mutable window1Enable: bool
    mutable window1Inverted: bool
    mutable window2Enable: bool
    mutable window2Inverted: bool
    mutable priorities: int[] }

module Layer =

  let init priorities =
    { enable = Array.zeroCreate 256
      raster = Array.zeroCreate 256
      priority = Array.zeroCreate 256
      windowMain = false
      windowSub = false
      screenMain = 0
      screenSub = 0
      windowLogic = 0
      window1Enable = false
      window1Inverted = false
      window2Enable = false
      window2Inverted = false
      priorities = Array.zeroCreate priorities }


  let getWindow window1 window2 x state =
    if (not state.window1Enable) && (not state.window2Enable) then
      false
    else
      let mutable w1 = (x >= int window1.x1) && (x <= int window1.x2)
      let mutable w2 = (x >= int window2.x1) && (x <= int window2.x2)

      if state.window1Inverted then
        w1 <- not w1

      if state.window2Inverted then
        w2 <- not w2

      w1 <- w1 && state.window1Enable
      w2 <- w2 && state.window2Enable

      match state.windowLogic with
      | 0 -> (w1 || w2) // or
      | 1 -> (w1 && w2) // and
      | 2 -> (w1 <> w2) // xor
      | _ -> (w1 =  w2) // xnor


  let getMainColor window1 window2 x state =
    if state.windowMain && (getWindow window1 window2 x state) then
      0
    else
      state.raster.[x] &&& state.screenMain


  let getSubColor window1 window2 x state =
    if state.windowSub && (getWindow window1 window2 x state) then
      0
    else
      state.raster.[x] &&& state.screenSub
