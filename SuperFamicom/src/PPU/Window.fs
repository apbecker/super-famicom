namespace SuperFamicom.PPU

type Window =
  { mutable x1: byte
    mutable x2: byte }

module Window =

  let init () =
    { x1 = 0uy
      x2 = 0uy }
