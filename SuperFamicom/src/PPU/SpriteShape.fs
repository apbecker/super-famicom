namespace SuperFamicom.PPU

type SpriteShape =
  | Shape0
  | Shape1
  | Shape2
  | Shape3
  | Shape4
  | Shape5
  | Shape6
  | Shape7

module SpriteShape =

  let ofPrimitive value =
    match value &&& 7 with
    | 0 -> Shape0
    | 1 -> Shape1
    | 2 -> Shape2
    | 3 -> Shape3
    | 4 -> Shape4
    | 5 -> Shape5
    | 6 -> Shape6
    | _ -> Shape7
