namespace SuperFamicom

[<Measure>] type CpuCycle
[<Measure>] type PpuCycle
[<Measure>] type SpcCycle
[<Measure>] type MasterCycle

module CpuCycle =

  let [<Literal>] Slow = 12<CpuCycle>
  let [<Literal>] Norm =  8<CpuCycle>
  let [<Literal>] Fast =  6<CpuCycle>


  let inline convert src dst (cpu: int<CpuCycle>) rem =
    let ppu = (cpu * dst) + rem
    let quo = (ppu / src)
    let rem = ppu - (src * quo)

    (quo, rem)


module PpuCycle =

  let [<Literal>] CpuTimebase = 1<CpuCycle>
  let [<Literal>] PpuTimebase = 4<PpuCycle>


  let ofCpuCycle =
    CpuCycle.convert CpuTimebase PpuTimebase


module SpcCycle =

  let [<Literal>] CpuTimebase = 39_375<CpuCycle>
  let [<Literal>] SpcTimebase = 45_056<SpcCycle>


  let ofCpuCycle =
    CpuCycle.convert CpuTimebase SpcTimebase


module MasterCycle =

  let [<Literal>] Frequency = 236_250_000<MasterCycle> // 21.477272~ × 11

  let [<Literal>] CpuToMaster = 11<MasterCycle / CpuCycle>
