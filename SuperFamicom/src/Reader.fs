namespace SuperFamicom

type Reader =
  | Reader of (uint8 -> uint16 -> uint8 -> uint8)

module Reader =

  let run (Reader f) bank address data =
    f bank address data
