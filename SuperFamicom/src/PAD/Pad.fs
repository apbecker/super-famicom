namespace SuperFamicom.PAD

open SuperFamicom.Util

type PadState =
  { input: SDL2Input
    mutable value: uint16 }

module Pad =

  let init index =
    let input = SDL2Input(index, 12)

    input.Map 0 "A"
    input.Map 1 "X"
    input.Map 2 "Back"
    input.Map 3 "Menu"
    input.Map 4 "DPad-U"
    input.Map 5 "DPad-D"
    input.Map 6 "DPad-L"
    input.Map 7 "DPad-R"
    input.Map 8 "B"
    input.Map 9 "Y"
    input.Map 10 "L-Shoulder"
    input.Map 11 "R-Shoulder"

    { input = input
      value = 0us }


  let frame state =
    state.input.Update ()

    let mutable latch = 0x0000

    if state.input.Pressed 0x0 then latch <- latch ||| 0x8000
    if state.input.Pressed 0x1 then latch <- latch ||| 0x4000
    if state.input.Pressed 0x2 then latch <- latch ||| 0x2000
    if state.input.Pressed 0x3 then latch <- latch ||| 0x1000
    if state.input.Pressed 0x4 then latch <- latch ||| 0x0800
    if state.input.Pressed 0x5 then latch <- latch ||| 0x0400
    if state.input.Pressed 0x6 then latch <- latch ||| 0x0200
    if state.input.Pressed 0x7 then latch <- latch ||| 0x0100
    if state.input.Pressed 0x8 then latch <- latch ||| 0x0080
    if state.input.Pressed 0x9 then latch <- latch ||| 0x0040
    if state.input.Pressed 0xa then latch <- latch ||| 0x0020
    if state.input.Pressed 0xb then latch <- latch ||| 0x0010

    state.value <- uint16 latch
