namespace SuperFamicom

type Writer =
  | Writer of (uint8 -> uint16 -> uint8 -> unit)

module Writer =

  let run (Writer f) bank address data =
    f bank address data
