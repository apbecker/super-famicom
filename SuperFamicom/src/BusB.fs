module SuperFamicom.BusB

open SuperFamicom.Memory
open SuperFamicom.PPU
open SuperFamicom.SMP.CPU


let read smp ppu wram = Reader (fun _ address data ->
  let address = int address
  if (address &&& 0xc0) = 0x40 then
    // S-SMP Registers
    Smp.readPort smp (uint16 (address &&& 3))
  else
    match address &&& 0xff with
    // S-PPU Registers
    | 0x34 -> ppu |> Ppu.read2134
    | 0x35 -> ppu |> Ppu.read2135
    | 0x36 -> ppu |> Ppu.read2136
    | 0x37 -> ppu |> Ppu.read2137
    | 0x38 -> ppu |> Ppu.read2138
    | 0x39 -> ppu |> Ppu.read2139
    | 0x3a -> ppu |> Ppu.read213A
    | 0x3b -> ppu |> Ppu.read213B
    | 0x3c -> ppu |> Ppu.read213C
    | 0x3d -> ppu |> Ppu.read213D
    | 0x3e -> ppu |> Ppu.read213E
    | 0x3f -> ppu |> Ppu.read213F

    // W-RAM Registers
    | 0x80 -> wram |> WRAM.autoRead
    | 0x81 -> data
    | 0x82 -> data
    | 0x83 -> data

    | _ -> data
)


let write psram ppu wram = Writer (fun _ address data ->
  let address = int address
  if (address &&& 0xc0) = 0x40 then
    Smp.writePort psram (uint16 (address &&& 3)) data
  else
    match address &&& 0xff with
    // S-PPU Registers
    | 0x00 -> ppu |> Ppu.write2100(data)
    | 0x01 -> ppu |> Ppu.write2101(data)
    | 0x02 -> ppu |> Ppu.write2102(data)
    | 0x03 -> ppu |> Ppu.write2103(data)
    | 0x04 -> ppu |> Ppu.write2104(data)
    | 0x05 -> ppu |> Ppu.write2105(data)
    | 0x06 -> ppu |> Ppu.write2106(data)
    | 0x07 -> ppu |> Ppu.write2107(data)
    | 0x08 -> ppu |> Ppu.write2108(data)
    | 0x09 -> ppu |> Ppu.write2109(data)
    | 0x0a -> ppu |> Ppu.write210A(data)
    | 0x0b -> ppu |> Ppu.write210B(data)
    | 0x0c -> ppu |> Ppu.write210C(data)
    | 0x0d -> ppu |> Ppu.write210D(data)
    | 0x0e -> ppu |> Ppu.write210E(data)
    | 0x0f -> ppu |> Ppu.write210F(data)
    | 0x10 -> ppu |> Ppu.write2110(data)
    | 0x11 -> ppu |> Ppu.write2111(data)
    | 0x12 -> ppu |> Ppu.write2112(data)
    | 0x13 -> ppu |> Ppu.write2113(data)
    | 0x14 -> ppu |> Ppu.write2114(data)
    | 0x15 -> ppu |> Ppu.write2115(data)
    | 0x16 -> ppu |> Ppu.write2116(data)
    | 0x17 -> ppu |> Ppu.write2117(data)
    | 0x18 -> ppu |> Ppu.write2118(data)
    | 0x19 -> ppu |> Ppu.write2119(data)
    | 0x1a -> ppu |> Ppu.write211A(data)
    | 0x1b -> ppu |> Ppu.write211B(data)
    | 0x1c -> ppu |> Ppu.write211C(data)
    | 0x1d -> ppu |> Ppu.write211D(data)
    | 0x1e -> ppu |> Ppu.write211E(data)
    | 0x1f -> ppu |> Ppu.write211F(data)
    | 0x20 -> ppu |> Ppu.write2120(data)
    | 0x21 -> ppu |> Ppu.write2121(data)
    | 0x22 -> ppu |> Ppu.write2122(data)
    | 0x23 -> ppu |> Ppu.write2123(data)
    | 0x24 -> ppu |> Ppu.write2124(data)
    | 0x25 -> ppu |> Ppu.write2125(data)
    | 0x26 -> ppu |> Ppu.write2126(data)
    | 0x27 -> ppu |> Ppu.write2127(data)
    | 0x28 -> ppu |> Ppu.write2128(data)
    | 0x29 -> ppu |> Ppu.write2129(data)
    | 0x2a -> ppu |> Ppu.write212A(data)
    | 0x2b -> ppu |> Ppu.write212B(data)
    | 0x2c -> ppu |> Ppu.write212C(data)
    | 0x2d -> ppu |> Ppu.write212D(data)
    | 0x2e -> ppu |> Ppu.write212E(data)
    | 0x2f -> ppu |> Ppu.write212F(data)
    | 0x30 -> ppu |> Ppu.write2130(data)
    | 0x31 -> ppu |> Ppu.write2131(data)
    | 0x32 -> ppu |> Ppu.write2132(data)
    | 0x33 -> ppu |> Ppu.write2133(data)

    // W-RAM Registers
    | 0x80 -> wram |> WRAM.autoWrite data
    | 0x81 -> wram.index <- (wram.index &&& 0x1ff00) ||| (((int data) <<<  0) &&& 0x000ff)
    | 0x82 -> wram.index <- (wram.index &&& 0x100ff) ||| (((int data) <<<  8) &&& 0x0ff00)
    | 0x83 -> wram.index <- (wram.index &&& 0x0ffff) ||| (((int data) <<< 16) &&& 0x10000)

    | _ -> ()
)
