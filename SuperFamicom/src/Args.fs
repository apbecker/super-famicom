namespace SuperFamicom

type CartridgeType =
  | HiRom
  | LoRom

type Args =
  { cartType: CartridgeType
    fileName: string }

module Args =

  let init =
    { cartType = LoRom
      fileName = "" }


  let rec private createFromList argv args =
    match argv with
    | [] ->
      args

    | "-t" :: "lorom" :: argv
    | "--type" :: "lorom" :: argv ->
      createFromList argv
        { args with cartType = LoRom }

    | "-t" :: "hirom" :: argv
    | "--type" :: "hirom" :: argv ->
      createFromList argv
        { args with cartType = HiRom }

    | fileName :: argv ->
      createFromList argv
        { args with fileName = fileName }


  let create argv =
    createFromList (Array.toList argv) init
