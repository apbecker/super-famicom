module SuperFamicom.Cartridges.LoRomCartridge

open SuperFamicom

let read image = Reader (fun bank address _ ->
  let mask = (Array.length image) - 1
  let index = ((int bank) <<< 15) ||| ((int address) &&& 0x7fff)

  image.[index &&& mask]
)
