module SuperFamicom.Cartridges.HiRomCartridge

open SuperFamicom

let read image = Reader (fun bank address _ ->
  let mask = (Array.length image) - 1
  let index = ((int bank) <<< 16) ||| ((int address) &&& 0xffff)

  image.[index &&& mask]
)
