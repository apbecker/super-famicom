namespace SuperFamicom

open System.IO
open SuperFamicom.Cartridges
open SuperFamicom.CPU
open SuperFamicom.Memory
open SuperFamicom.PAD
open SuperFamicom.PPU
open SuperFamicom.SMP
open SuperFamicom.SMP.DSP
open SuperFamicom.SMP.CPU
open SuperFamicom.Util

type Driver =
  private
    | Driver of (AudioSink -> VideoSink -> unit)

module Driver =

  let smpUpdate spcClock dspClock adding remain =
    let (cycles, remain) = SpcCycle.ofCpuCycle adding remain
    spcClock cycles
    dspClock cycles
    remain


  let ppuUpdate ppuClock adding remain =
    let (cycles, remain) = PpuCycle.ofCpuCycle adding remain
    ppuClock cycles
    remain


  let init args =
    let binary = File.ReadAllBytes args.fileName
    let wram = WRAM.init ()
    let ppu = Ppu.init ()
    let psram = PSRAM.init ()
    let dsp = Dsp.init ()
    let smp = Smp.init ()

    let dspReader () = Dsp.read (PSRAM.read psram) dsp
    let dspWriter data = Dsp.write (PSRAM.read psram) dsp data

    let smpReader = Smp.read8 psram smp dspReader
    let smpWriter = Smp.write8 psram smp dspWriter

    Smp.reset smpReader smp

    let cpu = Cpu.init ()

    let joypad1 = Pad.init 0
    let joypad2 = Pad.init 1

    let cartReader =
      match args.cartType with
      | LoRom -> LoRomCartridge.read binary
      | HiRom -> HiRomCartridge.read binary

    let busAReader =
      BusA.read
        <| BusB.read smp ppu wram
        <| cartReader
        <| WRAM.read wram
        <| BusA.readSCPU joypad1 joypad2 cpu

    let busAWriter =
      BusA.write
        <| BusB.write psram ppu wram
        <| WRAM.write wram
        <| BusA.writeSCPU cpu

    Cpu.reset busAReader cpu

    let mutable cycles = 0<MasterCycle>

    Driver (fun audio video ->
      Pad.frame(joypad1)
      Pad.frame(joypad2)

      let hblank value = cpu |> Cpu.hblank value
      let vblank value = cpu |> Cpu.vblank value
      let cpuClock () = Cpu.update busAReader busAWriter cpu
      let dspClock adding = Dsp.update (PSRAM.read psram) (PSRAM.write psram) dsp audio adding
      let spcClock adding = Smp.clock smpReader smpWriter smp adding
      let ppuClock adding = Ppu.clock hblank vblank video adding ppu

      let smpUpdate adding remain = smpUpdate spcClock dspClock adding remain
      let ppuUpdate adding remain = ppuUpdate ppuClock adding remain

      let rec loop cycles =
        if cycles >= MasterCycle.Frequency then
          cycles - MasterCycle.Frequency
        else
          let adding = cpuClock ()

          smp.cycles <- smpUpdate adding smp.cycles
          ppu.cycles <- ppuUpdate adding ppu.cycles

          loop (cycles + (adding * 60 * MasterCycle.CpuToMaster))

      cycles <- loop cycles
    )


  let run (Driver(driver)) audio video =
    driver audio video
