module SuperFamicom.BusA

open SuperFamicom.CPU
open SuperFamicom.PAD


let readSCPU joypad1 joypad2 scpu = Reader (fun bank address data ->
  match int address with
  | 0x4016 -> data // JOYSER0
  | 0x4017 -> data // JOYSER1

  | 0x4210 -> byte ((data &&& 0x70uy) ||| (if scpu.inVBlank then 0x80uy else 0uy) ||| 0x02uy)
  | 0x4211 -> byte ((data &&& 0x7fuy) ||| (if scpu.timerCoincidence then 0x80uy else 0uy))
  | 0x4212 -> byte ((data &&& 0x3euy) ||| (if scpu.inVBlank then 0x80uy else 0uy) ||| (if scpu.inHBlank then 0x40uy else 0uy))
  | 0x4213 -> data // I/O Port
  | 0x4214 -> byte (scpu.rddiv >>> 0) // RDDIVL
  | 0x4215 -> byte (scpu.rddiv >>> 8) // RDDIVH
  | 0x4216 -> byte (scpu.rdmpy >>> 0) // RDMPYL
  | 0x4217 -> byte (scpu.rdmpy >>> 8) // RDMPYH

  | 0x4218 -> byte (joypad1.value >>> 0)
  | 0x4219 -> byte (joypad1.value >>> 8)

  | 0x421a -> byte (joypad2.value >>> 0)
  | 0x421b -> byte (joypad2.value >>> 8)

  | 0x421c -> 0uy // JOY3L
  | 0x421d -> 0uy // JOY3H

  | 0x421e -> 0uy // JOY4L
  | 0x421f -> 0uy // JOY4H

  | _ ->
    failwithf "Unknown address: $%02x:%04x." bank address
)


let writeSCPU scpu = Writer (fun bank address data ->
  let address = int address
  if (address &&& 0xff00) = 0x4300 then
    let dma = scpu.dma.channels.[(address >>> 4) &&& 7]

    match (address &&& 0xff0f) with
    | 0x4300 -> dma.control <- data
    | 0x4301 -> dma.addressB <- data
    | 0x4302 -> dma.addressA <- (dma.addressA &&& 0xffff00) ||| ((int data) <<< 0)
    | 0x4303 -> dma.addressA <- (dma.addressA &&& 0xff00ff) ||| ((int data) <<< 8)
    | 0x4304 -> dma.addressA <- (dma.addressA &&& 0x00ffff) ||| ((int data) <<< 16)
    | 0x4305 -> dma.count <- (dma.count &&& 0xff00us) ||| ((uint16 data) <<< 0)
    | 0x4306 -> dma.count <- (dma.count &&& 0x00ffus) ||| ((uint16 data) <<< 8)
    | 0x4307 -> ()
    | 0x4308 -> ()
    | 0x4309 -> ()
    | 0x430a -> ()
    | 0x430b -> ()
    | 0x430c -> ()
    | 0x430d -> ()
    | 0x430e -> ()
    | 0x430f -> ()
    | _ -> ()
  else
    match address with
    | 0x4016 -> ()

    | 0x4200 ->
      scpu.reg4200 <- int data
      scpu |> Cpu.nmiWrapper ((scpu.reg4200 &&& 0x80) <> 0)

    | 0x4201 -> () // I/O Port
    | 0x4202 -> scpu.wrmpya <- data // WRMPYA
    | 0x4203 -> scpu.wrmpyb <- data; scpu.rdmpy <- uint16 (scpu.wrmpya * scpu.wrmpyb) // WRMPYB
    | 0x4204 -> scpu.wrdiv <- (scpu.wrdiv &&& 0xff00us) ||| ((uint16 data) <<< 0) // WRDIVL
    | 0x4205 -> scpu.wrdiv <- (scpu.wrdiv &&& 0x00ffus) ||| ((uint16 data) <<< 8) // WRDIVH
    | 0x4206 -> // WRDIVB
      scpu.wrdivb <- data

      if scpu.wrdivb = 0uy then
        scpu.rddiv <- 0xffffus
        scpu.rdmpy <- scpu.wrdiv
      else
        scpu.rddiv <- (scpu.wrdiv / (uint16 scpu.wrdivb))
        scpu.rdmpy <- (scpu.wrdiv % (uint16 scpu.wrdivb))

    | 0x4207 -> scpu.hTarget <- (scpu.hTarget &&& (~~~0x00ff)) ||| ((int data) <<< 0)
    | 0x4208 -> scpu.hTarget <- (scpu.hTarget &&& (~~~0xff00)) ||| ((int data) <<< 8)
    | 0x4209 -> scpu.vTarget <- (scpu.vTarget &&& (~~~0x00ff)) ||| ((int data) <<< 0)
    | 0x420a -> scpu.vTarget <- (scpu.vTarget &&& (~~~0xff00)) ||| ((int data) <<< 8)

    | 0x420b -> // MDMAEN
      scpu.dma.mdmaEn <- data
      scpu.dma.mdmaCount <- 2

    | 0x420c -> // HDMAEN
      scpu.dma.hdmaEn <- data

    | 0x420d ->
      scpu.fastCart <- (data &&& 1uy) <> 0uy

    | _ ->
      failwithf "Unknown address: $%02x:%04x." bank address
)


let read busB cart wram scpu = Reader (fun bank address data ->
  match int bank with
  | b when (b &&& 0x7e) = 0x7e ->
    Reader.run wram bank address data

  | b when (b &&& 0x7f) <= 0x3f ->
    match int address with
    | a when ((a &&& 0xe000) = 0x0000) -> Reader.run wram 00uy address data
    | a when ((a &&& 0xff00) = 0x2100) -> Reader.run busB 00uy address data
    | a when ((a &&& 0xfc00) = 0x4000) -> Reader.run scpu bank address data
    | a when ((a &&& 0x8000) = 0x8000) -> Reader.run cart bank address data
    | _ -> data

  | _ ->
    Reader.run cart bank address data
)


let write busB wram scpu = Writer (fun bank address data ->
  match int bank with
  | b when (b &&& 0x7e) = 0x7e ->
    Writer.run wram bank address data

  | b when (b &&& 0x7f) <= 0x3f ->
    match int address with
    | a when (a &&& 0xe000) = 0x0000 (* $0000-$1fff *) -> Writer.run wram 00uy address data
    | a when (a &&& 0xff00) = 0x2100 (* $2100-$21ff *) -> Writer.run busB 00uy address data
    | a when (a &&& 0xfc00) = 0x4000 (* $4000-$43ff *) -> Writer.run scpu bank address data
    | a when (a &&& 0x8000) = 0x8000 (* $8000-$ffff *) -> ()
    | _ -> ()

  | _ ->
    ()
)
