namespace SuperFamicom.Util

#nowarn "9"

type SDL2Audio =
  { device: uint32
    samples: int
    buffer: int16 []
    mutable cursor: int }

module SDL2Audio =

  open Microsoft.FSharp.NativeInterop
  open SDL2

  let [<Literal>] Channels = 2
  let [<Literal>] SampleRate = 32000


  let create () =
    SDL.SDL_Init SDL.SDL_INIT_AUDIO |> ignore

    let samples = SampleRate / 60

    let mutable have = Unchecked.defaultof<SDL.SDL_AudioSpec>
    let mutable want = Unchecked.defaultof<SDL.SDL_AudioSpec>
    want.freq <- SampleRate
    want.format <- SDL.AUDIO_S16
    want.channels <- byte Channels
    want.samples <- uint16 samples
    want.callback <- null

    match SDL.SDL_OpenAudioDevice(null, 0, &want, &have, 0) with
    | 0u -> failwith (SDL.SDL_GetError ())
    | id ->
      SDL.SDL_PauseAudioDevice (id, 0)

      { device = id
        samples = samples
        buffer = Array.zeroCreate samples
        cursor = 0 }


  let destroy audio =
    SDL.SDL_CloseAudioDevice audio.device


  let sink audio = AudioSink (fun sample ->
    audio.buffer.[audio.cursor] <- int16 sample
    audio.cursor <- audio.cursor + 1

    if audio.cursor = Array.length audio.buffer then
      audio.cursor <- 0

      if SDL.SDL_GetQueuedAudioSize audio.device > 50000u then
        failwith "too many audio buffers were queued"

      use pointer = fixed audio.buffer in
      let result =
        SDL.SDL_QueueAudio
          ( audio.device
          , NativePtr.toNativeInt pointer
          , uint32 (audio.samples * 2) )
      in
      match result with
      | 0 -> ()
      | _ -> failwith (SDL.SDL_GetError ())
  )


  let render _audio =
    ()
