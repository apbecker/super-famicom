namespace SuperFamicom.Util

type BitValue =
  | Off
  | On

[<RequireQualifiedAccess>]
module BitValue =

  let toInt32 =
    function
    | Off -> 0
    | On -> 1


  let ofInt32 =
    function
    | 0 -> Off
    | _ -> On
