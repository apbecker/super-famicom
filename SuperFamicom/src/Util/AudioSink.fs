namespace SuperFamicom.Util

type AudioSink =
  | AudioSink of (int -> unit)

module AudioSink =

  let run (AudioSink f) sample =
    f sample
