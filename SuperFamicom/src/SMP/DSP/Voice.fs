namespace SuperFamicom.SMP.DSP

type Envelope =
  | EnvelopeA = 1
  | EnvelopeD = 2
  | EnvelopeS = 3
  | EnvelopeR = 0

type Voice =
  { buffer: ModuloArray      // decoded samples
    mutable bufPos: int      // place in buffer where next samples will be decoded
    mutable interpPos: int   // relative fractional position in sample (0x1000 = 1.0)
    mutable brrAddr: int     // address of current BRR block
    mutable brrOffset: int   // current decoding offset in BRR block
    mutable vbit: int        // bitmask for voice: 0x01 for voice 0, 0x02 for voice 1, etc
    mutable vidx: int        // voice channel register index: 0x00 for voice 0, 0x10 for voice 1, etc
    mutable konDelay: int    // KON delay/current setup phase
    mutable envMode: Envelope
    mutable env: int         // current envelope level
    mutable envxOut: int
    mutable hiddenEnv: int } // used by GAIN mode 7, very obscure quirk

module Voice =

  let [<Literal>] BrrBufferSize = 12


  let init i =
    { buffer = ModuloArray.init BrrBufferSize
      bufPos = 0
      interpPos = 0
      brrAddr = 0
      brrOffset = 1
      vbit = (1 <<< i)
      vidx = (i <<< 4)
      konDelay = 0
      envMode = Envelope.EnvelopeR
      env = 0
      envxOut = 0
      hiddenEnv = 0 }
