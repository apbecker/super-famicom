namespace SuperFamicom.SMP.DSP

type ModuloArray =
  private
    { length: int
      buffer: int[] }

module ModuloArray =

  let init length =
    { length = length
      buffer = Array.zeroCreate (length * 3) }


  let read array index =
    array.buffer.[index + array.length]


  let write array index value =
    array.buffer.[index] <- value
    array.buffer.[index + array.length] <- value
    array.buffer.[index + array.length + array.length] <- value
