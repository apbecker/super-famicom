namespace SuperFamicom.SMP.DSP

open SuperFamicom
open SuperFamicom.Util

type Dsp =
  { regs: byte[]
    echoHist: ModuloArray[]
    mutable echoHistPos: int
    mutable everyOtherSample: bool  // toggles every sample
    mutable keyOn: int              // KON value when last checked
    mutable noise: int
    mutable counter: int
    mutable echoOffset: int         // offset from ESA in echo buffer
    mutable echoLength: int         // number of bytes that echoOffset will stop at

    //hidden registers also written to when main register is written to
    mutable newKeyOn: int

    mutable endxBuf: int
    mutable envxBuf: int
    mutable outxBuf: int

    //temporary state between clocks

    //read once per sample
    mutable pitchModOn: int
    mutable noiseOn: int
    mutable echoOn: int
    mutable dir: int
    mutable keyOff: int

    //read a few clocks ahead before used
    mutable brrNextAddr: int
    mutable adsr0: int
    mutable brrHeader: int
    mutable brrByte: int
    mutable srcN: int
    mutable esa: int
    mutable echoDisabled: int

    //public state that is recalculated every sample
    mutable dirAddr: int
    mutable pitch: int
    mutable output: int
    mutable looped: int
    mutable echoPtr: int

    //left/right sums
    mainOut: int[]
    echoOut: int[]
    echoIn: int[]
    voice: Voice[]
    mutable step: int }

module Dsp =

  let [<Literal>] MVOLL = 0x0c
  let [<Literal>] MVOLR = 0x1c
  let [<Literal>] EVOLL = 0x2c
  let [<Literal>] EVOLR = 0x3c
  let [<Literal>] KON = 0x4c
  let [<Literal>] KOFF = 0x5c
  let [<Literal>] FLG = 0x6c
  let [<Literal>] ENDX = 0x7c
  let [<Literal>] EFB = 0x0d
  let [<Literal>] PMON = 0x2d
  let [<Literal>] NON = 0x3d
  let [<Literal>] EON = 0x4d
  let [<Literal>] DIR = 0x5d
  let [<Literal>] ESA = 0x6d
  let [<Literal>] EDL = 0x7d
  let [<Literal>] FIR = 0x0f
  let [<Literal>] VOLL = 0x00
  let [<Literal>] VOLR = 0x01
  let [<Literal>] PITCHL = 0x02
  let [<Literal>] PITCHH = 0x03
  let [<Literal>] SRCN = 0x04
  let [<Literal>] ADSR0 = 0x05
  let [<Literal>] ADSR1 = 0x06
  let [<Literal>] GAIN = 0x07
  let [<Literal>] ENVX = 0x08
  let [<Literal>] OUTX = 0x09

  // public constants
  let [<Literal>] EchoHistorySize = 8

  let [<Literal>] BrrBlockSize = 9
  let [<Literal>] CounterRange = 0x7800

  // gaussian
  let gaussianTable = [|
      0;    0;    0;    0;    0;    0;    0;    0;    0;    0;    0;    0;    0;    0;    0;    0
      1;    1;    1;    1;    1;    1;    1;    1;    1;    1;    1;    2;    2;    2;    2;    2
      2;    2;    3;    3;    3;    3;    3;    4;    4;    4;    4;    4;    5;    5;    5;    5
      6;    6;    6;    6;    7;    7;    7;    8;    8;    8;    9;    9;    9;    10;   10;   10
      11;   11;   11;   12;   12;   13;   13;   14;   14;   15;   15;   15;   16;   16;   17;   17
      18;   19;   19;   20;   20;   21;   21;   22;   23;   23;   24;   24;   25;   26;   27;   27
      28;   29;   29;   30;   31;   32;   32;   33;   34;   35;   36;   36;   37;   38;   39;   40
      41;   42;   43;   44;   45;   46;   47;   48;   49;   50;   51;   52;   53;   54;   55;   56
      58;   59;   60;   61;   62;   64;   65;   66;   67;   69;   70;   71;   73;   74;   76;   77
      78;   80;   81;   83;   84;   86;   87;   89;   90;   92;   94;   95;   97;   99;   100;  102
      104;  106;  107;  109;  111;  113;  115;  117;  118;  120;  122;  124;  126;  128;  130;  132
      134;  137;  139;  141;  143;  145;  147;  150;  152;  154;  156;  159;  161;  163;  166;  168
      171;  173;  175;  178;  180;  183;  186;  188;  191;  193;  196;  199;  201;  204;  207;  210
      212;  215;  218;  221;  224;  227;  230;  233;  236;  239;  242;  245;  248;  251;  254;  257
      260;  263;  267;  270;  273;  276;  280;  283;  286;  290;  293;  297;  300;  304;  307;  311
      314;  318;  321;  325;  328;  332;  336;  339;  343;  347;  351;  354;  358;  362;  366;  370
      374;  378;  381;  385;  389;  393;  397;  401;  405;  410;  414;  418;  422;  426;  430;  434
      439;  443;  447;  451;  456;  460;  464;  469;  473;  477;  482;  486;  491;  495;  499;  504
      508;  513;  517;  522;  527;  531;  536;  540;  545;  550;  554;  559;  563;  568;  573;  577
      582;  587;  592;  596;  601;  606;  611;  615;  620;  625;  630;  635;  640;  644;  649;  654
      659;  664;  669;  674;  678;  683;  688;  693;  698;  703;  708;  713;  718;  723;  728;  732
      737;  742;  747;  752;  757;  762;  767;  772;  777;  782;  787;  792;  797;  802;  806;  811
      816;  821;  826;  831;  836;  841;  846;  851;  855;  860;  865;  870;  875;  880;  884;  889
      894;  899;  904;  908;  913;  918;  923;  927;  932;  937;  941;  946;  951;  955;  960;  965
      969;  974;  978;  983;  988;  992;  997;  1001; 1005; 1010; 1014; 1019; 1023; 1027; 1032; 1036
      1040; 1045; 1049; 1053; 1057; 1061; 1066; 1070; 1074; 1078; 1082; 1086; 1090; 1094; 1098; 1102
      1106; 1109; 1113; 1117; 1121; 1125; 1128; 1132; 1136; 1139; 1143; 1146; 1150; 1153; 1157; 1160
      1164; 1167; 1170; 1174; 1177; 1180; 1183; 1186; 1190; 1193; 1196; 1199; 1202; 1205; 1207; 1210
      1213; 1216; 1219; 1221; 1224; 1227; 1229; 1232; 1234; 1237; 1239; 1241; 1244; 1246; 1248; 1251
      1253; 1255; 1257; 1259; 1261; 1263; 1265; 1267; 1269; 1270; 1272; 1274; 1275; 1277; 1279; 1280
      1282; 1283; 1284; 1286; 1287; 1288; 1290; 1291; 1292; 1293; 1294; 1295; 1296; 1297; 1297; 1298
      1299; 1300; 1300; 1301; 1302; 1302; 1303; 1303; 1303; 1304; 1304; 1304; 1304; 1304; 1305; 1305
    |]


  let counterRate = Array.map uint16 [|
    0x000; 0x800; 0x600
    0x500; 0x400; 0x300
    0x280; 0x200; 0x180
    0x140; 0x100; 0x0c0
    0x0a0; 0x080; 0x060
    0x050; 0x040; 0x030
    0x028; 0x020; 0x018
    0x014; 0x010; 0x00c
    0x00a; 0x008; 0x006
    0x005; 0x004; 0x003;
           0x002;
           0x001
  |]


  let counterOffset = Array.map uint16 [|
    0x000; 0x000; 0x410
    0x218; 0x000; 0x410
    0x218; 0x000; 0x410
    0x218; 0x000; 0x410
    0x218; 0x000; 0x410
    0x218; 0x000; 0x410
    0x218; 0x000; 0x410
    0x218; 0x000; 0x410
    0x218; 0x000; 0x410
    0x218; 0x000; 0x410;
           0x000;
           0x000
  |]


  let init () =
    let regs = Array.zeroCreate 128
    regs.[FLG] <- 0xe0uy

    { regs = regs
      echoHist = [| // echo history keeps most recent 8 samples
        ModuloArray.init EchoHistorySize
        ModuloArray.init EchoHistorySize
      |]

      echoHistPos = 0

      everyOtherSample = true  // toggles every sample
      keyOn = 0                  // KON value when last checked
      noise = 0x4000
      counter = 0
      echoOffset = 0           // offset from ESA in echo buffer
      echoLength = 0           // number of bytes that echoOffset will stop at

      //hidden registers also written to when main register is written to
      newKeyOn = 0

      endxBuf = 0
      envxBuf = 0
      outxBuf = 0

      //temporary state between clocks

      //read once per sample
      pitchModOn = 0

      noiseOn = 0
      echoOn = 0
      dir = 0
      keyOff = 0

      //read a few clocks ahead before used
      brrNextAddr = 0

      adsr0 = 0
      brrHeader = 0
      brrByte = 0
      srcN = 0
      esa = 0
      echoDisabled = 0

      //public state that is recalculated every sample
      dirAddr = 0

      pitch = 0
      output = 0
      looped = 0
      echoPtr = 0

      //left/right sums
      mainOut = Array.zeroCreate 2

      echoOut = Array.zeroCreate 2
      echoIn = Array.zeroCreate 2
      voice = Array.init 8 Voice.init
      step = 0 }


  let signedClamp (x: int) =
    match x with
    | x when x > +0x7fff -> +0x7fff
    | x when x < -0x8000 -> -0x8000
    | _ ->
      x


  let signedClip (x: int) =
    int32 (int16 x)


  let gaussianInterpolate (v: Voice) =
    //make pointers into gaussian table based on fractional position between samples
    let offset1 = (v.interpPos >>> 4) &&& 0xff
    let offset2 = (v.interpPos >>> 12) + v.bufPos

    let readOffset n = ModuloArray.read v.buffer (offset2 + n)

    let mutable output = 0
    output <-          ((gaussianTable.[255 - offset1] * (readOffset 0)) >>> 11)
    output <- output + ((gaussianTable.[511 - offset1] * (readOffset 1)) >>> 11)
    output <- output + ((gaussianTable.[256 + offset1] * (readOffset 2)) >>> 11)
    output <- signedClamp output
    output <- output + ((gaussianTable.[  0 + offset1] * (readOffset 3)) >>> 11)

    (signedClamp output) &&& (~~~1)


  //counter
  let counterTick dsp =
    dsp.counter <- dsp.counter - 1
    if dsp.counter < 0 then
      dsp.counter <- CounterRange - 1


  let counterPoll dsp (rate: int32) =
    if rate = 0 then
      false
    else
      let counter = uint32 dsp.counter
      let offset = uint32 counterOffset.[rate]
      let rate = uint32 counterRate.[rate]
      ((counter + offset) % rate) = 0u


  //envelope
  let envelopeRun dsp (v: Voice) =
    let mutable env = v.env

    if v.envMode = Envelope.EnvelopeR then // 60%
      env <- env - 0x8
      if env < 0 then
        env <- 0
      v.env <- env
    else
      let mutable rate = 0
      let mutable envData = int <| dsp.regs.[v.vidx + ADSR1]
      if (dsp.adsr0 &&& 0x80) <> 0 then // 99% ADSR
        if (v.envMode >= Envelope.EnvelopeD) then // 99%
          env <- env - 1
          env <- env - (env >>> 8)
          rate <- envData &&& 0x1f
          if v.envMode = Envelope.EnvelopeD then // 1%
            rate <- ((dsp.adsr0 >>> 3) &&& 0x0e) + 0x10
        else // envAttack
          rate <- ((dsp.adsr0 &&& 0x0f) <<< 1) + 1
          env <- env + (if rate < 31 then 0x20 else 0x400)
      else // GAIN
        envData <- int <| dsp.regs.[v.vidx + GAIN]
        let mode = envData >>> 5
        if mode < 4 then // direct
          env <- envData <<< 4
          rate <- 31
        else
          rate <- envData &&& 0x1f
          if mode = 4 then // 4: linear decrease
            env <- env - 0x20
          elif mode < 6 then // 5: exponential decrease
            env <- env - 1
            env <- env - (env >>> 8)
          else // 6, 7: linear increase
            env <- env + 0x20
            if mode > 6 && (v.hiddenEnv < 0 || v.hiddenEnv >= 0x600) then
              env <- env + (0x8 - 0x20); //7: two-slope linear increase

      //sustain level
      if (env >>> 8) = (envData >>> 5) && v.envMode = Envelope.EnvelopeD then
        v.envMode <- Envelope.EnvelopeS

      v.hiddenEnv <- env

      //unsigned cast because linear decrease underflowing also triggers this
      if env < 0 || env > 0x7ff then
        env <- if env < 0 then 0 else 0x7ff

        if v.envMode = Envelope.EnvelopeA then
          v.envMode <- Envelope.EnvelopeD

      if counterPoll dsp rate then
        v.env <- env


  //brr
  let brrDecode psram dsp (v: Voice) =
    let brr = psram (uint16 (v.brrAddr + v.brrOffset + 1))
    let mutable nybbles = (dsp.brrByte <<< 8) + (int brr)

    let filter = (dsp.brrHeader >>> 2) &&& 3
    let scale = (dsp.brrHeader >>> 4)

    //decode four samples
    for i in 0 .. 3 do
      //bits 12-15 = current nybble; sign extend, then shift right to 4-bit precision
      //result: s = 4-bit sign-extended sample value
      let mutable s = int32 ((int16)nybbles >>> 12)
      nybbles <- nybbles <<< 4; //slide nybble so that on next loop iteration, bits 12-15 = current nybble

      if scale <= 12 then
        s <- s <<< scale
        s <- s >>> 1
      else
        s <- s &&& (~~~0x7ff)

      //apply IIR filter (2 is the most commonly used)
      let p1 = ModuloArray.read v.buffer (v.bufPos - 1)
      let p2 = ModuloArray.read v.buffer (v.bufPos - 2) >>> 1

      match filter with
      | 0 -> () //no filter
      | 1 -> //s += p1 * 0.46875
        s <- s + (p1 >>> 1)
        s <- s + ((-p1) >>> 5)

      | 2 -> //s += p1 * 0.953125 - p2 * 0.46875
        s <- s + p1
        s <- s - p2
        s <- s + (p2 >>> 4)
        s <- s + ((p1 * -3) >>> 6)

      | _ -> //s += p1 * 0.8984375 - p2 * 0.40625
        s <- s + p1
        s <- s - p2
        s <- s + ((p1 * -13) >>> 7)
        s <- s + ((p2 * 3) >>> 4)

      //adjust and write sample
      s <- signedClamp(s)
      s <- signedClip (s <<< 1)
      ModuloArray.write v.buffer v.bufPos s
      v.bufPos <- v.bufPos + 1

      if v.bufPos >= Voice.BrrBufferSize then
        v.bufPos <- 0


  //misc
  let misc27 state =
    state.pitchModOn <- (int state.regs.[PMON]) &&& (~~~1); //voice 0 doesn't support PMON


  let misc28 state =
    state.noiseOn <- int <| state.regs.[NON]
    state.echoOn <- int <| state.regs.[EON]
    state.dir <- int <| state.regs.[DIR]


  let misc29 state =
    state.everyOtherSample <- not state.everyOtherSample

    if state.everyOtherSample then
      state.newKeyOn <- state.newKeyOn &&& (~~~state.keyOn); //clears KON 63 clocks after it was last read


  let misc30 state =
    if (state.everyOtherSample) then
      state.keyOn <- state.newKeyOn
      state.keyOff <- int <| state.regs.[KOFF]

    counterTick state

    //noise
    if counterPoll state ((int state.regs.[FLG]) &&& 0x1f) then
      let feedback = (state.noise <<< 13) ^^^ (state.noise <<< 14)
      state.noise <- (feedback &&& 0x4000) ^^^ (state.noise >>> 1)


  //voice
  let voiceOutput state (v: Voice) (channel: int) =
    //apply left/right volume
    let voll = (sbyte)(state.regs.[v.vidx + VOLL + channel])
    let amp = (state.output * (int voll)) >>> 7

    //add to output total
    state.mainOut.[channel] <- state.mainOut.[channel] + amp
    state.mainOut.[channel] <- signedClamp (state.mainOut.[channel])

    //optionally add to echo total
    if (state.echoOn &&& v.vbit) <> 0 then
      state.echoOut.[channel] <- state.echoOut.[channel] + amp
      state.echoOut.[channel] <- signedClamp (state.echoOut.[channel])


  let v1 state (v: Voice) =
    state.dirAddr <- (state.dir <<< 8) + (state.srcN <<< 2)
    state.srcN <- int <| state.regs.[v.vidx + SRCN]


  let v2 psram state (v: Voice) =
    //read sample pointer (ignored if not needed)
    let mutable addr = state.dirAddr
    if v.konDelay = 0 then
      addr <- addr + 2

    let lo = psram (uint16 (addr + 0)) |> int
    let hi = psram (uint16 (addr + 1)) |> int
    state.brrNextAddr <- ((hi <<< 8) + lo)

    state.adsr0 <- int <| state.regs.[v.vidx + ADSR0]

    //read pitch, spread over two clocks
    state.pitch <- int <| state.regs.[v.vidx + PITCHL]


  let v3A state (v: Voice) =
    let pitch = int <| state.regs.[v.vidx + PITCHH]
    state.pitch <- state.pitch + ((pitch &&& 0x3f) <<< 8)


  let v3B psram state (v: Voice) =
    state.brrByte <- psram (uint16 (v.brrAddr + v.brrOffset)) |> int
    state.brrHeader <- psram (uint16 (v.brrAddr)) |> int


  let v3C state (v: Voice) =
    //pitch modulation using previous voice's output
    if (state.pitchModOn &&& v.vbit) <> 0 then
      state.pitch <- state.pitch + (((state.output >>> 5) * state.pitch) >>> 10)

    if v.konDelay <> 0 then
      //get ready to start BRR decoding on next sample
      if v.konDelay = 5 then
        v.brrAddr <- state.brrNextAddr
        v.brrOffset <- 1
        v.bufPos <- 0
        state.brrHeader <- 0; //header is ignored on this sample

      //envelope is never run during KON
      v.env <- 0
      v.hiddenEnv <- 0

      //disable BRR decoding until last three samples
      v.interpPos <- 0
      v.konDelay <- v.konDelay - 1

      if (v.konDelay &&& 3) <> 0 then
        v.interpPos <- 0x4000

      //pitch is never added during KON
      state.pitch <- 0

    //gaussian interpolation
    let mutable output = gaussianInterpolate(v)

    //noise
    if ((state.noiseOn &&& v.vbit) <> 0) then
      output <- signedClip (state.noise <<< 1)

    //apply envelope
    state.output <- ((output * v.env) >>> 11) &&& (~~~1)
    v.envxOut <- v.env >>> 4

    //immediate silence due to end of sample or soft reset
    if (state.regs.[FLG] &&& 0x80uy) <> 0uy || (state.brrHeader &&& 3) = 1 then
      v.envMode <- Envelope.EnvelopeR
      v.env <- 0

    if state.everyOtherSample then
      //KOFF
      if (state.keyOff &&& v.vbit) <> 0 then
        v.envMode <- Envelope.EnvelopeR

      //KON
      if (state.keyOn &&& v.vbit) <> 0 then
        v.konDelay <- 5
        v.envMode <- Envelope.EnvelopeA

    //run envelope for next sample
    if v.konDelay = 0 then
      envelopeRun state v


  let v3 psram state (v: Voice) =
    v3A state v
    v3B psram state v
    v3C state v


  let v4 psram state (v: Voice) =
    //decode BRR
    state.looped <- 0

    if v.interpPos >= 0x4000 then
      brrDecode psram state v
      v.brrOffset <- v.brrOffset + 2

      if v.brrOffset >= 9 then
        //start decoding next BRR block
        v.brrAddr <- (v.brrAddr + 9) &&& 0xffff

        if (state.brrHeader &&& 1) <> 0 then
          v.brrAddr <- state.brrNextAddr
          state.looped <- v.vbit

        v.brrOffset <- 1

    //apply pitch
    v.interpPos <- (v.interpPos &&& 0x3fff) + state.pitch

    //keep from getting too far ahead (when using pitch modulation)
    if v.interpPos > 0x7fff then
      v.interpPos <- 0x7fff

    //output left
    voiceOutput state v 0


  let v5 state (v: Voice) =
    //output right
    voiceOutput state v 1

    //ENDX, OUTX and ENVX won't update if you wrote to them 1-2 clocks earlier
    state.endxBuf <- (int state.regs.[ENDX]) ||| state.looped

    //clear bit in ENDX if KON just began
    if v.konDelay = 5 then
      state.endxBuf <- state.endxBuf &&& (~~~v.vbit)


  let v6 state (v: Voice) =
    state.outxBuf <- (state.output >>> 8)


  let v7 state (v: Voice) =
    //update ENDX
    state.regs.[ENDX] <- (byte)state.endxBuf
    state.envxBuf <- v.envxOut


  let v8 state (v: Voice) =
    //update OUTX
    state.regs.[v.vidx + OUTX] <- byte state.outxBuf


  let v9 state (v: Voice) =
    //update ENVX
    state.regs.[v.vidx + ENVX] <- byte state.envxBuf


  //echo
  let calcFir state (i: int) (channel: int) =
    let fir = (sbyte)(state.regs.[FIR + i * 0x10])
    let s = ModuloArray.read state.echoHist.[channel] (state.echoHistPos + i + 1)
    (s * (int fir)) >>> 6


  let echoOutput state (channel: int) =
    let mvoll = (sbyte)(state.regs.[MVOLL + channel * 0x10])
    let evoll = (sbyte)(state.regs.[EVOLL + channel * 0x10])

    let output =
      ((state.mainOut.[channel] * (int mvoll)) >>> 7) +
      ((state.echoIn.[channel] * (int evoll)) >>> 7)

    signedClamp(output)


  let echoRead psram state (channel: int) =
    let addr = state.echoPtr + channel * 2
    let lo = psram (uint16 (addr + 0)) |> int
    let hi = psram (uint16 (addr + 1)) |> int
    let s = signedClip ((hi <<< 8) + lo)
    ModuloArray.write state.echoHist.[channel] state.echoHistPos (s >>> 1)


  let echoWrite psram state (channel: int) =
    if (state.echoDisabled &&& 0x20) = 0 then
      let addr = state.echoPtr + channel * 2
      let s = state.echoOut.[channel]
      psram (uint16 (addr + 0)) (uint8 (s >>> 0))
      psram (uint16 (addr + 1)) (uint8 (s >>> 8))

    state.echoOut.[channel] <- 0


  let echo22 psram state =
    //history
    state.echoHistPos <- state.echoHistPos + 1
    if state.echoHistPos >= EchoHistorySize then
      state.echoHistPos <- 0

    state.echoPtr <- ((state.esa <<< 8) + state.echoOffset) &&& 0xffff
    echoRead psram state 0

    //FIR
    let l = calcFir state 0 0
    let r = calcFir state 0 1

    state.echoIn.[0] <- l
    state.echoIn.[1] <- r


  let echo23 psram state =
    let l = (calcFir state 1 0) + (calcFir state 2 0)
    let r = (calcFir state 1 1) + (calcFir state 2 1)

    state.echoIn.[0] <- state.echoIn.[0] + l
    state.echoIn.[1] <- state.echoIn.[1] + r

    echoRead psram state 1


  let echo24 state =
    let l = (calcFir state 3 0) + (calcFir state 4 0) + (calcFir state 5 0)
    let r = (calcFir state 3 1) + (calcFir state 4 1) + (calcFir state 5 1)

    state.echoIn.[0] <- state.echoIn.[0] + l
    state.echoIn.[1] <- state.echoIn.[1] + r


  let echo25 state =
    let mutable l = state.echoIn.[0] + (calcFir state 6 0)
    let mutable r = state.echoIn.[1] + (calcFir state 6 1)

    l <- signedClip l
    r <- signedClip r

    l <- l + (calcFir state 7 0)
    r <- r + (calcFir state 7 1)

    state.echoIn.[0] <- (signedClamp l) &&& (~~~1)
    state.echoIn.[1] <- (signedClamp r) &&& (~~~1)


  let echo26 state =
    //left output volumes
    //(save sample for next clock so we can output both together)
    state.mainOut.[0] <- echoOutput state 0

    //echo feedback
    let efb = (sbyte)state.regs.[EFB]
    let l = state.echoOut.[0] + (signedClip ((state.echoIn.[0] * (int efb)) >>> 7))
    let r = state.echoOut.[1] + (signedClip ((state.echoIn.[1] * (int efb)) >>> 7))

    state.echoOut.[0] <- (signedClamp l) &&& (~~~1)
    state.echoOut.[1] <- (signedClamp r) &&& (~~~1)


  let echo27 dsp audio =
    //output
    let mutable outl = dsp.mainOut.[0]
    let mutable outr = echoOutput dsp 1
    dsp.mainOut.[0] <- 0
    dsp.mainOut.[1] <- 0

    //global muting isn't this simple
    //(turns DAC on and off or something, causing small ~37-sample pulse when first muted)
    if (dsp.regs.[FLG] &&& 0x40uy) <> 0uy then
      outl <- 0
      outr <- 0

    //output sample to DAC
    AudioSink.run audio outl
    AudioSink.run audio outr


  let echo28 state =
    state.echoDisabled <- int <| state.regs.[FLG]


  let echo29 psram state =
    state.esa <- int <| state.regs.[ESA]

    if state.echoOffset = 0 then
      state.echoLength <- ((int state.regs.[EDL]) &&& 0x0f) <<< 11

    state.echoOffset <- state.echoOffset + 4

    if (state.echoOffset >= state.echoLength) then
      state.echoOffset <- 0

    //write left echo
    echoWrite psram state 0

    state.echoDisabled <- int <| state.regs.[FLG]


  let echo30 psram state =
    //write right echo
    echoWrite psram state 1


  let tick (psramReader: uint16 -> uint8) psramWriter dsp audio =
    let v i = dsp.voice.[i]
    let v1  = v1  dsp
    let v2  = v2  psramReader dsp
    let v3  = v3  psramReader dsp
    let v3A = v3A dsp
    let v3B = v3B psramReader dsp
    let v3C = v3C dsp
    let v4  = v4  psramReader dsp
    let v5  = v5  dsp
    let v6  = v6  dsp
    let v7  = v7  dsp
    let v8  = v8  dsp
    let v9  = v9  dsp

    match dsp.step with
    | 0x00 -> v5 (v 0); v2(v 1)
    | 0x01 -> v6 (v 0); v3(v 1)
    | 0x02 -> v7 (v 0); v4(v 1); v1(v 3)
    | 0x03 -> v8 (v 0); v5(v 1); v2(v 2)
    | 0x04 -> v9 (v 0); v6(v 1); v3(v 2)
    | 0x05 -> v7 (v 1); v4(v 2); v1(v 4)
    | 0x06 -> v8 (v 1); v5(v 2); v2(v 3)
    | 0x07 -> v9 (v 1); v6(v 2); v3(v 3)
    | 0x08 -> v7 (v 2); v4(v 3); v1(v 5)
    | 0x09 -> v8 (v 2); v5(v 3); v2(v 4)
    | 0x0a -> v9 (v 2); v6(v 3); v3(v 4)
    | 0x0b -> v7 (v 3); v4(v 4); v1(v 6)
    | 0x0c -> v8 (v 3); v5(v 4); v2(v 5)
    | 0x0d -> v9 (v 3); v6(v 4); v3(v 5)
    | 0x0e -> v7 (v 4); v4(v 5); v1(v 7)
    | 0x0f -> v8 (v 4); v5(v 5); v2(v 6)
    | 0x10 -> v9 (v 4); v6(v 5); v3(v 6)
    | 0x11 -> v1 (v 0); v7(v 5); v4(v 6)
    | 0x12 -> v8 (v 5); v5(v 6); v2(v 7)
    | 0x13 -> v9 (v 5); v6(v 6); v3(v 7)
    | 0x14 -> v1 (v 1); v7(v 6); v4(v 7)
    | 0x15 -> v8 (v 6); v5(v 7); v2(v 0)
    | 0x16 -> v3A(v 0); v9(v 6); v6(v 7); echo22 psramReader dsp
    | 0x17 -> v7 (v 7); echo23 psramReader dsp
    | 0x18 -> v8 (v 7); echo24 dsp
    | 0x19 -> v3B(v 0); v9(v 7); echo25 dsp
    | 0x1a -> echo26 dsp
    | 0x1b -> misc27 dsp; echo27 dsp audio
    | 0x1c -> misc28 dsp; echo28 dsp
    | 0x1d -> misc29 dsp; echo29 psramWriter dsp
    | 0x1e -> misc30 dsp; v3C(v 0); echo30 psramWriter dsp
    | 0x1f -> v4(v 0); v1(v 2)
    | _ ->
      ()

    dsp.step <- (dsp.step + 1) &&& 0x1f


  let update psramReader psramWriter state audio (clocks: int<SpcCycle>) =
    for i in 0 .. 32 .. (clocks / 1<_>) do
      tick psramReader psramWriter state audio


  let read psramReader state =
    let addr = int <| (psramReader 0x00f2us : uint8)
    state.regs.[addr &&& 0x7f]


  let write psramReader state data =
    let addr = int <| (psramReader 0x00f2us : uint8)

    if (addr &&& 0x80) = 0 then
      state.regs.[addr] <- data

      match addr with
      | n when (n &&& 0x0f) = ENVX -> state.envxBuf <- int data
      | n when (n &&& 0x0f) = OUTX -> state.outxBuf <- int data
      | KON -> state.newKeyOn <- int data
      | ENDX ->
        state.endxBuf <- 0
        state.regs.[ENDX] <- 0uy
      | _ -> ()
