namespace SuperFamicom.SMP

type PSRAM =
  { buffer: byte[] }

module PSRAM =

  let init () =
    let buffer = Array.zeroCreate 65536

    buffer.[0x00f0] <- 0x0auy
    buffer.[0x00f1] <- 0xb0uy

    { buffer = buffer }


  let read psram (address: uint16) =
    psram.buffer.[int address]


  let write psram (address: uint16) data =
    psram.buffer.[int address] <- data
