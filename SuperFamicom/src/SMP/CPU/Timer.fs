namespace SuperFamicom.SMP.CPU

type Timer =
  { mutable enabled: bool
    mutable compare: int
    mutable stage1: int
    mutable stage2: int }

module Timer =

  open SuperFamicom

  let [<Literal>] Fast = 16<SpcCycle>
  let [<Literal>] Slow = 128<SpcCycle>


  let init () =
    { enabled = false
      compare = 255
      stage1 = 0
      stage2 = 0 }


  let update timer clocks =
    if timer.enabled then
      for _ in 1 .. clocks do
        timer.stage1 <- (timer.stage1 + 1) &&& 0xff

        if timer.stage1 = timer.compare then
          timer.stage1 <- 0
          timer.stage2 <- timer.stage2 + 1

      timer.stage2 <- timer.stage2 &&& 15
