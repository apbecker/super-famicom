namespace SuperFamicom.SMP.CPU

type Registers =
  { mutable pc: uint16
    mutable a: byte
    mutable s: byte
    mutable y: byte
    mutable x: byte }

module Registers =

  let init () =
    { pc = 0us
      a = 0uy
      s = 0uy
      x = 0uy
      y = 0uy }


  let pcl regs =
    uint8 (regs.pc >>> 0)


  let pch regs =
    uint8 (regs.pc >>> 8)


  let sp regs =
    0x100us ||| (uint16 regs.s)


  let ya regs =
    let y = uint16 regs.y
    let a = uint16 regs.a
    (y <<< 8) ||| a


  let putYA (value: uint16) regs =
    regs.y <- uint8 (value >>> 8)
    regs.a <- uint8 (value >>> 0)
