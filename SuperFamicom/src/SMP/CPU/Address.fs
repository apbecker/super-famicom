namespace SuperFamicom.SMP.CPU

[<Struct>]
type Address =
  | Address of uint16
