namespace SuperFamicom.SMP.CPU

open SuperFamicom
open SuperFamicom.SMP

type Smp =
  { registers: Registers
    timers: Timer[]
    port: byte[]
    mutable bootRomEnabled: bool
    mutable flagC: bool
    mutable flagZ: bool
    mutable flagH: bool
    mutable flagP: bool
    mutable flagV: bool
    mutable flagN: bool
    mutable timerCycles1: int<SpcCycle>
    mutable timerCycles2: int<SpcCycle>
    mutable cycles: int<CpuCycle * SpcCycle> }

module Smp =

  let private read16 r (Address(address)) =
    let l = uint16 <| r (Address(address + 0us))
    let h = uint16 <| r (Address(address + 1us))

    (h <<< 8) ||| l


  let private write16 write8 (Address(address)) data =
    write8 (Address(address + 0us)) ((byte)(data >>> 0))
    write8 (Address(address + 1us)) ((byte)(data >>> 8))


  let private immediateAddress smp =
    let address = smp.registers.pc
    smp.registers.pc <- smp.registers.pc + 1us
    Address(address)


  let private absoluteAddressIndexed r smp (index: uint8) =
    let l = uint16 <| r(immediateAddress smp)
    let h = uint16 <| r(immediateAddress smp)

    ((h <<< 8) ||| l) + (uint16 index)
      |> Address


  let private absoluteAddress r smp =
    absoluteAddressIndexed r smp 0uy


  let private absoluteAddressX r smp =
    absoluteAddressIndexed r smp smp.registers.x


  let private absoluteAddressY r smp =
    absoluteAddressIndexed r smp smp.registers.y


  let private absoluteAddress13 r smp =
    let (Address(address)) = absoluteAddress r smp
    let shift = (int address) >>> 13
    let mask = 1uy <<< shift
    (Address(address &&& 0x1fffus), mask)


  let private directPageAddressBase smp (offset: uint8) =
    if smp.flagP then
      Address((uint16 offset) ||| 0x100us)
    else
      Address((uint16 offset) ||| 0x000us)


  let private directPageAddress r smp =
    let dp = r (immediateAddress smp)
    directPageAddressBase smp dp


  let private directPageAddressIndirectIndexed r smp (index: uint8) =
    let dp = read16 r (directPageAddress r smp)
    dp + (uint16 index)
      |> Address


  let private directPageAddressIndirect r smp =
    directPageAddressIndirectIndexed r smp 0uy


  let private directPageAddressX r smp =
    let dp = r (immediateAddress smp)
    directPageAddressBase smp (dp + smp.registers.x)


  let private directPageAddressY r smp =
    let dp = r (immediateAddress smp)
    directPageAddressBase smp (dp + smp.registers.y)


  let private directPageAddressXIndirect r smp =
    Address(read16 r (directPageAddressX r smp))


  let private directPageAddressYIndirect r smp =
    directPageAddressIndirectIndexed r smp smp.registers.y


  let private directPageXAddress smp =
    directPageAddressBase smp smp.registers.x


  let private directPageXAddressIncrement smp =
    let x = smp.registers.x
    smp.registers.x <- smp.registers.x + 1uy
    directPageAddressBase smp x


  let private directPageYAddress smp =
    directPageAddressBase smp smp.registers.y


  let private yxAddress smp =
    let y = directPageYAddress smp
    let x = directPageXAddress smp
    (y, x)


  let private pullByte r smp =
    smp.registers.s <- smp.registers.s + 1uy
    r (Address(Registers.sp smp.registers))


  let private pullWord r smp =
    let l = uint16 <| pullByte r smp
    let h = uint16 <| pullByte r smp

    (h <<< 8) ||| l


  let private loadFlags smp value =
    smp.flagC <- (value &&& 0x01uy) <> 0uy
    smp.flagZ <- (value &&& 0x02uy) <> 0uy
    smp.flagH <- (value &&& 0x08uy) <> 0uy
    smp.flagP <- (value &&& 0x20uy) <> 0uy
    smp.flagV <- (value &&& 0x40uy) <> 0uy
    smp.flagN <- (value &&& 0x80uy) <> 0uy


  let private pushByte w smp value =
    w (Address(Registers.sp smp.registers)) value
    smp.registers.s <- smp.registers.s - 1uy


  let private pushWord w smp value =
    pushByte w smp ((byte)(value >>> 8))
    pushByte w smp ((byte)(value >>> 0))


  let private pushFlags w smp =
    let mutable flags = 0

    if smp.flagC then flags <- flags ||| 0x01
    if smp.flagZ then flags <- flags ||| 0x02
    if smp.flagH then flags <- flags ||| 0x08
    if smp.flagP then flags <- flags ||| 0x20
    if smp.flagV then flags <- flags ||| 0x40
    if smp.flagN then flags <- flags ||| 0x80

    pushByte w smp (uint8 flags)


  let private zn8 smp value =
    smp.flagZ <- value = 0x00uy
    smp.flagN <- value > 0x7fuy
    value


  let private zn16 smp value =
    smp.flagZ <- (value &&& 0xffffus) = 0us
    smp.flagN <- (value &&& 0x8000us) <> 0us
    value


  let private shl8 smp value carry =
    smp.flagC <- (value &&& 0x80uy) <> 0uy
    let value = (value <<< 1) ||| (byte carry)
    zn8 smp value


  let private shr8 smp value carry =
    smp.flagC <- (value &&& 0x01uy) <> 0uy
    let value = (value >>> 1) ||| (byte carry)
    zn8 smp value


  let private and8 smp value =
    smp.registers.a <- zn8 smp (smp.registers.a &&& value)


  let private eor8 smp value =
    smp.registers.a <- zn8 smp (smp.registers.a ^^^ value)


  let private ora8 smp value =
    smp.registers.a <- zn8 smp (smp.registers.a ||| value)


  let private asl8 smp value =
    shl8 smp value 0


  let private lsr8 smp value =
    shr8 smp value 0


  let private rol8 smp value =
    shl8 smp value (if smp.flagC then 0x01 else 0)


  let private ror8 smp value =
    shr8 smp value (if smp.flagC then 0x80 else 0)


  let private adc8 smp left right =
    let temporary32 = (int left) + (int right) + (if smp.flagC then 1 else 0)
    smp.flagC <- (temporary32 > 0xff)
    let temporary = byte temporary32
    smp.flagV <- (~~~(left ^^^ right) &&& (right ^^^ temporary) &&& 0x80uy) <> 0uy
    smp.flagH <- ((left ^^^ right ^^^ temporary) &&& 0x10uy) <> 0uy
    zn8 smp temporary


  let private add8 smp left right =
    smp.flagC <- false

    let lower = adc8 smp (byte (left >>> 0)) (byte (right >>> 0))
    let upper = adc8 smp (byte (left >>> 8)) (byte (right >>> 8))

    let temporary =
      ((uint16 lower) <<< 0) |||
      ((uint16 upper) <<< 8)

    smp.flagZ <- temporary = 0us

    temporary


  let private sbc8 smp left right =
    adc8 smp left (~~~right)


  let private sub8 smp left right =
    add8 smp left ((~~~right) + 1us)


  let private cmp8 smp left right =
    smp.flagC <- left >= right
    ignore <| zn8 smp (left - right)


  let private cmp16 smp left right =
    smp.flagC <- left >= right
    ignore <| zn16 smp (left - right)


  let private addCycles smp amount =
    smp.timerCycles1 <- smp.timerCycles1 + amount
    smp.timerCycles2 <- smp.timerCycles2 + amount


  let private takeBranch r smp =
    let address = immediateAddress smp
    let offset = uint16 (int8 (r address))
    smp.registers.pc <- smp.registers.pc + offset
    addCycles smp 2<SpcCycle>


  let private branch r smp flag =
    if flag then
      takeBranch r smp
    else
      smp.registers.pc <- smp.registers.pc + 1us


  //#region Codes


  let private op00 r w smp =
    ()


  let private op01 r w smp =
    let data = read16 r (Address(0xffdeus))
    pushWord w smp smp.registers.pc
    smp.registers.pc <- data


  let private op02 r w smp =
    let address = directPageAddress r smp
    let data = (r address) ||| 0x01uy
    w address data


  let private op03 r w smp =
    branch r smp ((r (directPageAddress r smp) &&& 1uy) <> 0uy)


  let private op04 r w smp =
    ora8 smp (r (directPageAddress r smp))


  let private op05 r w smp =
    ora8 smp (r (absoluteAddress r smp))


  let private op06 r w smp =
    ora8 smp (r (directPageXAddress smp))


  let private op07 r w smp =
    ora8 smp (r (directPageAddressXIndirect r smp))


  let private op08 r w smp =
    ora8 smp (r (immediateAddress smp))


  let private op09 r w smp =
    let data = r (directPageAddress r smp)
    let address = directPageAddress r smp
    let data = zn8 smp (data ||| (r address))
    w address data


  let private op0A r w smp =
    let (address, mask) = absoluteAddress13 r smp

    if ((r address) &&& mask) <> 0uy then
      smp.flagC <- true


  let private op0B r w smp =
    let address = directPageAddress r smp
    w address (asl8 smp (r address))


  let private op0C r w smp =
    let address = absoluteAddress r smp
    w address (asl8 smp (r address))


  let private op0D r w smp =
    pushFlags w smp


  let private op0E r w smp =
    let address = absoluteAddress r smp
    let data = r address
    ignore <| zn8 smp (smp.registers.a - data)
    w address (data ||| smp.registers.a)


  let private op0F r w smp =
    let data = read16 r (Address(0xfffeus))
    pushWord w smp smp.registers.pc
    pushFlags w smp
    smp.registers.pc <- data


  let private op10 r w smp =
    branch r smp (not smp.flagN)


  let private op11 r w smp =
    let data = read16 r (Address(0xffdcus))
    pushWord w smp smp.registers.pc
    smp.registers.pc <- data


  let private op12 r w smp =
    let address = directPageAddress r smp
    let data = (r address) &&& (~~~0x01uy)
    w address data


  let private op13 r w smp =
    branch r smp ((r (directPageAddress r smp) &&& 1uy) = 0uy)


  let private op14 r w smp =
    ora8 smp (r (directPageAddressX r smp))


  let private op15 r w smp =
    ora8 smp (r (absoluteAddressX r smp))


  let private op16 r w smp =
    ora8 smp (r (absoluteAddressY r smp))


  let private op17 r w smp =
    ora8 smp (r (directPageAddressYIndirect r smp))


  let private op18 r w smp =
    let data = r (immediateAddress smp)
    let address = directPageAddress r smp
    let data = zn8 smp (data ||| (r address))
    w address data


  let private op19 r w smp =
    let (y, x) = yxAddress smp
    let data = zn8 smp ((r y) ||| (r x))
    w x data


  let private op1A r w smp =
    let address = directPageAddress r smp
    let data = zn16 smp ((read16 r address) - 1us)
    write16 w address data


  let private op1B r w smp =
    let address = directPageAddressX r smp
    w address (asl8 smp (r address))


  let private op1C r w smp =
    smp.registers.a <- asl8 smp smp.registers.a


  let private op1D r w smp =
    smp.registers.x <- zn8 smp (smp.registers.x - 1uy)


  let private op1E r w smp =
    cmp8 smp smp.registers.x (r (absoluteAddress r smp))


  let private op1F r w smp =
    smp.registers.pc <- (read16 r (absoluteAddressX r smp))


  let private op20 r w smp =
    smp.flagP <- false


  let private op21 r w smp =
    let data = read16 r (Address(0xffdaus))
    pushWord w smp smp.registers.pc
    smp.registers.pc <- data


  let private op22 r w smp =
    let address = directPageAddress r smp
    let data = (r address) ||| 0x02uy
    w address data


  let private op23 r w smp =
    branch r smp ((r (directPageAddress r smp) &&& 2uy) <> 0uy)


  let private op24 r w smp =
    and8 smp (r (directPageAddress r smp))


  let private op25 r w smp =
    and8 smp (r (absoluteAddress r smp))


  let private op26 r w smp =
    and8 smp (r (directPageXAddress smp))


  let private op27 r w smp =
    and8 smp (r (directPageAddressXIndirect r smp))


  let private op28 r w smp =
    and8 smp (r (immediateAddress smp))


  let private op29 r w smp =
    let data = r (directPageAddress r smp)
    let address = directPageAddress r smp
    let data = zn8 smp (data &&& (r address))
    w address data


  let private op2A r w smp =
    let (address, mask) = absoluteAddress13 r smp

    if ((r address) &&& mask) = 0uy then
      smp.flagC <- true


  let private op2B r w smp =
    let address = directPageAddress r smp
    w address (rol8 smp (r address))


  let private op2C r w smp =
    let address = absoluteAddress r smp
    w address (rol8 smp (r address))


  let private op2D r w smp =
    pushByte w smp smp.registers.a


  let private op2E r w smp =
    branch r smp (r (directPageAddress r smp) <> smp.registers.a)


  let private op2F r w smp =
    takeBranch r smp


  let private op30 r w smp =
    branch r smp smp.flagN


  let private op31 r w smp =
    let data = read16 r (Address(0xffd8us))
    pushWord w smp smp.registers.pc
    smp.registers.pc <- data


  let private op32 r w smp =
    let address = directPageAddress r smp
    let data = (r address) &&& (~~~0x02uy)
    w address data


  let private op33 r w smp =
    branch r smp ((r (directPageAddress r smp) &&& 2uy) = 0uy)


  let private op34 r w smp =
    and8 smp (r (directPageAddressX r smp))


  let private op35 r w smp =
    and8 smp (r (absoluteAddressX r smp))


  let private op36 r w smp =
    and8 smp (r (absoluteAddressY r smp))


  let private op37 r w smp =
    and8 smp (r (directPageAddressYIndirect r smp))


  let private op38 r w smp =
    let data = r (immediateAddress smp)
    let address = directPageAddress r smp
    let data = zn8 smp (data &&& (r address))
    w address data


  let private op39 r w smp =
    let (y, x) = yxAddress smp
    let data = zn8 smp ((r y) &&& (r x))
    w x data


  let private op3A r w smp =
    let address = directPageAddress r smp
    let data = zn16 smp ((read16 r address) + 1us)
    write16 w address data


  let private op3B r w smp =
    let address = directPageAddressX r smp
    w address (rol8 smp (r address))


  let private op3C r w smp =
    smp.registers.a <- rol8 smp smp.registers.a


  let private op3D r w smp =
    smp.registers.x <- zn8 smp (smp.registers.x + 1uy)


  let private op3E r w smp =
    cmp8 smp smp.registers.x (r (directPageAddress r smp))


  let private op3F r w smp =
    let (Address(value)) = absoluteAddress r smp
    pushWord w smp smp.registers.pc
    smp.registers.pc <- value


  let private op40 r w smp =
    smp.flagP <- true


  let private op41 r w smp =
    let data = read16 r (Address(0xffd6us))
    pushWord w smp smp.registers.pc
    smp.registers.pc <- data


  let private op42 r w smp =
    let address = directPageAddress r smp
    let data = (r address) ||| 0x04uy
    w address data


  let private op43 r w smp =
    branch r smp ((r (directPageAddress r smp) &&& 4uy) <> 0uy)


  let private op44 r w smp =
    eor8 smp (r (directPageAddress r smp))


  let private op45 r w smp =
    eor8 smp (r (absoluteAddress r smp))


  let private op46 r w smp =
    eor8 smp (r (directPageXAddress smp))


  let private op47 r w smp =
    eor8 smp (r (directPageAddressXIndirect r smp))


  let private op48 r w smp =
    eor8 smp (r (immediateAddress smp))


  let private op49 r w smp =
    let data = r (directPageAddress r smp)
    let address = directPageAddress r smp
    let data = zn8 smp (data ^^^ (r address))
    w address data


  let private op4A r w smp =
    let (address, mask) = absoluteAddress13 r smp
    
    smp.flagC <- smp.flagC && ((r address) &&& mask) <> 0uy


  let private op4B r w smp =
    let address = directPageAddress r smp
    w address (lsr8 smp (r address))


  let private op4C r w smp =
    let address = absoluteAddress r smp
    w address (lsr8 smp (r address))


  let private op4D r w smp =
    pushByte w smp smp.registers.x


  let private op4E r w smp =
    let address = absoluteAddress r smp
    let data = r address
    ignore <| zn8 smp (smp.registers.a - data)
    w address (data &&& (~~~smp.registers.a))


  let private op4F r w smp =
    let data = 0xff00us ||| (uint16 <| r(immediateAddress smp))
    pushWord w smp smp.registers.pc
    smp.registers.pc <- data


  let private op50 r w smp =
    branch r smp (not smp.flagV)


  let private op51 r w smp =
    let data = read16 r (Address(0xffd4us))
    pushWord w smp smp.registers.pc
    smp.registers.pc <- data


  let private op52 r w smp =
    let address = directPageAddress r smp
    let data = (r address) &&& (~~~0x04uy)
    w address data


  let private op53 r w smp =
    branch r smp ((r (directPageAddress r smp) &&& 4uy) = 0uy)


  let private op54 r w smp =
    eor8 smp (r (directPageAddressX r smp))


  let private op55 r w smp =
    eor8 smp (r(absoluteAddressX r smp))


  let private op56 r w smp =
    eor8 smp (r(absoluteAddressY r smp))


  let private op57 r w smp =
    eor8 smp (r (directPageAddressYIndirect r smp))


  let private op58 r w smp =
    let mutable data = r (immediateAddress smp)
    let address = directPageAddress r smp
    data <- zn8 smp (data ^^^ (r address))
    w address data


  let private op59 r w smp =
    let (y, x) = yxAddress smp
    let data = zn8 smp ((r y) ^^^ (r x))
    w x data


  let private op5A r w smp =
    let (Address(value)) = directPageAddressIndirect r smp
    cmp16 smp (Registers.ya smp.registers) value


  let private op5B r w smp =
    let address = directPageAddressX r smp
    w address (lsr8 smp (r address))


  let private op5C r w smp =
    smp.registers.a <- lsr8 smp smp.registers.a


  let private op5D r w smp =
    smp.registers.x <- zn8 smp smp.registers.a


  let private op5E r w smp =
    cmp8 smp smp.registers.y (r (absoluteAddress r smp))


  let private op5F r w smp =
    let (Address(value)) = absoluteAddress r smp
    smp.registers.pc <- value


  let private op60 r w smp =
    smp.flagC <- false


  let private op61 r w smp =
    let data = read16 r (Address(0xffd2us))
    pushWord w smp smp.registers.pc
    smp.registers.pc <- data


  let private op62 r w smp =
    let address = directPageAddress r smp
    let data = (r address) ||| 0x08uy
    w address data


  let private op63 r w smp =
    branch r smp ((r (directPageAddress r smp) &&& 8uy) <> 0uy)


  let private op64 r w smp =
    cmp8 smp smp.registers.a (r(directPageAddress r smp))


  let private op65 r w smp =
    cmp8 smp smp.registers.a (r(absoluteAddress r smp))


  let private op66 r w smp =
    cmp8 smp smp.registers.a (r(directPageXAddress smp))


  let private op67 r w smp =
    cmp8 smp smp.registers.a (r(directPageAddressXIndirect r smp))


  let private op68 r w smp =
    cmp8 smp smp.registers.a (r(immediateAddress smp))


  let private op69 r w smp =
    let data = r(directPageAddress r smp)
    cmp8 smp (r(directPageAddress r smp)) data


  let private op6A r w smp =
    let (address, mask) = absoluteAddress13 r smp
    
    smp.flagC <- smp.flagC && (r(address) &&& mask) = 0uy


  let private op6B r w smp =
    let address = directPageAddress r smp
    w address (ror8 smp (r address))


  let private op6C r w smp =
    let address = absoluteAddress r smp
    w address (ror8 smp (r address))


  let private op6D r w smp =
    pushByte w smp smp.registers.y


  let private op6E r w smp =
    let address = directPageAddress r smp
    let data = (r address) - 1uy
    w address data
    branch r smp (data <> 0uy)


  let private op6F r w smp =
    smp.registers.pc <- pullWord r smp


  let private op70 r w smp =
    branch r smp smp.flagV


  let private op71 r w smp =
    let data = read16 r (Address(0xffd0us))
    pushWord w smp smp.registers.pc
    smp.registers.pc <- data


  let private op72 r w smp =
    let address = directPageAddress r smp
    let data = (r address) &&& (~~~0x08uy)
    w address data


  let private op73 r w smp =
    branch r smp ((r (directPageAddress r smp) &&& 8uy) = 0uy)


  let private op74 r w smp =
    cmp8 smp smp.registers.a (r (directPageAddressX r smp))


  let private op75 r w smp =
    cmp8 smp smp.registers.a (r (absoluteAddressX r smp))


  let private op76 r w smp =
    cmp8 smp smp.registers.a (r (absoluteAddressY r smp))


  let private op77 r w smp =
    cmp8 smp smp.registers.a (r (directPageAddressYIndirect r smp))


  let private op78 r w smp =
    let data = r(immediateAddress smp)
    cmp8 smp (r(directPageAddress r smp)) data


  let private op79 r w smp =
    let (y, x) = yxAddress smp
    let b = r y
    let a = r x
    cmp8 smp a b


  let private op7A r w smp =
    let (Address(value)) = directPageAddressIndirect r smp
    let ya = Registers.ya smp.registers
    smp.registers
      |> Registers.putYA (add8 smp ya value)


  let private op7B r w smp =
    let address = directPageAddressX r smp
    w address (ror8 smp (r address))


  let private op7C r w smp =
    smp.registers.a <- ror8 smp smp.registers.a


  let private op7D r w smp =
    smp.registers.a <- zn8 smp smp.registers.x


  let private op7E r w smp =
    cmp8 smp smp.registers.y (r (directPageAddress r smp))


  let private op7F r w smp =
    loadFlags smp (pullByte r smp)
    smp.registers.pc <- pullWord r smp


  let private op80 r w smp =
    smp.flagC <- true


  let private op81 r w smp =
    let data = read16 r (Address(0xffceus))
    pushWord w smp smp.registers.pc
    smp.registers.pc <- data


  let private op82 r w smp =
    let address = directPageAddress r smp
    let data = (r address) ||| 0x10uy
    w address data


  let private op83 r w smp =
    branch r smp ((r (directPageAddress r smp) &&& 16uy) <> 0uy)


  let private op84 r w smp =
    smp.registers.a <- adc8 smp smp.registers.a (r (directPageAddress r smp))


  let private op85 r w smp =
    smp.registers.a <- adc8 smp smp.registers.a (r (absoluteAddress r smp))


  let private op86 r w smp =
    smp.registers.a <- adc8 smp smp.registers.a (r (directPageXAddress smp))


  let private op87 r w smp =
    smp.registers.a <- adc8 smp smp.registers.a (r (directPageAddressXIndirect r smp))


  let private op88 r w smp =
    smp.registers.a <- adc8 smp smp.registers.a (r (immediateAddress smp))


  let private op89 r w smp =
    let data = r(directPageAddress r smp)
    let address = directPageAddress r smp
    let data = adc8 smp (r address) data
    w address data


  let private op8A r w smp =
    let (address, mask) = absoluteAddress13 r smp

    if ((r address) &&& mask) <> 0uy then
      smp.flagC <- not smp.flagC


  let private op8B r w smp =
    let address = directPageAddress r smp
    let data = zn8 smp ((r address) - 1uy)
    w address data


  let private op8C r w smp =
    let address = absoluteAddress r smp
    let data = zn8 smp ((r address) - 1uy)
    w address data


  let private op8D r w smp =
    smp.registers.y <- zn8 smp (r (immediateAddress smp))


  let private op8E r w smp =
    loadFlags smp (pullByte r smp)


  let private op8F r w smp =
    let data = r (immediateAddress smp)
    w (directPageAddress r smp) data


  let private op90 r w smp =
    branch r smp (not smp.flagC)


  let private op91 r w smp =
    let data = read16 r (Address(0xffccus))
    pushWord w smp smp.registers.pc
    smp.registers.pc <- data


  let private op92 r w smp =
    let address = directPageAddress r smp
    let data = (r address) &&& (~~~0x10uy)
    w address data


  let private op93 r w smp =
    branch r smp ((r (directPageAddress r smp) &&& 16uy) = 0uy)


  let private op94 r w smp =
    smp.registers.a <- adc8 smp smp.registers.a (r(directPageAddressX r smp))


  let private op95 r w smp =
    smp.registers.a <- adc8 smp smp.registers.a (r(absoluteAddressX r smp))


  let private op96 r w smp =
    smp.registers.a <- adc8 smp smp.registers.a (r(absoluteAddressY r smp))


  let private op97 r w smp =
    smp.registers.a <- adc8 smp smp.registers.a (r(directPageAddressYIndirect r smp))


  let private op98 r w smp =
    let data = r (immediateAddress smp)
    let address = directPageAddress r smp
    let data = adc8 smp (r address) data
    w address data


  let private op99 r w smp =
    let (y, x) = yxAddress smp
    let b = r y
    let a = r x
    w x (adc8 smp a b)


  let private op9A r w smp =
    let (Address(value)) = directPageAddressIndirect r smp
    let ya = Registers.ya smp.registers
    smp.registers
      |> Registers.putYA (sub8 smp ya value)


  let private op9B r w smp =
    let address = directPageAddressX r smp
    let data = zn8 smp ((r address) - 1uy)
    w address data


  let private op9C r w smp =
    smp.registers.a <- zn8 smp (smp.registers.a - 1uy)


  let private op9D r w smp =
    smp.registers.x <- zn8 smp smp.registers.s


  let private op9E r w smp =
    smp.flagV <- (smp.registers.y &&& 0xffuy) >= (smp.registers.x &&& 0xffuy)
    smp.flagH <- (smp.registers.y &&& 0x0fuy) >= (smp.registers.x &&& 0x0fuy)

    let ya = smp.registers |> Registers.ya
    let y = ya % (uint16 smp.registers.x)
    let a = ya / (uint16 smp.registers.x)

    smp.registers.y <- byte y
    smp.registers.a <- byte a |> zn8 smp


  let private op9F r w smp =
    let value = (smp.registers.a <<< 4) ||| (smp.registers.a >>> 4)
    smp.registers.a <- zn8 smp value


  let private opA0 r w smp =
    ()


  let private opA1 r w smp =
    let data = read16 r (Address(0xffcaus))
    pushWord w smp smp.registers.pc
    smp.registers.pc <- data


  let private opA2 r w smp =
    let address = directPageAddress r smp
    let data = (r address) ||| 0x20uy
    w address data


  let private opA3 r w smp =
    branch r smp ((r (directPageAddress r smp) &&& 0x20uy) <> 0uy)


  let private opA4 r w smp =
    smp.registers.a <- sbc8 smp smp.registers.a (r(directPageAddress r smp))


  let private opA5 r w smp =
    smp.registers.a <- sbc8 smp smp.registers.a (r(absoluteAddress r smp))


  let private opA6 r w smp =
    smp.registers.a <- sbc8 smp smp.registers.a (r(directPageXAddress smp))


  let private opA7 r w smp =
    smp.registers.a <- sbc8 smp smp.registers.a (r(directPageAddressXIndirect r smp))


  let private opA8 r w smp =
    smp.registers.a <- sbc8 smp smp.registers.a (r(immediateAddress smp))


  let private opA9 r w smp =
    let data = r(directPageAddress r smp)
    let address = directPageAddress r smp
    let data = sbc8 smp (r address) data
    w address data


  let private opAA r w smp =
    let (address, mask) = absoluteAddress13 r smp

    smp.flagC <- ((r address) &&& mask) <> 0uy


  let private opAB r w smp =
    let address = directPageAddress r smp
    let data = zn8 smp ((r address) + 1uy)
    w address data


  let private opAC r w smp =
    let address = absoluteAddress r smp
    let data = zn8 smp ((r address) + 1uy)
    w address data


  let private opAD r w smp =
    cmp8 smp smp.registers.y (r(immediateAddress smp))


  let private opAE r w smp =
    smp.registers.a <- pullByte r smp


  let private opAF r w smp =
    w (directPageXAddressIncrement smp) smp.registers.a


  let private opB0 r w smp =
    branch r smp smp.flagC


  let private opB1 r w smp =
    let data = read16 r (Address(0xffc8us))
    pushWord w smp smp.registers.pc
    smp.registers.pc <- data


  let private opB2 r w smp =
    let address = directPageAddress r smp
    let data = (r address) &&& (~~~0x20uy)
    w address data


  let private opB3 r w smp =
    branch r smp ((r(directPageAddress r smp) &&& 0x20uy) = 0uy)


  let private opB4 r w smp =
    smp.registers.a <- sbc8 smp smp.registers.a (r(directPageAddressX r smp))


  let private opB5 r w smp =
    smp.registers.a <- sbc8 smp smp.registers.a (r(absoluteAddressX r smp))


  let private opB6 r w smp =
    smp.registers.a <- sbc8 smp smp.registers.a (r(absoluteAddressY r smp))


  let private opB7 r w smp =
    smp.registers.a <- sbc8 smp smp.registers.a (r(directPageAddressYIndirect r smp))


  let private opB8 r w smp =
    let data = r(immediateAddress smp)
    let address = directPageAddress r smp
    let data = sbc8 smp (r address) data
    w address data


  let private opB9 r w smp =
    let (y, x) = yxAddress smp
    let b = r y
    let a = r x
    w x (sbc8 smp a b)


  let private opBA r w smp =
    let (Address(value)) = directPageAddressIndirect r smp
    Registers.putYA (zn16 smp value) smp.registers


  let private opBB r w smp =
    let address = directPageAddressX r smp
    let data = zn8 smp ((r address) + 1uy)
    w address data


  let private opBC r w smp =
    smp.registers.a <- zn8 smp (smp.registers.a + 1uy)


  let private opBD r w smp =
    smp.registers.s <- smp.registers.x


  let private opBE r w smp =
    let mutable data = smp.registers.a
    if (not smp.flagC) || data > 153uy then
      data <- data - 96uy
      smp.flagC <- false

    if (not smp.flagH) || (data &&& 0x0fuy) > 9uy then
      data <- data - 6uy

    smp.registers.a <- zn8 smp data


  let private opBF r w smp =
    smp.registers.a <- zn8 smp (r (directPageXAddressIncrement smp))


  let private opC0 r w smp =
    ()


  let private opC1 r w smp =
    let data = read16 r (Address(0xffc6us))
    pushWord w smp smp.registers.pc
    smp.registers.pc <- data


  let private opC2 r w smp =
    let address = directPageAddress r smp
    let data = (r address) ||| 0x40uy
    w address data


  let private opC3 r w smp =
    branch r smp ((r(directPageAddress r smp) &&& 64uy) <> 0uy)


  let private opC4 r w smp =
    w (directPageAddress r smp) smp.registers.a


  let private opC5 r w smp =
    w (absoluteAddress r smp) smp.registers.a


  let private opC6 r w smp =
    w (directPageXAddress smp) smp.registers.a


  let private opC7 r w smp =
    w (directPageAddressXIndirect r smp) smp.registers.a


  let private opC8 r w smp =
    cmp8 smp smp.registers.x (r(immediateAddress smp))


  let private opC9 r w smp =
    w (absoluteAddress r smp) smp.registers.x


  let private opCA r w smp =
    let (address, mask) = absoluteAddress13 r smp
    let data = ((r address) &&& (~~~mask)) ||| (if smp.flagC then mask else 0uy)
    w address data


  let private opCB r w smp =
    w (directPageAddress r smp) smp.registers.y


  let private opCC r w smp =
    w (absoluteAddress r smp) smp.registers.y


  let private opCD r w smp =
    smp.registers.x <- zn8 smp (r (immediateAddress smp))


  let private opCE r w smp =
    smp.registers.x <- pullByte r smp


  let private opCF r w smp =
    let ya = (uint16 smp.registers.y) * (uint16 smp.registers.a)

    Registers.putYA ya smp.registers

    smp.flagN <- (smp.registers.y &&& 0x80uy) <> 0uy
    smp.flagZ <- (smp.registers.y = 0uy) && (smp.registers.a = 0uy)


  let private opD0 r w smp =
    branch r smp (not smp.flagZ)


  let private opD1 r w smp =
    let data = read16 r (Address(0xffc4us))
    pushWord w smp smp.registers.pc
    smp.registers.pc <- data


  let private opD2 r w smp =
    let address = directPageAddress r smp
    let data = (r address) &&& (~~~0x40uy)
    w address data


  let private opD3 r w smp =
    branch r smp ((r(directPageAddress r smp) &&& 64uy) = 0uy)


  let private opD4 r w smp =
    w (directPageAddressX r smp) smp.registers.a


  let private opD5 r w smp =
    w (absoluteAddressX r smp) smp.registers.a


  let private opD6 r w smp =
    w (absoluteAddressY r smp) smp.registers.a


  let private opD7 r w smp =
    w (directPageAddressYIndirect r smp) smp.registers.a


  let private opD8 r w smp =
    w (directPageAddress r smp) smp.registers.x


  let private opD9 r w smp =
    w (directPageAddressY r smp) smp.registers.x


  let private opDA r w smp =
    write16 w (directPageAddress r smp) (Registers.ya smp.registers)


  let private opDB r w smp =
    w (directPageAddressX r smp) smp.registers.y


  let private opDC r w smp =
    smp.registers.y <- zn8 smp (smp.registers.y - 1uy)


  let private opDD r w smp =
    smp.registers.a <- zn8 smp smp.registers.y


  let private opDE r w smp =
    branch r smp (r(directPageAddressX r smp) <> smp.registers.a)


  let private opDF r w smp =
    let mutable data = smp.registers.a
    if (smp.flagC || data > 153uy) then
      data <- data + 96uy
      smp.flagC <- true

    if (smp.flagH || (data &&& 15uy) > 9uy) then
      data <- data + 6uy

    smp.registers.a <- zn8 smp data


  let private opE0 r w smp =
    smp.flagV <- false
    smp.flagH <- false


  let private opE1 r w smp =
    let data = read16 r (Address(0xffc2us))
    pushWord w smp smp.registers.pc
    smp.registers.pc <- data


  let private opE2 r w smp =
    let address = directPageAddress r smp
    let data = (r address) ||| 0x80uy
    w address data


  let private opE3 r w smp =
    branch r smp ((r(directPageAddress r smp) &&& 128uy) <> 0uy)


  let private opE4 r w smp =
    smp.registers.a <- zn8 smp (r (directPageAddress r smp))


  let private opE5 r w smp =
    smp.registers.a <- zn8 smp (r (absoluteAddress r smp))


  let private opE6 r w smp =
    smp.registers.a <- zn8 smp (r (directPageXAddress smp))


  let private opE7 r w smp =
    smp.registers.a <- zn8 smp (r (directPageAddressXIndirect r smp))


  let private opE8 r w smp =
    smp.registers.a <- zn8 smp (r (immediateAddress smp))


  let private opE9 r w smp =
    smp.registers.x <- zn8 smp (r (absoluteAddress r smp))


  let private opEA r w smp =
    let (address, mask) = absoluteAddress13 r smp

    w address ((r address) ^^^ mask)


  let private opEB r w smp =
    smp.registers.y <- zn8 smp (r (directPageAddress r smp))


  let private opEC r w smp =
    smp.registers.y <- zn8 smp (r (absoluteAddress r smp))


  let private opED r w smp =
    smp.flagC <- not smp.flagC


  let private opEE r w smp =
    smp.registers.y <- pullByte r smp


  let private opEF r w smp =
    smp.registers.pc <- smp.registers.pc - 1us


  let private opF0 r w smp =
    branch r smp smp.flagZ


  let private opF1 r w smp =
    let data = read16 r (Address(0xffc0us))
    pushWord w smp smp.registers.pc
    smp.registers.pc <- data


  let private opF2 r w smp =
    let address = directPageAddress r smp
    let data = (r address) &&& (~~~0x80uy)
    w address data


  let private opF3 r w smp =
    branch r smp ((r (directPageAddress r smp) &&& 128uy) = 0uy)


  let private opF4 r w smp =
    smp.registers.a <- zn8 smp (r (directPageAddressX r smp))


  let private opF5 r w smp =
    smp.registers.a <- zn8 smp (r (absoluteAddressX r smp))


  let private opF6 r w smp =
    smp.registers.a <- zn8 smp (r (absoluteAddressY r smp))


  let private opF7 r w smp =
    smp.registers.a <- zn8 smp (r (directPageAddressYIndirect r smp))


  let private opF8 r w smp =
    smp.registers.x <- zn8 smp (r (directPageAddress r smp))


  let private opF9 r w smp =
    smp.registers.x <- zn8 smp (r (directPageAddressY r smp))


  let private opFA r w smp =
    let data = r(directPageAddress r smp)
    w (directPageAddress r smp) data


  let private opFB r w smp =
    smp.registers.y <- zn8 smp (r (directPageAddressX r smp))


  let private opFC r w smp =
    smp.registers.y <- zn8 smp (smp.registers.y + 1uy)


  let private opFD r w smp =
    smp.registers.y <- zn8 smp smp.registers.a


  let private opFE r w smp =
    smp.registers.y <- zn8 smp (smp.registers.y - 1uy)


  let private opFF r w smp =
    smp.registers.pc <- smp.registers.pc - 1us


  //#endregion


  let init () =
    { registers = Registers.init ()
      timers = Array.init 3 (fun _ -> Timer.init ())
      port = Array.zeroCreate<byte> 4
      bootRomEnabled = true
      flagC = false
      flagZ = false
      flagH = false
      flagP = false
      flagV = false
      flagN = false
      timerCycles1 = 0<SpcCycle>
      timerCycles2 = 0<SpcCycle>
      cycles = 0<SpcCycle * CpuCycle> }


  let reset r smp =
    smp.registers.pc <- read16 r (Address(0xfffeus))


  let private codeTable = [|
    op00; op01; op02; op03; op04; op05; op06; op07; op08; op09; op0A; op0B; op0C; op0D; op0E; op0F
    op10; op11; op12; op13; op14; op15; op16; op17; op18; op19; op1A; op1B; op1C; op1D; op1E; op1F
    op20; op21; op22; op23; op24; op25; op26; op27; op28; op29; op2A; op2B; op2C; op2D; op2E; op2F
    op30; op31; op32; op33; op34; op35; op36; op37; op38; op39; op3A; op3B; op3C; op3D; op3E; op3F
    op40; op41; op42; op43; op44; op45; op46; op47; op48; op49; op4A; op4B; op4C; op4D; op4E; op4F
    op50; op51; op52; op53; op54; op55; op56; op57; op58; op59; op5A; op5B; op5C; op5D; op5E; op5F
    op60; op61; op62; op63; op64; op65; op66; op67; op68; op69; op6A; op6B; op6C; op6D; op6E; op6F
    op70; op71; op72; op73; op74; op75; op76; op77; op78; op79; op7A; op7B; op7C; op7D; op7E; op7F
    op80; op81; op82; op83; op84; op85; op86; op87; op88; op89; op8A; op8B; op8C; op8D; op8E; op8F
    op90; op91; op92; op93; op94; op95; op96; op97; op98; op99; op9A; op9B; op9C; op9D; op9E; op9F
    opA0; opA1; opA2; opA3; opA4; opA5; opA6; opA7; opA8; opA9; opAA; opAB; opAC; opAD; opAE; opAF
    opB0; opB1; opB2; opB3; opB4; opB5; opB6; opB7; opB8; opB9; opBA; opBB; opBC; opBD; opBE; opBF
    opC0; opC1; opC2; opC3; opC4; opC5; opC6; opC7; opC8; opC9; opCA; opCB; opCC; opCD; opCE; opCF
    opD0; opD1; opD2; opD3; opD4; opD5; opD6; opD7; opD8; opD9; opDA; opDB; opDC; opDD; opDE; opDF
    opE0; opE1; opE2; opE3; opE4; opE5; opE6; opE7; opE8; opE9; opEA; opEB; opEC; opED; opEE; opEF
    opF0; opF1; opF2; opF3; opF4; opF5; opF6; opF7; opF8; opF9; opFA; opFB; opFC; opFD; opFE; opFF
  |]


  let private instrTimes = Array.map ((*) 1<SpcCycle>) [|
  //0  1  2  3  4  5  6  7  8  9  A  B  C  D  E  F
    2; 8; 4; 5; 3; 4; 3; 6; 2; 6; 5; 4; 5; 4; 6; 8; // 0
    2; 8; 4; 5; 4; 5; 5; 6; 5; 5; 6; 5; 2; 2; 4; 6; // 1
    2; 8; 4; 5; 3; 4; 3; 6; 2; 6; 5; 4; 5; 4; 5; 2; // 2
    2; 8; 4; 5; 4; 5; 5; 6; 5; 5; 6; 5; 2; 2; 3; 8; // 3
    2; 8; 4; 5; 3; 4; 3; 6; 2; 6; 4; 4; 5; 4; 6; 6; // 4
    2; 8; 4; 5; 4; 5; 5; 6; 5; 5; 4; 5; 2; 2; 4; 3; // 5
    2; 8; 4; 5; 3; 4; 3; 6; 2; 6; 4; 4; 5; 4; 5; 5; // 6
    2; 8; 4; 5; 4; 5; 5; 6; 5; 5; 5; 5; 2; 2; 3; 6; // 7
    2; 8; 4; 5; 3; 4; 3; 6; 2; 6; 5; 4; 5; 2; 4; 5; // 8
    2; 8; 4; 5; 4; 5; 5; 6; 5; 5; 5; 5; 2; 2;12; 5; // 9
    3; 8; 4; 5; 3; 4; 3; 6; 2; 6; 4; 4; 5; 2; 4; 4; // A
    2; 8; 4; 5; 4; 5; 5; 6; 5; 5; 5; 5; 2; 2; 3; 4; // B
    3; 8; 4; 5; 4; 5; 4; 7; 2; 5; 6; 4; 5; 2; 4; 9; // C
    2; 8; 4; 5; 5; 6; 6; 7; 4; 5; 5; 5; 2; 2; 6; 3; // D
    2; 8; 4; 5; 3; 4; 3; 6; 2; 4; 5; 3; 4; 3; 4; 0; // E
    2; 8; 4; 5; 4; 5; 5; 6; 3; 4; 5; 4; 2; 2; 4; 0  // F
  |]


  let timebaseConvert (cpu: int<CpuCycle>) rem =
    let cpuTimebase = 39_375<CpuCycle> // 39375 = 236,250,000
    let smpTimebase = 45_056<SpcCycle> // 45056 =  24,576,000 * 11

    let spc = (cpu * smpTimebase) + rem
    let quo = (spc / cpuTimebase)
    let rem = spc - (cpuTimebase * quo)

    (quo, rem)


  let clock r w smp (cycles: int<SpcCycle>) =
    for _ in 1 .. (cycles / 1<_>) do
      let opcode = int <| r (immediateAddress smp)
      codeTable.[opcode] r w smp
      addCycles smp instrTimes.[opcode]


  let readPort smp (address: uint16) =
    smp.port.[int address]


  let writePort psram address data =
    PSRAM.write psram (address + 0xf4us) data


  let private updateTimers smp =
    Timer.update smp.timers.[0] (smp.timerCycles1 / Timer.Slow)
    Timer.update smp.timers.[1] (smp.timerCycles1 / Timer.Slow)
    Timer.update smp.timers.[2] (smp.timerCycles2 / Timer.Fast)

    smp.timerCycles1 <- smp.timerCycles1 % Timer.Slow
    smp.timerCycles2 <- smp.timerCycles2 % Timer.Fast


  let private readTimer smp index =
    updateTimers smp

    let result = uint8 smp.timers.[index].stage2
    smp.timers.[index].stage2 <- 0
    result


  let read8 psram smp dsp (Address(address)) =
    if address >= 0xffc0us && smp.bootRomEnabled then
      Boot.read8 (Address(address))
    else
      match address with
      | 0x00f3us -> dsp ()
      | 0x00fdus -> readTimer smp 0
      | 0x00feus -> readTimer smp 1
      | 0x00ffus -> readTimer smp 2

      | _ ->
        PSRAM.read psram address


  let private writeTimer smp i data =
    let mask = 1uy <<< i
    let timer = smp.timers.[i]
    if (not timer.enabled) && (data &&& mask) <> 0uy then
      timer.stage1 <- 0
      timer.stage2 <- 0

    timer.enabled <- (data &&& mask) <> 0uy


  let write8 psram smp dsp (Address(address)) data =
    match int address with
    | 0x00f0 -> ()
    | 0x00f1 ->
      updateTimers smp

      writeTimer smp 0 data
      writeTimer smp 1 data
      writeTimer smp 2 data

      if ((data &&& 0x10uy) <> 0uy) then
        PSRAM.write psram 0x00f4us 0uy
        PSRAM.write psram 0x00f5us 0uy

      if ((data &&& 0x20uy) <> 0uy) then
        PSRAM.write psram 0x00f6us 0uy
        PSRAM.write psram 0x00f7us 0uy

      smp.bootRomEnabled <- (data &&& 0x80uy) <> 0uy

    | 0x00f2 -> ()
    | 0x00f3 -> dsp data
    | 0x00f4 -> smp.port.[0] <- data
    | 0x00f5 -> smp.port.[1] <- data
    | 0x00f6 -> smp.port.[2] <- data
    | 0x00f7 -> smp.port.[3] <- data
    | 0x00fa -> updateTimers smp; smp.timers.[0].compare <- int data
    | 0x00fb -> updateTimers smp; smp.timers.[1].compare <- int data
    | 0x00fc -> updateTimers smp; smp.timers.[2].compare <- int data
    | 0x00fd -> ()
    | 0x00fe -> ()
    | 0x00ff -> ()
    | _      -> PSRAM.write psram address data
